import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors : {
        'merah-lihat-semua' : '#EF3F3A',
        'abu-abu' : '#545454',
        'warna-text-harga-produk' : '#EF3F3A',
        'warna-text-title-produk' : '#1B1C1E',
        'warna-text-sold-produk' : '#858585',
        'biru-muda' : '#2B8BFD',
        'text-kuning' : '#FFB800',
        'abu-abu-terang' : '#F4F4F4',
        'hijau-logo' : '#94A561',
        'hijau' : '#B7DD41',
        'black-#0A0A0A' : '#0A0A0A',
        'neutral-border' : '#D9D9D9',
        'border-hijau' : '#48872A',
        'abu-terang' : '#828282',
        'merah-terang' : '#FFF2F2',
        'hijau-success' : '#5cb85c'

      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      fontFamily: {
        'roboto' : ['Roboto, sans-serif']
      }
    },
  },
  plugins: [],
}
export default config
