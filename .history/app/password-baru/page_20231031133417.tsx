"use client"
import React, { useEffect, useState } from 'react'
import Image from 'next/image';
import Trolley from "../../public/Group 97.svg"
import Pasarid from "../../public/logoPasarId.svg"
import Link from 'next/link';
import { getMyStatePhoneProps } from '../global'
import { useRouter } from 'next/navigation';
import Swal from 'sweetalert2';

export default function page() {
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('')
    const [passwordSame, setPasswordSame] = useState('')
    const [colorButton, setColorButton] = useState('#D4D4D4');
    const [code, setCode] = useState('')
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [passAlert, setPassAlert] = useState('none');
    const [isDisable, setIsDisable] = useState(true);
    const [isCheckedPasword, setIsCheckedPassword] = useState(false);
    const [isPasswordConfirmVisible, setIsPasswordConfirmVisible] = useState(false);
    const [confirmPassword, setConfirmPassword] = useState('');
    const [isPasswordsMatch, setIsPasswordsMatch] = useState(false);
    const [SamePassAlert, setSamePassAlert] = useState('none');

    const router = useRouter()
    const handleTogglePassword = () => {
        setIsPasswordVisible(!isPasswordVisible);
    };
    const handleTogglePasswordConfirm = () => {
        setIsPasswordConfirmVisible(!isPasswordConfirmVisible);
    };
    const passwordComplexity = (event: { target: { value: any; }; }) => {

        const passwordConst = event.target.value;
        const check = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;
        const isPasswordComplex = check.test(passwordConst);
        console.log(check.test(passwordConst));

        console.log("ini pw " + passwordConst)
        setPassword(passwordConst)
        setPassAlert(isPasswordComplex ? 'none' : 'block')
        setIsDisable(!isPasswordComplex)
        // setIsFilledPassword(isPasswordComplex)
        setIsCheckedPassword(isPasswordComplex)
    };

    const handleConfirmPasswordChange = (event: { target: { value: any; }; }) => {
        const newConfirmPassword = event.target.value;
        setConfirmPassword(newConfirmPassword);

        // Check if passwords match
        const matching = (newConfirmPassword === password)
        setIsPasswordsMatch(matching)
        setPasswordSame(newConfirmPassword)
        setSamePassAlert(matching ? 'none' : 'block')
        setIsDisable(!isPasswordsMatch)
    };
    const inputCode = (event: { target: { value: any; }; }) => {
        const inputnyacode = event.target.value
        setCode(inputnyacode)
    }

const submitClick = async (event: any) => {
    event.preventDefault()
    try {
        const response = await fetch(`/api/password-reset-sms-submition`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                phone: "62" + getMyStatePhoneProps(),
                password,
                password_confirmation: passwordSame,
                code,
            }),
        });
        if (response.ok) {
            console.log("Request successful, redirecting to homepage");
            const respon = await response.json()
            console.log("respon ", respon)
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Berhasil mengubah password',
                showConfirmButton: false,
                width: 350,
                timer: 3000, // mau berapa lama, atur dsni
            }).then(() => {
                router.push(`/`);
            })
        } else {
            console.log("Verification failed, try again!");
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Gagal mengubah password',
                showConfirmButton: false,
                width: 350,
                timer: 3000, // mau berapa lama, atur dsni
            }).then(() => {
                router.push(`/lupaPassword`);
            })
        }
    } catch (error) {
        console.error('API request error', error);
    }
}
const check = () => {
    if (isCheckedPasword && isPasswordsMatch) {
        setColorButton('#EF3F3A')
        setIsDisable(false)
    }
    else {
        setColorButton('#D4D4D4')
        setIsDisable(true)
    }
}

useEffect(() => {
    check()
}, [isDisable, isCheckedPasword, isPasswordsMatch])

return (
    <div>
        <section className="min-h-screen flex items-center inset-0 z-30" >
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="container ">
                <div className='absolute inset-x-0 top-10 flex justify-center'>
                    <Image src={Pasarid} width={204} height={109} alt={''} />
                </div>
                <div className="flex justify-evenly items-center">
                    <Image src={Trolley} width="620" height="468" alt={''} className='items-center' />
                    <div className="px-[24px] py-[50px] w-[376px] h-[422px]  bg-white rounded-lg shadow-xl ">
                        <form onSubmit={submitClick}>
                            <div className="mb-[10px]">
                                <h2 className="text-center text-[32px] font-bold text-gray-600">Buat Password Baru</h2>
                                <p className='w-[328px] text-[14px] font-sans font-normal mt-[10px]'>Silahkan membuat password baru. Password baru harus berbeda dengan password lama.</p>
                            </div>
                            <div className="mb-3 flex justify-between border rounded-lg px-[6px]">
                                <input
                                    className="w-full py-2 px-[10px] hover:border-indigo-300 focus:outline-none focus:ring-0 text-[14px] font-sans"
                                    type={isPasswordVisible ? 'text' : 'password'}
                                    id="password"
                                    value={password}
                                    onChange={passwordComplexity}
                                    placeholder="Kata Sandi Baru" required
                                />
                                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePassword}>
                                    {isPasswordVisible ? 'visibility' : 'visibility_off'}
                                </span>
                            </div>
                            <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: passAlert }}>Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol</div>
                            <div className="mb-3 flex justify-between border rounded-lg px-[6px]">
                                <input
                                    className="w-full py-2 px-[10px] hover:border-indigo-300 focus:outline-none focus:ring-0 text-[14px] font-sans"
                                    type={isPasswordConfirmVisible ? 'text' : 'password'}
                                    id="password" value={passwordSame}
                                    onChange={handleConfirmPasswordChange}
                                    placeholder="Konfirmasi Kata Sandi Baru" required
                                />
                                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePasswordConfirm}>
                                    {isPasswordConfirmVisible ? 'visibility' : 'visibility_off'}
                                </span>
                            </div>
                            <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: SamePassAlert }}>Password tidak sama</div>
                            <div className="mt-[18px] flex border rounded-lg px-[6px]">
                                <input
                                    className="w-full py-[16px] px-[10px] text-[14px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners"
                                    type="text" onChange={inputCode}
                                    placeholder="Kode Verifikasi" required
                                >
                                </input>
                            </div>
                            <button className="w-full text-center text-[12px] font-normal font-sans px-3 py-2 mt-[10px] bg-[#EF3F3A] text-[#FFFFFF] rounded-full hover:bg-red-600"
                                type="submit" disabled={isDisable} style={{ backgroundColor: colorButton }}>Reset Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
)
}
