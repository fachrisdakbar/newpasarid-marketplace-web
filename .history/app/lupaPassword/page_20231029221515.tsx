"use client"
// import { Link } from 'react-router-dom';
import '../globals.css'
import Image from 'next/image';
import Trolley from "../../public/Group 97.svg"
import Line from "../../public/Line 19.svg"
import Pasarid from "../../public/logoPasarId.svg"
import { useEffect, useState } from 'react';
import LogoID from "../../public/logo_indonesia.svg"
import Modal from "./components/Modal"
import { setMyStatePhoneProps, setMyStateKeyProps, setMyStateCaptchaProps } from '../global';

export default function forgotpass() {
  const [phone, setPhone] = useState('');
  const [captcha, setCaptcha] = useState('');
  const [imageCaptcha, setImageCaptcha] = useState("");
  const [key, setKey] = useState('');
  const [sendType, setSendType] = useState('');
  const [isChecked, setIsChecked] = useState(false);

  const [isDisable, setIsDisable] = useState(true);
  const [isFilled, setIsFilled] = useState(false)
  const [passAlert, setPassAlert] = useState('none');
  const [userAlert, setUserAlert] = useState('none');
  const [isCheckedUsername, setIsCheckedUsername] = useState(false);
  const [isCheckedPasword, setIsCheckedPassword] = useState(false);
  const [isFilledUsername, setIsFilledUsername] = useState(false);
  const [isFilledPassword, setIsFilledPassword] = useState(false);
  const [isFilledCaptcha, setIsFilledCaptcha] = useState(false);
  const [alert, setAlert] = useState('none');
  const [colorButton, setColorButton] = useState('#D4D4D4');

  const handlePhoneChange = (event: { target: { value: any; }; }) => {
    const validPrefixes = [
      '831', '832', '833', '838', '895', '896', '897', '898', '899', '817',
      '818', '819', '859', '878', '877', '814', '815', '816', '855', '856',
      '857', '858', '812', '813', '852', '853', '821', '823', '822', '851',
      '811', '881', '882', '883', '884', '885', '886', '887', '888', '889'
    ];

    const phoneConst = event.target.value;
    console.log("ini angka " + phoneConst);

    // starts angka 8
    const doesNotStartWith8 = !phoneConst.startsWith('8');
    const isValidPrefix = validPrefixes.some((prefix) =>
      phoneConst.startsWith(prefix)
    );
    const isLengthValid = phoneConst.length >= 9 && phoneConst.length <= 14;

    const isValidPhoneNumber =
      !doesNotStartWith8 && isValidPrefix && isLengthValid;
    setPhone(phoneConst)
    setIsFilled(isValidPhoneNumber);
    if (isValidPhoneNumber) {
      setMyStatePhoneProps(phoneConst);
    }
    setAlert(isValidPhoneNumber ? 'none' : 'block')

    setIsDisable(!isValidPhoneNumber);
  };


  const captchaCheck = (event: { target: { value: any; }; }) => {
    const captchaConst = event.target.value;
    setCaptcha(captchaConst);
    const isLengthValidCaptcha = captchaConst.length == 6;
    setIsFilledCaptcha(isLengthValidCaptcha);
    if (isLengthValidCaptcha) {
      setMyStateCaptchaProps(captchaConst);
    }
  };

  const check = () => {
    if (isFilled && isFilledCaptcha) {
      setColorButton('#EF3F3A')
      setIsDisable(false)
    }
    else {
      setColorButton('#D4D4D4')
      setIsDisable(true)
    }
  }

  useEffect(() => {
    check()
  }, [isChecked, isFilled, isDisable, isFilledCaptcha])

  useEffect(() => {
    fetch('/api/captcha')
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        setKey(data.data['key'])
        console.log("key", key)
        setMyStateKeyProps(data.data['key'])
        setImageCaptcha(data.data['img'])
      })
      .catch((error) => {
        console.error('Terjadi kesalahan saat fetching data:', error);
      });
  }, []);



  return (
    <section className="min-h-screen flex items-center inset-0 z-30" >
      <div className="container ">
        <div className='absolute inset-x-0 top-10 flex justify-center'>
          <Image src={Pasarid} width={204} height={109} alt={''} />
        </div>
        <div className="flex justify-evenly items-center">
          <Image src={Trolley} width="620" height="468" alt={''} className='items-center' />
          <div className="w-[376px] px-[24px] py-[36px] h-[430px]  bg-white rounded-lg drop-shadow-xl">
            <div className="w-[328px]">
              <h2 className="text-center text-[32px] font-roboto font-bold text-gray-600">Lupa Password</h2>
              <p className=' mt-[10px] text-[14px] font-normal font-sans'>Masukkan nomor handphone yang Anda gunakan untuk mendaftar akun. Instruksi reset password akan kami kirimkan ke nomor Anda</p>
            </div>
            <form>
              <div className="mb-3 mt-[10px] flex border rounded-lg">
                {/* logo Indonesia starts */}
                <div className='self-center pl-[11px] py-[12px]'>
                  <Image src={LogoID} alt='' />
                </div>
                {/* Logo Indonesia ends */}
                <p className='self-center py-[16px] ml-[4px] text-[14px] font-sans text-[#1B1C1E]'>+62</p>
                <p className='self-center px-[8px] mb-1 text-lg font-sans text-[#858585]'>&#9474;</p>
                <input
                  className="py-[16px] text-[14px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners"
                  type="number" value={phone} onChange={handlePhoneChange}
                  placeholder="8123456789" required
                >
                </input>
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: alert }}>Nomor ponsel belum sesuai</div>
              <div className='mb-1 '>
                <p className='text-gray-400 text-[10px] font-sans'>Contoh: 8123456789</p>
              </div>
              <div className="mb-3">
                {imageCaptcha != "" ? (
                  <div>
                    <img src={imageCaptcha} alt="Captcha" />
                    {/* <p>Sensitive: {data.sensitive ? 'true' : 'false'}</p> */}
                    {/* <p>Key: {captchaData.key}</p> */}
                  </div>
                ) : (
                  <p>Mengambil data captcha...</p>
                )}
              </div>
              <div className="mb-3 flex justify-between border rounded-md px-3 py-2">
                <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                  type="text" id="captcha"
                  value={captcha}
                  onChange={captchaCheck}
                  placeholder="Captcha" required />
              </div>
            </form>
          </div>
          <div className='absolute z-10 ml-[792px] mt-[340px] w-[328px] '>
            {
              <Modal
                colorButton={colorButton}
                isDisable={isDisable}
                keyProps={key}
                phoneProps={phone}
                captchaProps={captcha}
              />
            }
          </div>


        </div>
      </div>

    </section >
  );
};