"use client"
import React, { useEffect, useState } from 'react'
import Image from "next/image"
import Mail from "../../../public/mail_outline.svg"
import Whatsapp from "../../../public/whatsapp.svg"
import ArrowBack from "../../../public/arrowback_white.svg"
import Link from 'next/link'
import { useRouter, useSearchParams } from 'next/navigation'
import { getMyStatePhoneProps, getMyStateKeyProps, getMyStateCaptchaProps, setMyState } from '../../global'


// interface SMSVerificationData {
//     phone: string;
//     key: string;
//     captcha: string;
//     send_type: string;
// }

const page = () => {
    const [sendType, setSendType] = useState('')
    const router = useRouter()
    

    const smsClick = async () => {
        setSendType('sms')
        setMyState('SMS')
        try {
            const response = await fetch(`/api/password-reset-sms-verification`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    // msi coding? mau mtiin
                    phone: "62" + getMyStatePhoneProps(),
                    key: getMyStateKeyProps(),
                    captcha: getMyStateCaptchaProps(),
                    send_type: 'sms',
                }),
            });
            if (response.status === 200) {
                console.log("Request successful, redirecting to /inputCode");
                const respon = await response.json()
                console.log("response : ", respon)
                router.push(`/password-baru`);
            } else {
                console.log("Verification failed, try again!");
                // router.push('/lupaPassword')

            }
        } catch (error) {
            console.error('API request error', error);
        }
    }

    const waClick = async () => {
        setSendType('wa')
        setMyState('whatsapp')
        try {
            const response = await fetch(`/api/password-reset-sms-verification`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    phone: "62" + getMyStatePhoneProps(),
                    key: getMyStateKeyProps(),
                    captcha: getMyStateCaptchaProps(),
                    send_type: 'wa',
                }),
            });
            if (response.status === 200) {
                console.log("Request successful, redirecting to /inputCode");
                const respon = await response.json()
                console.log("respon ", respon)
                router.push(`/password-baru`);
            } else {
                console.log("Verification failed, try again!");
                // router.push('/lupaPassword')

            }
        } catch (error) {
            console.error('API request error', error);
        }
    }
    return (
        <div>
            <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                <div className='self-center mr-[106px]'>
                    <button onClick={router.back}>
                        <Image src={ArrowBack} alt='' className='fill-current text-white' />
                    </button>
                </div>
                <div>
                    <h3 className='font-medium text-white text-[20px] mr-[100px]'>Verifikasi</h3>
                </div>
            </div>
            <section className="min-h-screen flex items-center justify-center" >
                <div className=" border bg-white rounded-lg w-[376px] shadow-lg ">
                    <div className="mt-[36px] mb-[60px]">
                        <h2 className="text-[20px] text-center font-semibold text-[#303030]">Pilih Metode Verifikasi</h2>
                        <div className='w-[191px] mx-auto'>
                            <p className="  text-center font-semibold text-[12px] font-sans">Pilih metode dibawah ini untuk mendapatkan kode verifikasi</p>
                        </div>
                    </div>
                    <div className='flex justify-center py-2 px-[32px] w-[312px] mx-auto h-[45px] bg-red-500 hover:bg-red-600 rounded-2xl'>
                        <div>
                            <Image src={Mail} alt='' />
                        </div>
                        <div className='ml-[17px] my-auto text-[12px]'>
                            <button
                                className="w-full font-normal text-white  "
                                type="submit" onClick={() => smsClick()}
                            >
                                Melalui SMS ke {" 0" + getMyStatePhoneProps()}
                            </button>
                        </div>
                    </div>
                    <div className='flex justify-center py-2 px-[32px] w-[312px] mt-[14px] mx-auto h-[45px] bg-red-500 hover:bg-red-600 rounded-2xl mb-[66px]'>
                        <div>
                            <Image src={Whatsapp} alt='' />
                        </div>
                        <div className='ml-[14px] my-auto text-[12px]'>
                            <button
                                className="w-full font-normal  text-white "
                                type="submit" onClick={waClick}
                            >
                                Melalui Whatsapp ke {" 0" + getMyStatePhoneProps()}
                            </button>
                        </div>
                    </div>
                </div>
            </section >
            <p className='text-center pb-[52px] font-roboto font-medium'>© 2023, PT. Bringing Inti Teknologi.</p>
        </div>
    )
}

export default page