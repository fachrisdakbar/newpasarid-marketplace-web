"use client"
import React from 'react'
import Header from "../components/Header"
import Footer from "../components/Footer"
import { useEffect, useState } from 'react';
import Link from 'next/link';
import Accordion from "../Accordion/accordion"
import Swal from 'sweetalert2'
import { setMyStatePhone } from '../global';

export const page = () => {

    const [phone, setPhone] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [nik, setNik] = useState('');
    const [nameAlert, setNameAlert] = useState('none');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [passAlert, setPassAlert] = useState('none');
    const [numAlert, setNumAlert] = useState('none');
    const [token, setToken] = useState('');
    const [isDisable, setIsDisable] = useState(true);
    const [isEdit, setIsEdit] = useState(false);
    const [isNumChecked, setIsNumChecked] = useState(false);
    const [colorButton, setColorButton] = useState('#D4D4D4');
    const [isChecked, setIsChecked] = useState(false);
    const [isCheckedName, setIsCheckedName] = useState(false);
    const [border, setBorder] = useState('#F4F4F4');
    const [background, setBackground] = useState('#F4F4F4');
    const [textColor, setTextColor] = useState('gray');
    const [cursor, setCursor] = useState('auto');
    const [selectedImage, setSelectedImage] = useState(null);
    const [id, setId] = useState();
    const [idPembeli, setIdPembeli] = useState();
    const [accordionActive, setAccordionActive] = useState(false);
    const [isPembeli, setIsPembeli] = useState(true);

    const handleImageUpload = (e: any) => {
        const image = e.target.files[0];
        const validExtensions = ['jpg', 'jpeg', 'png'];
        const extension = image.name.split('.').pop().toLowerCase();
        if (image.size > 10 * 1024 * 1024) {
            Swal.fire({
                icon: 'error',
                title: 'Ukuran file terlalu besar!',
                text: 'Mohon untuk mengunggah file lebih kecil dari 10 MB.',
            });;
            return;
        }
        if (validExtensions.indexOf(extension) === -1) {
            Swal.fire({
                icon: 'error',
                title: 'Tipe jenis file tidak valid!',
                text: 'Mohon unggah file jpg, jpeg, atau png file.',
            });
            return;
        }
        else if(image){
            setSelectedImage(image);
            fetchData()
        }
    }
    async function fetchData() {
        console.log(selectedImage);
        const formData = new FormData();
        formData.append('id', idPembeli!);
        formData.append('image', URL.createObjectURL(selectedImage!));
        formData.append('token', token);
        formData.append('_method', 'PUT');
        console.log(URL.createObjectURL(selectedImage!));
        
        try { 
            const response = await fetch(`/api/change-profile`, {
                method: 'POST',
                body: formData
            });
            const responseData = await response.json();
            if (response.ok && responseData) {
                const data = responseData;
                console.log('Data:', data);
                // setName(name);
                // setEmail(email);
                // setPhone(phone);
                // Swal.fire({
                //     position: 'center',
                //     icon: 'success',
                //     title: 'Berhasil Edit User',
                //     showConfirmButton: false,
                //     width: 350,
                //     timer: 2000
                //   })
            } else {
                console.error('Error:', responseData.message);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const handlePhoneChange = (event: { target: { value: any; }; }) => {
        const validPrefixes = [
            '831', '832', '833', '838', '895', '896', '897', '898', '899', '817',
            '818', '819', '859', '878', '877', '814', '815', '816', '855', '856',
            '857', '858', '812', '813', '852', '853', '821', '823', '822', '851',
            '811', '881', '882', '883', '884', '885', '886', '887', '888', '889'
        ];

        const phoneConst = event.target.value;
        setPhone(phoneConst);
        const doesNotStartWith8 = !phoneConst.startsWith('8');
        const isValidPrefix = validPrefixes.some((prefix) =>
            phoneConst.startsWith(prefix)
        );
        const isLengthValid = phoneConst.length >= 9 && phoneConst.length <= 14;
        const isValidPhoneNumber =
            !doesNotStartWith8 && isValidPrefix && isLengthValid;
        setIsNumChecked(isValidPhoneNumber)
        setNumAlert(isValidPhoneNumber ? 'none' : 'block')
        setIsDisable(!isValidPhoneNumber);
    };

    const nameCheck = (event: { target: { value: any; }; }) => {
        const nameConst = event.target.value;
        const check = /^[A-Za-z]+( [A-Za-z]+)*$/;
        const isNameString = check.test(nameConst);

        setName(nameConst)
        console.log(nameConst);
        
        setNameAlert(isNameString ? 'none' : 'block')
        setIsCheckedName(isNameString)
        setIsDisable(!isNameString)
    }
    const emailHandling = (event: { target: { value: any; }; }) => {
        const emailConst = event.target.value;
        setEmail(emailConst)
    }

    const checkIsEdit = () => {
        setIsEdit(!isEdit)
        if (!isEdit) {
            setBorder('#48872A')
            setBackground('white')
            setTextColor('#48872A')
            setCursor('pointer')
        }
        else if (isEdit) {
            setBorder('#F4F4F4')
            setBackground('#F4F4F4')
            setTextColor('gray')
            setCursor('auto')
        }
    }


    const editData=(e: { preventDefault: () => void; })=>{
        e.preventDefault()
        async function fetchData() {
            try { 
                const response = await fetch(`/api/edit-user/${id}`, {
                    method: 'POST',
                    body: JSON.stringify({
                        id : id,
                        name : name,
                        email : email,
                        phone : phone,
                        token : token,
                        _method: 'PUT',
                    }),
                });
                const responseData = await response.json();
                if (response.ok && responseData) {
                    const data = responseData;
                    console.log('Data:', data);
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Berhasil Edit User',
                        showConfirmButton: false,
                        width: 350,
                        timer: 2000
                      })
                } else {
                    console.error('Error:', responseData.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
    }

    useEffect(() => {
        const path = window.location.pathname;
        const lastPart = path.split('/').filter(Boolean).pop();
        setAccordionActive(true)
    
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);
            
        } else {
            console.error('Token data not found in local storage');
        }
        // ini taro luar atau harus dlm useefek?
        // harusnya udah anjir apalagi yg kureng, hrs nanya sepuh si ini w mandi jg da
        // oo gasuka ke? mana ruma lu serlok
        // off mndi mkn makan ga pduli, iya gasuka bg
        // gw mtiin ya
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    // body : JSON.stringify({
                    
                    // })
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                console.log("data: ", data);
                setIdPembeli(data.pembeli.id)
                setId(data.id)
                setName(data.name)
                setEmail(data.email)
                setPhone(data.phone)
                setMyStatePhone(data.phone)
                setNik(data.harga)
                setUsername(data.ukuran)
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
        console.log("hem");
        
    }, [token,name]);
    
    return (
        <div>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion 
                    isPembeli = {isPembeli}
                    isDataDiri={accordionActive}
                    />
                </div>
                <div className="w-[684px] font-roboto mr-32">
                    <div className=" ">
                        <form onSubmit={editData}>
                            <div className="text-left mb-6">
                                <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Data Diri</h2>
                            </div>
                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Nama Lengkap</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input
                                        className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                        type="text"
                                        value={name}
                                        onChange={nameCheck}
                                        disabled={!isEdit}
                                    >
                                        {/* au ah gush taro di folder API kkwkw */}
                                    </input>
                                </div>
                            </div>

                            {/* <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: numAlert }}>Nomor ponsel belum sesuai</div>
                            <div className='mb-2'>
                            <p className='text-gray-400 text-[10px] font-sans'>Contoh: 8123456789</p>
                            </div> */}
                            {/* <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>NIK</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                type="text" id="name"
                                value={nik}
                                disabled={!isEdit}
                                // onChange={nameCheck}
                                // placeholder="Name" required 
                                />
                                </div>
                            </div> */}

                            {/* <div className='flex justify-start items-center mb-6'> */}
                                {/* <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Status Anggota</h3> */}
                                {/* <div className="w-[500px] h-[45px] py-[16px] px-[10px] flex items-center text-[16px] font-normal "> */}
                                    {/* <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                                    type="text" id="name"
                                    // value={name}
                                    // onChange={nameCheck}
                                    // placeholder="Name" required 
                                /> */}
                                    {/* *Aktif* */}
                                {/* </div> */}
                            {/* </div> */}

                            {/* <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Jenis Kelamin</h3>
                                <div className="w-[500px] h-[45px] py-[16px] px-[10px] flex items-center">
                                <input className="mr-2"
                                type="radio" name='gender'
                                value={"Tidak Aktif"}
                                // checked={products?.['status'] == 'Tidak Aktif' ? true : false}
                                disabled={!isEdit}
                                    // onChange={nameCheck}
                                    // placeholder="Name" required 
                                    />
                                    Laki-laki
                                    <input className="ml-10 mr-2"
                                        type="radio" name='gender'
                                        value={"Aktif"}
                                        // checked={products?.['status'] == 'Aktif' ? true : false}
                                        disabled={!isEdit}
                                    // onChange={nameCheck}
                                    // placeholder="Name" required 
                                    />
                                    Perempuan
                                    </div>
                                </div> */}

                            {/* <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Username</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                type="text" id="name"
                                value={username}
                                disabled={!isEdit}
                                    // onChange={nameCheck}
                                    // placeholder="Name" required 
                                    />
                                </div>
                            </div> */}

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Email</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text" id="name"
                                        value={email}
                                        disabled={!isEdit}
                                        onChange={emailHandling}
                                    />
                                </div>
                            </div>

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Nomor Handphone</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text" id="name"
                                        value={phone}
                                        disabled={!isEdit}
                                        onChange={handlePhoneChange}
                                    />
                                </div>
                            </div>

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Password</h3>
                                    <Link href={'../data-diri/changePassword/verifikasi-method'} className='font-medium text-border-hijau text-sm'>Ubah</Link>
                            </div>

                            <div className='text-[10px] text-warna-text-harga-produk mt-[-15px] w-[500px] h-[30px] ml-[186px]' style={{ display: passAlert }}>Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol</div>

                            {/* <div className="">
                                <select id="dropdown" className=' text-sm font-sans w-full mb-3 flex justify-between border rounded-md px-2 py-2 focus:outline-none focus:ring-0 no-spinners'
                                
                                    value={type}
                                    onChange={handleDropdownChange}
                                >
                                    <option value="pilih">Pilih opsi</option>
                                    <option value="pembeli">Pembeli</option>
                                    <option value="penjual">Penjual</option>
                                </select>
                            </div> */}

                            {/* <div className="flex mb-6">
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Tanggal Lahir</h3>
                                <div className="w-[156px] h-[45px] py-[16px] px-[10px] flex border rounded-lg items-center justify-center mr-4">
                                    <label>
                                    <select value={day} onChange={(e) => handleDayChange(e)} disabled={!isEdit} required>
                                    <option value="">Pilih tanggal</option>
                                    {Array.from({ length: 31 }, (_, i) => i + 1).map((day) => (
                                        <option key={day} value={day}>
                                        {day}
                                        </option>
                                        ))}
                                        </select>
                                        </label>
                                        </div>
                                <div className="w-[156px] h-[45px] py-[16px] px-[10px] flex border rounded-lg items-center justify-center mr-4">
                                    <label>
                                    <select value={month} onChange={(e) => handleMonthChange(e)} disabled={!isEdit} required>
                                            <option value="">Pilih bulan</option>
                                            {Array.from({ length: 12 }, (_, i) => i + 1).map((month) => (
                                                <option key={month} value={month}>
                                                {month}
                                                </option>
                                                ))}
                                                </select>
                                                </label>
                                                </div>
                                                <div className="w-[156px] h-[45px] py-[16px] px-[10px] flex border rounded-lg items-center justify-center">
                                                <label>
                                                <select value={year} onChange={(e) => handleYearChange(e)} disabled={!isEdit} required>
                                                <option value="">Pilih tahun</option>
                                                {Array.from({ length: 100 }, (_, i) => new Date().getFullYear() - i).map((year) => (
                                                    <option key={year} value={year}>
                                                    {year}
                                                    </option>
                                                    ))}
                                                    </select>
                                                    </label>
                                                    </div>
                                                </div> */}

                            {/* <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: typeAlert }}>Pilih penjual atau pembeli</div> */}
                            {/* <div className='mb-3 flex justify-between'>
                                <input type='checkbox' className='w-5 ml-[3px] accent-[#FFB800]' onClick={() => checkBox()} required />
                                <p className='text-[10px] text-gray-400 ml-[8px] text-left font-sans'>
                                    Dengan mendaftar, saya menyetujui <Link className='text-red-600' href={'s&k'}> Syarat dan Ketentuan</Link> serta <Link href={'kebijakan-privasi'} className='text-red-600'>Kebijakan Privasi</Link>.
                                </p>
                            </div> */}

                            {isEdit &&
                                <div className='ml-[184px] flex'>
                                    <button type='submit' className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-border-hijau cursor-pointer flex justify-center items-center mr-14">
                                        <div>Simpan</div>
                                    </button>
                                    <div onClick={() => checkIsEdit()} className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-abu-terang cursor-pointer flex justify-center items-center">
                                        Batal
                                    </div>
                                </div>
                            }

                            {!isEdit &&
                                <div className='ml-[184px]'>
                                    <div onClick={() => checkIsEdit()}
                                        className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-merah-lihat-semua flex justify-center items-center cursor-pointer">
                                        Edit
                                    </div>
                                </div>}

                        </form>
                    </div>
                </div>

                <div className='bg-white border-[1px] border-neutral-border rounded-lg w-[260px] h-[416px] mt-[56px] flex flex-col'>
                    <div className='w-[228px] h-[228px] flex justify-center items-center mb-4 mt-4'>

                        {selectedImage && <img src={URL.createObjectURL(selectedImage)} alt="Selected" className='w-[220px] h-[200px]' />}
                    </div>
                    <div className='w-[228px] h-[44px] border-[1px] rounded-lg font-medium text-[14px] flex justify-center items-center mb-4 ml-4 mr-4 cursor-pointer'
                        style={{ borderColor: border, backgroundColor: background, color: textColor, cursor: 'pointer' }}
                        >
                        <input type='file'
                            id="imageInput"
                            onChange={handleImageUpload}
                            style={{ display: 'none' }}
                            disabled={!isEdit}
                            />

                        <label htmlFor="imageInput" className='w-[220px] flex justify-center' style={{ cursor: 'pointer' }}>Pilih Foto</label>
                    </div>
                    <div className='font-normal text-[14px] w-[228px] h-[80px] flex justify-center items-center text-abu-terang mb-4 ml-4 mr-4'>Besar file: maksimum 10.000.000 bytes (10 Megabytes). Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
export default page
