'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Accordion from "../../Accordion/accordion"
import React, { useEffect, useState } from 'react'
import Modal from './components/modal'
    ; import Swal from 'sweetalert2'
import { useRouter } from 'next/navigation'

const page = () => {
    const router = useRouter()

    const [apiCallSuccessful, setApiCallSuccessful] = useState(false);
    const [showModal, setShowModal] = useState(true);

    const [isPinVisibleLama, setIsPinVisibleLama] = useState(false);
    const [isPinVisibleBaru, setIsPinVisibleBaru] = useState(false);
    const [isPinVisibleKonfirmasi, setIsPinVisibleKonfirmasi] = useState(false);
    // const [accordionActive, setAccordionActive] = useState(false);
    const [isPembeli, setIsPembeli] = useState(true);
    const [onClose, setOnClose] = useState(true)
    const [showCaptchaModal, setShowCaptchaModal] = useState(true);
    const [otp, setOtp] = useState('')
    const [isFilledCode, setIsFilledCode] = useState(false)
    const [isDisable, setIsDisable] = useState(true);
    const [token, setToken] = useState('');
    const [phone, setPhone] = useState('')
    const [pinLama, setPinLama] = useState('')

    const handleCloseModal = () => {
        setShowModal(false);
    };

    // const handleTogglePinLama = () => {
    //     setIsPinVisibleLama(!isPinVisibleLama);
    // };
    const handleTogglePinBaru = () => {
        setIsPinVisibleBaru(!isPinVisibleBaru);

    };
    const handleTogglePinKonfirmasi = () => {
        setIsPinVisibleKonfirmasi(!isPinVisibleKonfirmasi);
    };
    const handlePin = (event: { target: { value: any; }; }) => {

    }
    const inputCode = (event: { target: { value: any; }; }) => {
        const inputnyacode = event.target.value
        const isLengthValidCode = inputnyacode.length == 6;
        if (isLengthValidCode) {
            setOtp(inputnyacode)
            setIsFilledCode(isLengthValidCode);
            setIsDisable(!isLengthValidCode)
        }
    }

    const handleSubmit = async (e: any, captcha: any, deliveryMethod: any) => {
        e.preventDefault()
        try {
            const responseUpdate = await fetch(`https://api.pasarid-dev.bitcorp.id/api/payment/update-pin`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ` + token,
                },
                body: JSON.stringify({
                    phone,
                    pin: pinLama,
                    code: otp,
                }),
            });
            if (responseUpdate.ok) {
                const dataUpdatePin = await responseUpdate.json()
                console.log("Response Update Pin successful : ", dataUpdatePin);
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Berhasil mengubah password',
                    showConfirmButton: false,
                    width: 350,
                    timer: 1500, // mau berapa lama, atur dsni
                }).then(() => {
                    router.push(`/data-diri`);
                })
            } else {
                console.log("Kesalahan pada PIN dan OTP anda!");
                throw new Error('Update PIN Network response error');
            }

        } catch (error) {
            console.error('Error fetching data:', error);
        }

    }

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        async function fetchPhone() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user`, {
                    headers: {
                        Authorization: `Bearer ` + token,
                    },
                });
                if (response.ok) {
                    const data = await response.json();
                    console.log("phone ", data.phone);
                    setPhone(data.phone)
                } else {
                    console.log("error fetching phone");
                    throw new Error('Network response was not ok');
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchPhone();
    }, [token])

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        isPembeli={isPembeli}
                        isChangePin={true}
                    />
                </div>
                <div className="w-[684px] font-roboto">
                    {<Modal onClose={onClose} />}
                    <div className='px-10 py-10'>
                        <div className="text-left mb-6">
                            <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Ganti PIN</h2>
                        </div>
                        <form>
                            <div className='font-roboto text-[16px] font-normal mb-4'>Kode PIN harus menggunakan 6 angka. Masukkan kode PIN untuk akun Localoka. Kode PIN baru harus berbeda dengan kode PIN lama.</div>
                            {/* <div className='flex justify-start items-center mb-6'>
                            <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Kode PIN Lama</h3>
                            <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                <input
                                    className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                    type={isPinVisibleLama ? 'text' : 'password'}
                                    placeholder='Masukkan kata sandi lama'
                                // value={name}
                                // onChange={nameCheck}
                                // disabled={!isEdit}
                                >
                                </input>
                                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePinLama}>
                                    {isPinVisibleLama ? 'visibility' : 'visibility_off'}
                                </span>
                            </div>
                        </div> */}

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Kode PIN Baru</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input
                                        className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                        type={isPinVisibleBaru ? 'text' : 'password'}
                                        placeholder='Masukkan kata sandi baru'
                                    // value={name}
                                    // onChange={nameCheck}
                                    // disabled={!isEdit}
                                    >
                                    </input>
                                    <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePinBaru}>
                                        {isPinVisibleBaru ? 'visibility' : 'visibility_off'}
                                    </span>
                                </div>
                            </div>
                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Konfirmasi</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input
                                        className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                        type={isPinVisibleKonfirmasi ? 'text' : 'password'}
                                        placeholder='Konfirmasi kata sandi baru'
                                    // value={name}
                                    // onChange={nameCheck}
                                    // disabled={!isEdit}
                                    >
                                    </input>
                                    <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePinKonfirmasi}>
                                        {isPinVisibleKonfirmasi ? 'visibility' : 'visibility_off'}
                                    </span>
                                </div>
                            </div>
                            <div className='flex  mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Kode OTP</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] border rounded-lg">
                                    <input
                                        className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[400px] h-[30px]"
                                        type='text'
                                        placeholder='OTP'
                                        onChange={inputCode}
                                    />
                                </div>
                            </div>
                        </form>
                        <div className='ml-[184px] flex'>
                            <div className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-border-hijau cursor-pointer flex justify-center items-center mr-14">
                                Simpan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default page