"use client"
import Image from 'next/image';
import React, { useEffect, useState } from 'react'
import Mail from "../../../..//public/mail_outline.svg"
import Whatsapp from "../../../../public/whatsapp.svg"
import ArrowBack from "../../../../public/arrowback_white.svg"
import { useRouter } from 'next/navigation';
import Loading from '../../../login/loading/page'
import { setMyState } from '@/app/global';
import { getMyStatePhone } from '@/app/global';

const page = () => {
    const router = useRouter()
    const [key, setKey] = useState('');
    const [imageCaptcha, setImageCaptcha] = useState("");
    const [token, setToken] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [sendType, setSendType] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }

        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
                    headers: {
                        Authorization: `Bearer ` + token,
                    },
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                console.log(data.phone);
                setPhoneNumber(data.phone)

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token])

    const sendOtpSms = () => {
        setLoading(true)
        async function fetchData() {
            try {
                const response = await fetch(`/api/changePin-otp(test)`, {
                    method: 'POST',
                    body: JSON.stringify({
                        // send_type: 'sms',
                        // token: token,
                        phone: "62" + getMyStatePhone(),
                        key,
                        // captcha,
                        // type

                    }),
                })
                const data = await response.json();
                if (response.ok && data) {
                    console.log(data);
                } else {
                    console.error('Error:', data.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
        setMyState('sms')
        setLoading(false)
        router.push('/data-diri/changePassword')
    }
    const sendOtpWa = () => {
        setLoading(true)
        async function fetchData() {
            try {
                const response = await fetch(`/api/changePassword-send-otp`, {
                    method: 'POST',
                    body: JSON.stringify({
                        send_type: 'wa',
                        token: token,
                    }),
                })
                const data = await response.json();
                if (response.ok && data) {
                    console.log(data);
                } else {
                    console.error('Error:', data.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
        setMyState('wa')
        setLoading(false)
        router.push('/data-diri/changePassword')
    }

    useEffect(() => {
        fetch('/api/captcha')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
                }
                return response.json();
            })
            .then((data) => {
                setKey(data.data['key']);
                setImageCaptcha(data.data['img'])
            })
            .catch((error) => {
                console.error('Terjadi kesalahan saat fetching data:', error);
            });
    }, [])
    
    return (
        <>
            <div>
                <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                    <div className='self-center mr-[106px]'>
                        <button onClick={router.back}>
                            <Image src={ArrowBack} alt='' className='fill-current text-white' />
                        </button>
                    </div>
                    <div>
                        <h3 className='font-medium text-white text-[20px] mr-[100px]'>Verifikasi</h3>
                    </div>
                    {loading && <Loading />}
                </div>
                <section className="min-h-screen flex items-center justify-center" >
                    <div className=" border bg-white rounded-lg w-[376px] shadow-lg ">
                        <div className="mt-[36px] mb-[60px]">
                            <h2 className="text-[20px] text-center font-semibold text-[#303030]">Pilih Metode Verifikasi</h2>
                            <div className='w-[191px] mx-auto'>
                                <p className="  text-center font-semibold text-[12px] font-sans">Pilih metode dibawah ini untuk mendapatkan kode verifikasi</p>
                            </div>
                        </div>
                        <div className='flex justify-center py-2 px-[32px] w-[312px] mx-auto h-[45px] bg-red-500 hover:bg-red-600 rounded-2xl cursor-pointer'
                            onClick={() => {
                                sendOtpSms()
                                // handleModalYesClick();
                            }}>
                            <div>
                                <Image src={Mail} alt='' />
                            </div>
                            <div className='ml-[17px] my-auto text-[12px]'>
                                <button
                                    className="w-full font-normal text-white  "
                                    type="submit"
                                >
                                    Melalui SMS ke {phoneNumber}
                                </button>
                            </div>
                        </div>
                        <div className='flex justify-center py-2 px-[32px] w-[312px] mt-[14px] mx-auto h-[45px] bg-red-500 hover:bg-red-600 rounded-2xl mb-[66px] cursor-pointer'
                            onClick={() => {
                                sendOtpWa()
                                // handleModalYesClick();
                            }}>
                            <div>
                                <Image src={Whatsapp} alt='' />
                            </div>
                            <div className='ml-[14px] my-auto text-[12px]'>
                                <button
                                    className="w-full font-normal text-white"
                                    type="submit"
                                >
                                    Melalui Whatsapp ke {phoneNumber}
                                </button>
                            </div>
                        </div>
                    </div>
                </section >
                <p className='text-center pb-[52px] font-roboto font-medium'>© 2023, PT. Bringing Inti Teknologi.</p>
            </div>
        </>
    )
}

export default page