// CaptchaModal.js
import React, { useState, useEffect } from 'react';

const Modal = ({ show, onClose, onCaptchaVerification }) => {
    const [captcha, setCaptcha] = useState('');
    const [deliveryMethod, setDeliveryMethod] = useState('sms');

    useEffect(() => {
        // Check for successful captcha verification
        if (captcha && deliveryMethod) {
            onCaptchaVerification(captcha, deliveryMethod);
        }
    }, [captcha, deliveryMethod, onCaptchaVerification]);

    return (
        show && (
            <div className="captcha-modal">
                {/* Captcha input */}
                <input type="text" value={captcha} onChange={(e) => setCaptcha(e.target.value)} />

                {/* Delivery method selection */}
                <label>
                    <input
                        type="radio"
                        value="sms"
                        checked={deliveryMethod === 'sms'}
                        onChange={() => setDeliveryMethod('sms')}
                    />
                    SMS
                </label>
                <label>
                    <input
                        type="radio"
                        value="wa"
                        checked={deliveryMethod === 'wa'}
                        onChange={() => setDeliveryMethod('wa')}
                    />
                    WhatsApp
                </label>
            </div>
        )
    );
};

export default Modal;
