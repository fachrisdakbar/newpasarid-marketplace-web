// CaptchaModal.js
import { useRouter } from 'next/navigation';
import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { SelectButton } from 'primereact/selectbutton'
import { Dropdown } from 'primereact/dropdown';


const Modal = ({ onClose = false }) => {
    const router = useRouter()
    const [phone, setPhone] = useState('')
    const [captcha, setCaptcha] = useState('');
    const [token, setToken] = useState('');
    const [selectedCity, setSelectedCity] = useState(null);
    const cities = [
        { name: 'New York', code: 'NY' },
        { name: 'Rome', code: 'RM' },
        { name: 'London', code: 'LDN' },
        { name: 'Istanbul', code: 'IST' },
        { name: 'Paris', code: 'PRS' }
    ];

    const [deliveryMethod, setDeliveryMethod] = useState('');
    const [key, setKey] = useState('');
    const [imageCaptcha, setImageCaptcha] = useState("");

    const options = ['SMS', 'WhatsApp'];
    const [value, setValue] = useState(options[0]);
    // https://api.pasarid-dev.bitcorp.id/api/user

    const handleModalSubmit = async () => {
        try {
            const responseUpdate = await fetch(`https://api.pasarid-dev.bitcorp.id/api/payment/send-sms-otp-for-update-pin`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ` + token,
                },
                body: JSON.stringify({
                    phone,
                    key,
                    captcha,
                    type: deliveryMethod,
                }),
            });
            if (responseUpdate.ok) {
                const dataUpdatePin = await responseUpdate.json()
                console.log("Response OTP for Update Pin successful : ", dataUpdatePin);
                onClose
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Berhasil mengubah password',
                    showConfirmButton: false,
                    width: 350,
                    timer: 1500, // mau berapa lama, atur dsni
                }).then(() => {
                    router.push(`/data-diri`);
                })
            } else {
                console.log("Kesalahan pada PIN dan OTP anda!");
                throw new Error('Update PIN Network response error');
            }

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }
    // useEffect(() => {
    //     // fetching phone 
    //     fetch('https://api.pasarid-dev.bitcorp.id/api/user')
    //         .then((response) => {
    //             if (!response.ok) {
    //                 throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
    //             }
    //             return response.json();
    //         })
    //         .then((data) => {
    //             setPhone(data.data['phone']);
    //         })
    //         .catch((error) => {
    //             console.error('Terjadi kesalahan saat fetching data:', error);
    //         });
    // }, [])

    // useEffect(() => {
    //     // fetching token dari localStorage
    //     const tokenData = localStorage.getItem('token');
    //     if (tokenData) {
    //         const parsedData = JSON.parse(tokenData);
    //         const extractedValue = `${parsedData.token}`;
    //         setToken(extractedValue);
    //         console.log(extractedValue);

    //     } else {
    //         console.error('Token data not found in local storage');
    //     }
    //     // fetching captcha
    //     fetch('/api/captcha')
    //         .then((response) => {
    //             if (!response.ok) {
    //                 throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
    //             }
    //             return response.json();
    //         })
    //         .then((data) => {
    //             setKey(data.data['key']);
    //             setImageCaptcha(data.data['img'])
    //         })
    //         .catch((error) => {
    //             console.error('Terjadi kesalahan saat fetching data:', error);
    //         });
    // }, [token])

    // useEffect(() => {
    //     if (captcha && deliveryMethod) {
    //         onCaptchaVerification(captcha, deliveryMethod);
    //     }
    // }, [captcha, deliveryMethod, onCaptchaVerification]);

    return (
        <>
            <script src="https://unpkg.com/primereact/core/core.min.js"></script>

            <script src="https://unpkg.com/primereact/selectbutton/selectbutton.min.js"></script>
            <div className='w-[1314px] h-[780px] bg-opacity-50 bg-black absolute '>
                <div className="grid place-items-center mt-16 ">
                    <div className="w-[480px] bg-white border rounded-xl px-[16px] py-[16px]">
                        <h3 className='text-[16px] font-semibold text-center items-center mr-10 mb-3'>Verifikasi Nomor HP</h3>
                        <p>Untuk mengganti PIN, silahkan verifikasi nomor anda terlebih dahulu.</p>
                        <form onSubmit={handleModalSubmit}>
                            {/* <Captcha /> */}
                            <div className="mb-3">
                                {imageCaptcha != "" ? (
                                    <div>
                                        <img src={imageCaptcha} alt="Captcha" />
                                        {/* <p>Sensitive: {data.sensitive ? 'true' : 'false'}</p> */}
                                        {/* <p>Key: {captchaData.key}</p> */}
                                    </div>
                                ) : (
                                    <p>Mengambil data captcha...</p>
                                )}
                            </div>
                            <div className='flex'>
                                <div className="mb-3 flex justify-between ">
                                    <h3 className='text-[16px] w-36 h-11 font-normal flex justify-start items-center mr-10'>Captcha</h3>
                                    <input className="w-full border rounded-md px-3 py-2 hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                                        type="text" id="captcha"
                                        value={captcha}
                                        onChange={(e) => setCaptcha(e.target.value)}
                                        placeholder="Captcha" required />
                                </div>
                            </div>
                            {/* Delivery method selection */}
                            <div className='mb-3'>
                                {/* <label>
                                    <input
                                        type="radio"
                                        value="sms"
                                        checked={deliveryMethod === 'sms'}
                                        onChange={() => setDeliveryMethod('sms')}
                                    />
                                    SMS
                                </label> */}
                                <div className="card flex justify-content-center">
                                    <SelectButton value={value} options={options} onChange={(e) => setValue(e.value)}></SelectButton>
                                </div>
                                
                                {/* <label>
                                    <input
                                        type="radio"
                                        value="wa"
                                        checked={deliveryMethod === 'wa'}
                                        onChange={() => setDeliveryMethod('wa')}
                                    />
                                    WhatsApp
                                </label> */}
                            </div>
                            <button type='submit' className='w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-border-hijau cursor-pointer flex justify-center items-center mr-14'>Kirim</button>
                        </form>
                    </div>
                </div>
            </div >
        </>
    );

};

export default Modal;
