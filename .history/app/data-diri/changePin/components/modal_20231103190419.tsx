// CaptchaModal.js
import React, { useState, useEffect } from 'react';

const Modal = ({ show, onClose, onCaptchaVerification }) => {
    const [captcha, setCaptcha] = useState('');
    const [deliveryMethod, setDeliveryMethod] = useState('sms');
    const [key, setKey] = useState('');
    const [imageCaptcha, setImageCaptcha] = useState("");

    useEffect(() => {
        fetch('/api/captcha')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
                }
                return response.json();
            })
            .then((data) => {
                setKey(data.data['key']);
                setImageCaptcha(data.data['img'])
            })
            .catch((error) => {
                console.error('Terjadi kesalahan saat fetching data:', error);
            });
    }, [])
    useEffect(() => {
        // Check for successful captcha verification
        if (captcha && deliveryMethod) {
            onCaptchaVerification(captcha, deliveryMethod);
        }
    }, [captcha, deliveryMethod, onCaptchaVerification]);

    return (
        show && (
            <div className="flex">
                
                {/* <Captcha /> */}
                <div className="mb-3">
                    {imageCaptcha != "" ? (
                        <div>
                            <img src={imageCaptcha} alt="Captcha" />
                            {/* <p>Sensitive: {data.sensitive ? 'true' : 'false'}</p> */}
                            {/* <p>Key: {captchaData.key}</p> */}
                        </div>
                    ) : (
                        <p>Mengambil data captcha...</p>
                    )}
                </div>
                <div className="mb-3 flex justify-between border rounded-md px-3 py-2">
                    <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                        type="text" id="captcha"
                        value={captcha}
                        onChange={(e) => setCaptcha(e.target.value)}
                        placeholder="Captcha" required />
                </div>
                {/* Delivery method selection */}
                <label>
                    <input
                        type="radio"
                        value="sms"
                        checked={deliveryMethod === 'sms'}
                        onChange={() => setDeliveryMethod('sms')}
                    />
                    SMS
                </label>
                <label>
                    <input
                        type="radio"
                        value="wa"
                        checked={deliveryMethod === 'wa'}
                        onChange={() => setDeliveryMethod('wa')}
                    />
                    WhatsApp
                </label>
            </div>
        )
    );
};

export default Modal;
