'use client'
import React, { useEffect, useState } from 'react'
import Rumah from '../../../public/home-putih.svg'
import Toko from '../../../public/store-putih.svg'
import Image from 'next/image'
import Vector from '../../../public/plusvector.svg'

const page = () => {
    const [isGreeenHouse, setIsGreenHouse] = useState('#AEAEAE')
    const [isGreeenToko, setIsGreenToko] = useState('#AEAEAE')
    const [isHouse, setIsHouse] = useState(true)
    const [isToko, setIsToko] = useState(true)
    const [isVisible, setIsVisible] = useState('none')

    const clickHouse = () => {
    const [nama, setNama] = useState('')
    const [alamat, setAlamat] = useState('')
    const [pembeliId, setPembeliId] = useState('')
    const [longitude, setLongitude] = useState('')
    const [latitude, setLatitude] = useState('')
    const [namaPenerima, setNamaPenerima] = useState('')
    const [token, setToken] = useState('');

    


    const clickHouse = () => {
        setIsHouse(!isHouse)
        if (isHouse) {
            setIsGreenHouse('#48872A')
            setIsGreenToko('#AEAEAE')
            setIsToko(true)
        }
        else {
            setIsGreenHouse('#AEAEAE')
            setIsGreenToko('#AEAEAE')
        }
    }
    const clickToko = () => {
        setIsToko(!isToko)
        if (isToko) {
            setIsGreenToko('#48872A')
            setIsGreenHouse('#AEAEAE')
            setIsHouse(true)
        }
        else if (!isToko) {
            setIsGreenToko('#AEAEAE')
            setIsGreenHouse('#AEAEAE')
        }
    }

    const saveAlamat = async () => {
        // Construct the request body
        const requestBody = {
            nama,
            alamat,
            nama_penerima: namaPenerima,
            pembeli_id: pembeliId,
        };

        // Make a POST request to the API endpoint
        try {
            const response = await fetch('/api/elorest/Models/Alamat', {
                method: 'POST',
                headers: {
                    Authorization:  `Bearer ` + token,
                },
                body: JSON.stringify(requestBody),
            });

            if (response.ok && response.status=== 200) {
                console.log("Simpan data berhasil");
                
            } else {
                // Handle the error response, e.g., show an error message
                console.error('Failed to save Alamat:', response.status, response.statusText);
            }
        } catch (error) {
            // Handle network or other errors
            console.error('Error:', error);
        }
    };

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            console.log(tokenData);

            const parsedData = JSON.parse(tokenData);
            console.log(parsedData.token);

            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log("token adalah : ", extractedValue)
        } else {
            console.error('Token data not found in local storage');
        }
    }, []);

    return (
        <>
            <div className="flex px-4 py-3 border rounded-xl bg-[#48872A] text-white cursor-pointer"
                onClick={() => setIsVisible('block')}>
                <Image src={Vector} alt={''} className="mr-2" />
                <div>
                    <div>Tambah Alamat Baru</div>
                </div>
            </div>
            <section className='w-[100vw] h-[100vh] bg-opacity-50 bg-black fixed inset-0 z-30 flex justify-center items-center self-center' style={{ display: isVisible }}>
                <div className='flex justify-center items-center'>
                    <div className='w-[584px] h-[752px] bg-white rounded-2xl'>
                        <svg onClick={() => setIsVisible('none')} className='self-center cursor-pointer mt-3 ml-3 absolute' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                        </svg>
                        <div className='font-semibold font-roboto text-xl ml-10 mt-10 mb-6 w-[200px]'>Alamat Baru</div>
                        <div className='w-[504px] h-[532px] ml-10 mr-10 mb-10'>
                            <div className='flex mb-6'>
                                <div className='mr-6'>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Nama Penerima</label>
                                    <div className="w-[240px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[240px] h-[30px]"
                                            placeholder='Nama Penerima'
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Nomor Ponsel</label>
                                    <div className="w-[240px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[240px] h-[30px]"
                                            placeholder='Contoh : 08123456789'
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div className='flex mb-6'>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Lokasi</label>
                                    <div className="w-[504px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[504px] h-[30px]"
                                            placeholder='Provinsi, Kota, Kecamatan, Kelurahan'
                                        >
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div className='flex mb-6'>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Alamat</label>
                                    <div className="w-[504px] h-[80px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[504px] h-[30px]"
                                            placeholder='Nama Jalan, Gedung, No Rumah'
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div className='flex mb-6'>
                                <div className='mr-6'>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Label Alamat</label>
                                    <div className="w-[240px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[240px] h-[30px]"
                                            placeholder='Label Alamat'
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Tandai Sebagai</label>
                                    <div className="w-[240px] h-[44px] flex">
                                        <div className='w-[120px] h-[44px] flex justify-center items-center cursor-pointer'
                                            onClick={() => clickHouse()}
                                            style={{ backgroundColor: isGreeenHouse, borderTopLeftRadius: '40px', borderBottomLeftRadius: '40px' }}>
                                            <Image src={Rumah} alt='' width={100} height={100} className='w-5 h-5' />
                                            <div className='font-normal text-white font-roboto text-sm ml-2'>Rumah</div>
                                        </div>
                                        <div className='w-[120px] h-[44px] flex justify-center items-center cursor-pointer'
                                            onClick={() => clickToko()}
                                            style={{ backgroundColor: isGreeenToko, borderTopRightRadius: '40px', borderBottomRightRadius: '40px' }}>
                                            <Image src={Toko} alt='' width={100} height={100} className='w-5 h-5' />
                                            <div className='font-normal text-white font-roboto text-sm ml-2'>Toko</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='w-[504px] h-[128px] mb-10 bg-blue-300 flex justify-center items-center'>
                                map
                            </div>
                            <div className='w-[504px] h-[44px] bg-border-hijau text-sm font-roboto font-medium text-white flex justify-center items-center rounded-lg'>Simpan Alamat</div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default page