'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '../../../../Accordion/accordion'
import Clipboard from '../../../../../public/Clipboard.svg'
import Image from 'next/image'
import Market from '../../../../../public//toko.svg'
import Location from '../../../../../public/location.svg'
import Delivery from '../../../../../public/delivery.svg'
import Pickup from '../../../../../public/pickup.svg'
import Camera from '../../../../../public/camera.svg'
import Link from 'next/link'
import Rating from '@mui/material/Rating';
import Stack from '@mui/material/Stack';
import { useRouter, useParams } from 'next/navigation'
import Swal from 'sweetalert2'

interface Order {
    id: number,
    store_order_id: string,
    created_at: string,
    pengiriman_id: number,
    pengiriman: {
        waktu_pengiriman: string,
        biaya: number,
        jenis_pengiriman: {
            title: string
            id: number
        }
    },
    order_items: [
        {
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {
    const router = useRouter()
    const { id } = useParams()
    const [colorButton, setColorButton] = useState('#D4D4D4');
    const [isDisable, setIsDisable] = useState(true);
    const [token, setToken] = useState('');
    const [idPembeli, setIdPembeli] = useState('');
    const [orderId, setOrderId] = useState('')
    const [produkId, setProdukId] = useState('')
    const [idPenjual, setIdPenjual] = useState('')
    const [jenisPengirimanId, setJenisPengirimanId] = useState('')
    const [ratingPenjual, setRatingPenjual] = useState<number | null>(null)
    const [ratingProduk, setRatingProduk] = useState<number | null>(null)
    const [selectedImage, setSelectedImage] = useState(null);
    const [ratingJenisPengiriman, setRatingJenisPengiriman] = useState<number | null>(null)
    const [comment, setComment] = useState('')
    // const [orders, setOrders] = useState<Order[]>([]);
    const [namaProduk, setNamaProduk] = useState('')
    const [namaToko, setNamaToko] = useState('')
    const [namaPasar, setNamaPasar] = useState('')
    const [loading, setLoading] = useState(true);


    const handleSubmitRating = async (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        try {
            const formData = new FormData();
            // formData.append('id', orderId!);
            formData.append('order_id', orderId!);
            formData.append('pembeli_id', idPembeli);
            formData.append('produk_id', produkId);
            formData.append('penjual_id', idPenjual);
            formData.append('jenis_pengiriman_id', jenisPengirimanId);
            formData.append('rating_penjual', String(ratingPenjual ?? ''));
            formData.append('rating_produk', String(ratingProduk ?? ''));
            formData.append('rating_jenis_pengiriman', String(ratingJenisPengiriman ?? ''));
            formData.append('comment', comment);
            formData.append('token', token);
            formData.append('image', selectedImage!);

            const response = await fetch('/api/Rating', {
                // https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Rating
                method: 'POST',
                // mode: 'no-cors',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: 
                // formData
                JSON.stringify({
                    order_id: orderId,
                    pembeli_id: idPembeli,
                    produk_id: produkId,
                    penjual_id: idPenjual,
                    jenis_pengiriman_id: jenisPengirimanId,
                    rating_penjual: ratingPenjual,
                    rating_produk: ratingProduk,
                    rating_jenis_pengiriman: ratingJenisPengiriman,
                    // image: selectedImage,
                    comment,
                    token,
                  
                }),
            });
            const data = await response.json()
            console.log(data);

            if (response.ok) {
                Swal.fire({
                    icon: 'success',
                    position: 'center',
                    title: 'Berhasil memasukkan rating',
                    showConfirmButton: false,
                    width: 350,
                    timer: 2000
                }).then(() => {
                    // router.push('/Pesanan/PesananSelesai')
                })
            } else {
                const errortext = response.statusText
                console.log("Gagal mengirim rating :", errortext);
                Swal.fire({
                    icon: 'error',
                    position: 'center',
                    title: 'Gagal memasukkan rating',
                    // text: 'error:'  ,
                    showConfirmButton: false,
                    width: 350,
                    timer: 2000
                }).then(() => {
                    router.push('/Pesanan/PesananSelesai')
                })
            }
        } catch (error) {
            console.log('Kesalahan terjadi saat submit rating : ', error);
        }
    }
    const check = () => {

        if (ratingProduk !== null && ratingPenjual !== null && ratingJenisPengiriman !== null) {
            setColorButton('#EF3F3A')
            setIsDisable(false)
        }
        else {
            setColorButton('#D4D4D4')
            setIsDisable(true)
        }
    };

    const handleImageUpload = (e: any) => {
        const image = e.target.files[0];
        const validExtensions = ['jpg', 'jpeg', 'png'];
        const extension = image.name.split('.').pop().toLowerCase();
        console.log(image);

        if (image.size > 500 * 1024) { // Limit to 500 KB
            Swal.fire({
                icon: 'error',
                title: 'Ukuran file terlalu besar!',
                text: 'Mohon untuk mengunggah file lebih kecil dari 500 KB.',
            });
            return;
        }

        if (validExtensions.indexOf(extension) === -1) {
            Swal.fire({
                icon: 'error',
                title: 'Tipe jenis file tidak valid!',
                text: 'Mohon unggah file jpg, jpeg, atau png file.',
            });
            return;
        }

        if (image) {
            setSelectedImage(image);
            console.log(image);
        }
    }


    async function fetchData() {
        try {
            const response = await fetch(`/api/getUserPembeli`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res.data
            console.log(data);
            if (data) {
                setLoading(false)
            }
            else if (data == undefined) {
                setLoading(true)
            }
            setIdPembeli(data.pembeli.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.penjual&with[]=orderItems.pasar&with[]=pengiriman&with[]=pengiriman.jenisPengiriman&where[]=status,selesai&where[]=id,${id}&where[]=pembeli_id,${idPembeli}&orderby=updated_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                const res = await response.json();
                const data = res[0]
                console.log(data);

                setOrderId(data.id)
                console.log("orderID ", orderId)
                setProdukId(data.order_items[0].produk_id)
                setIdPenjual(data.order_items[0].penjual_id)
                setJenisPengirimanId(data.pengiriman.jenis_pengiriman.id)
                setNamaProduk(data.order_items[0].produk.nama)
                setNamaToko(data.order_items[0].produk.penjual.nama_toko)
                setNamaPasar(data.order_items[0].pasar.nama)
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPembeli]);

    useEffect(() => {
        check()
    }, [ratingProduk, ratingPenjual, ratingJenisPengiriman])
    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
    }, [token, idPembeli])

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananSelesai={true}
                    />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    {loading == true ?
                        <div className='flex flex-col justify-center items-center'>
                            <div className='loadingContainer '>
                                <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                <div className="loadingAnimation"></div>
                            </div>
                        </div>
                        :
                        <>
                            <div className='flex self-center text-center items-center'>
                                <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                                    <Link href={'/Pesanan/PesananSelesai'} className='text-[14px] mr-1 no-underline'>Pesanan selesai / </Link>
                                    <Link href={`/Pesanan/PesananSelesai/${id}`}><div className='text-[14px] mr-1 no-underline'>Detail / </div></Link>
                                    <div className='text-[14px] mr-1 no-underline'>Rating</div>
                                </div>
                            </div>
                            <div className='flex flex-col '>
                                <div className='flex justify-center items-center flex-col font-roboto '>
                                    <div className='text-lg font-semibold'>Nilai Pesanan</div>
                                </div>
                            </div>
                            <div className=''>
                                <div className='w-full grid grid-rows-none gap-3 pl-6 pr-6 pt-6 pb-6 font-roboto'>
                                    <form onSubmit={handleSubmitRating}>
                                        <div className=''>
                                            {/* kenaca kencanayo teng neng neng neng neng 
                                            ngeeeng kluar bntr*/}
                                            <div className='mb-2 p-3'>
                                                <h3 className='flex justify-center w-full text-white rounded-2xl font-medium bg-red-400 py-2 mb-2'>{namaProduk}</h3>
                                                <div className='bg-[#EDEDED] mb-2'>

                                                    <div className='flex justify-evenly bg-[#C6C6C6] px-2 py-2'>
                                                        <div className='flex'>
                                                            <Image src={Market} alt='' />
                                                            <div className='text-md font-bold ml-3'>{namaToko}</div>
                                                        </div>
                                                        <div className='flex'>
                                                            <Image src={Location} alt='' />
                                                            <div className='text-md font-bold ml-3'>{namaPasar}</div>
                                                        </div>
                                                    </div>
                                                    <div className='px-2 py-3 mb-2 bg-white'>
                                                        <h3 className='font-semibold mb-2'>
                                                            Kualitas Produk
                                                        </h3>
                                                        <Rating
                                                            name="simple-controlled size-large"
                                                            value={ratingProduk}
                                                            onChange={(event, newValue) => {
                                                                setRatingProduk(newValue);
                                                            }}
                                                        />
                                                    </div>
                                                    <div className='p-2 bg-white mt-2 mb-2'>
                                                        <p className='text-gray-500 text-sm mb-2'>Tambahkan 255 karakter dengan 1 foto</p>
                                                        <label htmlFor="imageInput" className='w-[220px] flex justify-center text-merah-lihat-semua font-semibold mb-4' style={{ cursor: 'pointer' }}>
                                                            <div className='ml-10 bg-white border-[1px] border-merah-lihat-semua w-[320px] h-[180px] flex flex-col justify-center  items-center'>
                                                                <div className='w-[228px] h-[228px] flex justify-center items-center mt-4 ml-4 mr-4'>
                                                                    {
                                                                        selectedImage ?
                                                                            <img src={(URL.createObjectURL(selectedImage))} alt="Selected" className='w-[100px] h-[100px]' />
                                                                            :
                                                                            <Image width={100} height={100} src={Camera} alt="Selected" className='w-[80px] h-[80px]' />
                                                                    }
                                                                </div>
                                                                <div className='w-[228px] h-[44px] rounded-lg font-medium text-[14px] flex justify-center items-center ml-4 mr-4 '
                                                                    style={{ backgroundColor: 'white', color: '#48872A', cursor: 'pointer' }}>
                                                                    <input type='file'
                                                                        id="imageInput"
                                                                        onChange={handleImageUpload}
                                                                        style={{ display: 'none' }}
                                                                    />
                                                                    <span className='w-[220px] flex justify-center text-merah-lihat-semua font-semibold mb-4' style={{ cursor: 'pointer' }}>Tambah Foto</span>
                                                                </div>
                                                            </div>
                                                        </label>

                                                        <p className='text-gray-500 text-sm mb-2 mt-2'>Maksimal ukuran foto 500 kb</p>
                                                        <textarea className='bg-[#E0E0E0] w-full p-2 h-[150px] outline-none mb-2' onChange={(e) => {
                                                            setComment(e.target.value)
                                                        }}
                                                            placeholder='Bagikan penilaianmu dan bantu pengguna lain membuat pilihan yang lebih baik!' />
                                                        <p className='flex justify-end text-sm text-gray-500'>Maksimal 255 karakter</p>
                                                    </div>
                                                    <div className='bg-white px-2 py-3 mb-2'>
                                                        <h3 className='font-semibold mb-1'>
                                                            Pelayanan Pedagang
                                                        </h3>
                                                        <Rating
                                                            name="simple-controlled size-large"
                                                            value={ratingPenjual}
                                                            onChange={(event, newValue) => {
                                                                setRatingPenjual(newValue);
                                                            }}
                                                        />
                                                    </div>
                                                    <div className='px-2 py-3 bg-white'>
                                                        <h3 className='font-semibold mb-1'>
                                                            Kecepatan Jasa Kirim
                                                        </h3>
                                                        <Rating
                                                            name="simple-controlled size-large"
                                                            value={ratingJenisPengiriman}
                                                            onChange={(event, newValue) => {
                                                                setRatingJenisPengiriman(newValue);
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                                <button
                                                    className="w-full font-normal px-3 py-2  text-white rounded-full "
                                                    type="submit" disabled={isDisable} style={{ backgroundColor: colorButton }}
                                                >
                                                    Kirim
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {/* } */}
                            </div>
                        </>
                    }
                </div>
            </div >
            <Footer />
        </>
    )
}

export default page