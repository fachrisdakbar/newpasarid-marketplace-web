'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '../../../../Accordion/accordion'
import Pasarid from "../../../../../public/LogoPasarIdBnW.svg"
// import Lunas from "../../../../../public/LUNAS.svg"
import Link from 'next/link'
import Image from 'next/image'
import '../../../../globals.css'
// import Delivery from '../../../../public/delivery.svg'   
// import Pickup from '../../../../public/pickup.svg'
// import Market from '../../../../public/toko.svg'
// import Location from '../../../../public/location.svg'
// import Saldo from '../../../../public/saldoAyam.svg'
// import Briva from '../../../../public/Briva.svg'
import { useParams } from 'next/navigation'
import { url } from 'inspector'


export const page = () => {
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState();
    const [idPembeli, setIdPembeli] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [totals, setTotal] = useState<number>();
    const [totalProduk, setTotalProduk] = useState<number>();

    const [token, setToken] = useState('');

    const { id } = useParams()

    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);
        const time = formattedDate.match(/\d+:\d+/)
        return formattedDate;
    }

    const formatNumber = (value: any) => {
        if (value !== undefined && value !== null) {
            return value.toLocaleString('id-ID', {
                useGrouping: true,
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
                minimumIntegerDigits: 1,
                style: 'decimal',
            });
        }
        return ''; // Return an empty string if value is undefined or null
    }

    async function fetchData() {
        try {
            const response = await fetch(`/api/getUser`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res.data
            console.log(data);
            if (data) {
                setLoading(false)
            }
            else if (data == undefined) {
                setLoading(true)
            }
            setIdPembeli(data.pembeli.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?where[]=id,${id}&with[]=pengiriman.jenisPengiriman&with[]=pembeli&with[]=orderItems&with[]=pembayaran.jenisPembayaran&with[]=orderItems.penjual&with[]=orderItems.produk&where[]=pembeli_id,${idPembeli}&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setOrders(data[0]);
                if (!data[0].store_order_id) {
                    setIsOrder(false)
                }
                else if (data[0].store_order_id) {
                    setIsOrder(true)
                }
                console.log(data);

                // total produk
                let total = 0;
                data[0]?.order_items.forEach((item: any) => {
                    const subtotal = item.jumlah * item.produk.harga;
                    console.log("subtotal", subtotal);
                    total += subtotal;
                    console.log("total ", total)
                    setTotalProduk(total)
                });
                console.log(total);


                // Add pengiriman.biaya to the total
                total += data[0]?.pengiriman?.biaya || 0;
                console.log("total+ongkir", total);
                
                // substract diskon app, diskon ongkir, cashback
                let totalAll = 0
                totalAll = total - data[0]?.promo_value - data[0]?.pengiriman?.promo_value
                console.log('Total:', totalAll);
                setTotal(totalAll);

                for (let i = 0; i < data.length; i++) {
                    console.log(data[i].order_items.length);
                }

                if (!data[0].store_order_id) {
                    setIsOrder(false)
                }
                else if (data[0].store_order_id) {
                    setIsOrder(true)
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPembeli]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
    }, [token, idPembeli])
    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananSelesai={true}
                    />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    <div className='flex self-center text-center items-center'>
                        <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                            <Link href={'/Pesanan/PesananSelesai'} className='text-[14px] mr-1 no-underline'>Pesanan selesai / </Link>
                            <Link href={'/Pesanan/PesananSelesai/Detail'} className='text-[14px] mr-1 no-underline'>Detail / </Link>
                            <div className='text-[14px] mr-1 no-underline'>Struk</div>
                        </div>
                    </div>
                    <div className=' top-10 flex justify-center'>
                        <Image src={Pasarid} width={204} height={109} alt={''} />
                    </div>
                    <div className='flex justify-center mt-4'>
                        <h2 className='font-bold text-xl'>Struk Transaksi New Pasar.id</h2>
                    </div>
                    <div className='w-full mt-5 flex px-4 justify-between'>
                        <div>No. Order</div>
                        <div className='font-semibold'>{orders?.['store_order_id']}</div>
                    </div>
                    <div className='w-full mt-2 flex px-4 justify-between'>
                        <div>Tanggal Pembelian</div>
                        <div className='font-semibold'>{(() => {
                            const formattedDate = formatDate(orders?.['created_at']);
                            const justDate = formattedDate.split(' ')
                            return formattedDate.replace('pukul', '').replace(/\./g, ':');

                        })()}
                        </div>
                    </div>
                    {/* pembatas */}
                    <div className='h-2 bg-gray-200 my-3'></div>
                    {/* pembatas */}

                    {/* rincian pengiriman */}
                    {
                        orders?.['pengiriman_id'] ?
                            <>
                                <div>
                                    <h3 className='font-semibold px-4'>Rincian Pengiriman</h3>
                                    <div className='flex justify-items-start px-4 mt-2'>
                                        <div>Kurir</div>
                                        <div className='font-semibold ml-40'>{orders?.['pengiriman']['jenis_pengiriman']['title']}</div>
                                    </div>
                                    <div className='flex justify-items-start px-4 mt-2'>
                                        <div>Alamat</div>
                                        <div className='ml-36'>
                                            <div className='font-semibold'>{orders?.['pengiriman']['alamat_nama_penerima']}</div>
                                            <div>{orders?.['pembeli']['phone']}</div>
                                            <div className='text-gray-400'>{orders?.['pengiriman']['alamat_nama']}</div>
                                        </div>
                                    </div>
                                </div>
                            </>
                            :
                            <div className='px-4'>Loading Data...</div>

                    }
                    {/* rincian pengiriman */}

                    {/* pembatas */}
                    <div className='h-2 bg-gray-200 my-3'></div>
                    {/* pembatas */}

                    {/* Rincian Pesanan */}
                    <div>
                        <h3 className='font-semibold px-4'>Rincian Pesanan</h3>
                        {/* abaikan merahnya */}
                        {orders?.['order_items']?.map((item: any, index: any) => (
                            <div key={index}>
                                <div className='flex justify-between px-4 mt-2'>
                                    <div className='flex'>
                                        <div className='font-semibold'>{item.jumlah}x</div>
                                        <div className='ml-4'>{item.produk.nama}</div>
                                    </div>
                                    <div className='font-semibold'>{"Rp " + formatNumber(item.produk.harga * item.jumlah)}</div>
                                </div>
                            </div>
                        ))}
                        {/* Rincian Pesanan */}
                    </div>
                    {/* pembatas */}
                    <div className='h-2 bg-gray-200 my-3'></div>
                    {/* pembatas */}

                    {/* rincian pembayaran */}
                    <div className='lunas ' >
                        <h3 className='font-semibold px-4'>Rincian Pembayaran</h3>
                        {
                            orders?.['pembayaran_id'] ?
                                <><>
                                    <div className='flex justify-between px-4 mt-2'>
                                        <div className=''>Metode Pembayaran</div>
                                        <div className='font-semibold'>{orders?.['pembayaran']['jenis_pembayaran']['title']}</div>
                                    </div>
                                </><div className='flex justify-between px-4 mt-2 breaking-border-top'>
                                        <div className=''>Subtotal untuk Produk</div>
                                        <div className='font-semibold'>{"Rp " + formatNumber(totalProduk)}</div>
                                    </div><div className='flex justify-between px-4 mt-2'>
                                        <div className=''>Biaya Pengiriman</div>
                                        <div className='font-semibold'>{"Rp " + formatNumber(orders?.['pengiriman']['biaya'])}</div>
                                    </div><div className='flex justify-between px-4 mt-2'>
                                        <div className=''>Diskon Pengiriman</div>
                                        {
                                            orders?.['pengiriman_id']['promo_value'] ?
                                                <div className='font-semibold'>{"Rp " + formatNumber(orders?.['pengiriman']['promo_value'])}</div>
                                                :
                                                <div className='font-semibold'>-</div>
                                        }
                                    </div><div className='flex justify-between px-4 mt-2'>
                                        <div className=''>Diskon</div>
                                        {
                                            orders?.['promo_value'] ?
                                                <div className='font-semibold'>{"Rp " + formatNumber(orders?.['promo_value'])}</div>
                                                :
                                                <div className='font-semibold'>-</div>
                                        }
                                    </div><div className='flex justify-between px-4 mt-2 breaking-border-bottom'>
                                        <div className=''>Cashback</div>
                                        {
                                            orders?.['cashback_promo_value'] ?
                                                <div className='font-semibold'>{"Rp " + formatNumber(orders?.['cashback_promo_value'])}</div>
                                                :
                                                <div className='font-semibold'>-</div>
                                        }
                                    </div><div className='flex justify-between px-4 mt-3'>
                                        <div className='font-semibold'>Total Pembayaran</div>
                                        <div className='font-semibold'>{"Rp " + formatNumber(totals)}</div>
                                    </div></>
                                :
                                <div className='px-4'>Loading Data ...</div>
                        }
                    </div>
                    {/* rincian pembayaran */}

                    {/* button */}
                    <div className='flex justify-center mt-4'>
                        <button type='submit' className="w-[200px] h-[40px]  font-semibold text-sm text-border-hijau rounded-full bg-border-white border-2 border-hijau cursor-pointer flex justify-center items-center">
                            <div>Simpan</div>
                        </button>
                        <button type='submit' className="w-[200px] h-[40px] font-semibold text-sm text-white rounded-full bg-border-hijau cursor-pointer flex justify-center items-center ml-10">
                            <div>Bagikan</div>
                        </button>
                    </div>




                    <div className='pt-3 pb-3 pl-6 pr-6'>
                        {/* {loading == true ?
                            <div className='flex flex-col justify-center items-center'>
                                <div className='loadingContainer '>
                                    <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                    <div className="loadingAnimation"></div>
                                </div>
                            </div>
                            : */}
                        <>

                        </>
                        {/* } */}
                    </div>
                </div>
            </div >
            <Footer />
        </>
    )
}


export default page
