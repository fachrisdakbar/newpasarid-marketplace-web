"use client"
require('dotenv').config();
import { useParams, useSearchParams } from 'next/navigation'
import React from 'react'
import Link from 'next/link';
import { useEffect, useState } from 'react';
import '../globals.css'
import { useRouter } from 'next/navigation';
import Image from 'next/image';
import HomeContent from '../components/HomeContent/HomeContent';
import Profile from '../Profile/page'
import Swal from 'sweetalert2';
import Loading from './loading/page'


export default function login({
  // isLogin = false,
  thisLogin = false,
  thisProductDetail = false,
}) {
  const axios = require('axios')
  const [reload, setReload] = useState("none");
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [refreshCaptcha, setRefrechCaptcha] = useState(0);
  const handleTogglePassword = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const device_name = "dashboard";
  const router = useRouter();

  const [captcha, setCaptcha] = useState('');
  const [key, setKey] = useState('');
  const [imageCaptcha, setImageCaptcha] = useState("");

  const [token, setToken] = useState('');
  const [isSessionEst, setIsSessionEst] = useState(false)

  const [isDisable, setIsDisable] = useState(true);
  const [isProfile, setProfile] = useState(false);
  const [passAlert, setPassAlert] = useState('none');
  const [userAlert, setUserAlert] = useState('none');
  const [buttonMasuk, setButtonMasuk] = useState('flex');
  const [isLogin, setIsLogin] = useState(localStorage.getItem('isLoggedIn') == 'true');
  const [isCheckedUsername, setIsCheckedUsername] = useState(false);
  const [isCheckedPasword, setIsCheckedPassword] = useState(false);
  const [isFilledUsername, setIsFilledUsername] = useState(false);
  const [isFilledPassword, setIsFilledPassword] = useState(false);
  const [isFilledCaptcha, setIsFilledCaptcha] = useState(false);
  const [colorButton, setColorButton] = useState('#D4D4D4');
  const [loading, setLoading] = useState(false);


  const handlePhoneChange = (event: { target: { value: any; }; }) => {
    const validPrefixes = [
      '62831', '62832', '62833', '62838', '62895', '62896', '62897', '62898', '62899', '62817',
      '62818', '62819', '62859', '62878', '62877', '62814', '62815', '62816', '62855', '62856',
      '62857', '62858', '62812', '62813', '62852', '62853', '62821', '62823', '62822', '62851',
      '62811', '62881', '62882', '62883', '62884', '62885', '62886', '62887', '62888', '62889'
    ];

    const phoneConst = event.target.value;
    console.log("ini angka " + phoneConst);


    // starts angka 8
    const doesNotStartWith8 = !phoneConst.startsWith('628');

    const isValidPrefix = validPrefixes.some((prefix) =>
      phoneConst.startsWith(prefix)
    );

    const isLengthValid = phoneConst.length >= 11 && phoneConst.length <= 16;

    const isValidPhoneNumber = !doesNotStartWith8 && isValidPrefix && isLengthValid;

    setUsername(phoneConst);

    console.log(isValidPhoneNumber);

    setIsFilledUsername(isValidPhoneNumber);
    setIsCheckedUsername(isValidPhoneNumber);

    setUserAlert(isValidPhoneNumber ? 'none' : 'block')

    setIsDisable(!isValidPhoneNumber);
  };
  const passwordComplexity = (event: { target: { value: any; }; }) => {

    const passwordConst = event.target.value;
    const check = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;
    const isPasswordComplex = check.test(passwordConst);
    console.log(check.test(passwordConst));

    console.log("ini pw " + passwordConst);
    setPassword(passwordConst)
    setPassAlert(isPasswordComplex ? 'none' : 'block');
    setIsDisable(!isPasswordComplex);
    setIsFilledPassword(isPasswordComplex);
    setIsCheckedPassword(isPasswordComplex);

  };

  const captchaCheck = (event: { target: { value: any; }; }) => {
    const captchaConst = event.target.value;
    setCaptcha(captchaConst);
    const isLengthValidCaptcha = captchaConst.length == 6;
    setIsFilledCaptcha(isLengthValidCaptcha);
  };

  // submit form and captcha
  const handleSubmit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    setLoading(true)
    try {
      const response_login = await fetch('/api/login-username', {
        method: 'POST',
        mode: 'no-cors',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username,
          password,
          device_name,
          key,
          captcha
        }),
      });

      if (response_login.ok) {
        console.log('Login successful!')
        const responseData = await response_login.text()
        console.log(responseData);
        const check = /^[0-9]{4}\|/;
        console.log(check.test(responseData));
        setToken(responseData)
        localStorage.setItem('token', responseData)
        console.log("LocalStorage established")

        if (check.test(responseData)) {
          localStorage.setItem('isLoggedIn', 'true')
          displayComponentProfile()
          setButtonMasuk('none')
          setCaptcha('')
          setUsername('')
          setPassword('')
          setIsPasswordVisible(false)
          setLoading(false)
          if (loading == false) {
            setReload('none')
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Berhasil masuk',
              showConfirmButton: false,
              width: 350,
              timer: 2000
            })
          }
        }
        else {
          setReload('block')
          setIsPasswordVisible(false)
          setRefrechCaptcha(refreshCaptcha + 1)
          localStorage.setItem('isLoggedIn', 'false')
          setLoading(false)
          Swal.fire({
            icon: 'error',
            title: 'Login gagal!',
            text: 'Coba cek kembali data anda.',
            timer: 2000,
            width: 350,
            showConfirmButton: false,
          });
          if (localStorage.getItem('isLoggedIn') == 'false') {
            localStorage.removeItem('isLoggedIn')
            localStorage.removeItem('token')
          }
        }

      } else {
        console.log('Login failed!');
        console.log("response is not ok")
        console.error('Error: ', response_login.statusText);
        // location.reload();
      }
    } catch (error) {
      console.log('An error occurred during login:', error);
    }
  };

  const displayComponentProfile = () => {
    updateStateFromLocalStorage()
    setProfile(true)
  }

  const check = () => {
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;
    if (isFilledUsername && isCheckedUsername && isFilledPassword && isCheckedPasword && isFilledCaptcha) {
      setColorButton('#EF3F3A')
      setIsDisable(false)
    }
    else {
      setColorButton('#D4D4D4')
      setIsDisable(true)
    }
  };

  useEffect(() => {
    if (localStorage.getItem('isLoggedIn') == 'false') {
      localStorage.removeItem('isLoggedIn')
      localStorage.removeItem('token')
    }
    check();

  }, [isDisable, isFilledUsername, isCheckedUsername, isFilledPassword, isCheckedPasword, isFilledCaptcha]);

  useEffect(() => {
    fetch('/api/captcha')
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        setKey(data.data['key']);
        setImageCaptcha(data.data['img'])
      })
      .catch((error) => {
        console.error('Terjadi kesalahan saat fetching data:', error);
      });
  }, [isProfile, refreshCaptcha])


  const updateStateFromLocalStorage = () => {
    const token = localStorage.getItem('token');
    const isLoggedIn = localStorage.getItem('isLoggedIn');

    if (token && isLoggedIn == 'true') {
      setIsLogin(true);
      setProfile(true)
    } else {
      setIsLogin(false);
      setButtonMasuk('flex')
      setProfile(false)
    }
    console.log(isLoggedIn);

  };

  useEffect(() => {
    const updateStateFromLocalStorage = () => {
      const token = localStorage.getItem('token');
      const isLoggedIn = localStorage.getItem('isLoggedIn');

      if (token && isLoggedIn == 'true') {
        setIsLogin(true);
        setProfile(true)
      } else {
        setIsLogin(false);
        setButtonMasuk('flex')
        setProfile(false)
      }
    };

    updateStateFromLocalStorage();
    const handleStorageChange = () => {
      updateStateFromLocalStorage();
    };

    window.addEventListener('storage', handleStorageChange);
    return () => {
      window.removeEventListener('storage', handleStorageChange);
    };
  }, [isProfile]);

  const closeModal = () => {
    setIsPasswordVisible(false)
    setReload('none')
    setUsername('')
    setPassword('')
    setCaptcha('')
  }

  return (
    <>
      <div className='btn-login h-28 w-24 flex items-center justify-center'>
        {thisLogin && !isProfile &&
          <div onClick={() => setReload("block")}
            className='no-underline cursor-pointer btn-login-container font-roboto font-medium w-[70px] h-7 flex justify-center items-center text-white mr-5'
            style={{ display: buttonMasuk }} >
            Masuk</div>
        }
        {
        thisProductDetail == false &&
          isProfile && isLogin &&
          <Profile />
        }
        {thisProductDetail &&
          <div className=''>
            <div className='ml-[70px] bg-warna-text-harga-produk w-[178px] h-[44px] rounded-[8px] text-white font-roboto text-[16px] font-medium flex justify-center items-center cursor-pointer mt-[38px] mb-[48px]'
              onClick={() => setReload('block')}>
              Beli</div>
          </div>
        }

        <section className='bg-opacity-50 bg-black fixed inset-0 z-30 ' style={{ display: reload }}>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
          <div className='flex justify-center h-[100vh]'>
            {loading &&
              <Loading />
            }
            <div className="flex items-center justify-center">
              <div className="px-6 py-9 border bg-white rounded-lg w-[376px]">
                <div className="text-left mb-5 flex justify-between">
                  <h2 className="text-[32px] font-bold text-abu-abu">Login</h2>
                  <svg onClick={(e) => closeModal()} className='self-center cursor-pointer' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                  </svg>
                </div>
                <form onSubmit={handleSubmit}>
                  <div className="mb-1 mt-5 px-3 py-2 border rounded-md">
                    <input
                      className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans no-spinners"
                      type="number"
                      id="username"
                      value={username}
                      onChange={handlePhoneChange}
                      placeholder="Nomor Handphone" required
                    />
                  </div>
                  <div className='mb-3'>
                    <p className='text-gray-400 text-[10px] font-sans'>Contoh: 628123456789</p>
                  </div>
                  <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: userAlert }}>Nomor ponsel belum sesuai.</div>
                  <div className="mb-3 flex justify-between border rounded-md px-3 py-2">
                    <input
                      className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                      type={isPasswordVisible ? 'text' : 'password'}
                      id="password"
                      value={password}
                      onChange={passwordComplexity}
                      placeholder="Kata Sandi" required
                    />
                    <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePassword}>
                      {isPasswordVisible ? 'visibility' : 'visibility_off'}
                    </span>

                  </div>
                  <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: passAlert }}>Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol</div>
                  {/* <Captcha /> */}
                  <div className="mb-3">
                    {imageCaptcha != "" ? (
                      <div>
                        <img src={imageCaptcha} alt="Captcha" />
                        {/* <p>Sensitive: {data.sensitive ? 'true' : 'false'}</p> */}
                        {/* <p>Key: {captchaData.key}</p> */}
                      </div>
                    ) : (
                      <p>Mengambil data captcha...</p>
                    )}
                  </div>
                  <div className="mb-3 flex justify-between border rounded-md px-3 py-2">
                    <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                      type="text" id="captcha"
                      value={captcha}
                      onChange={captchaCheck}
                      placeholder="Captcha" required />
                  </div>
                  <div className='mb-3'>
                    <p className='text-right  text-[12px]'><a href="/lupaPassword" className='no-underline text-red-600'>Lupa Password</a></p>
                  </div>
                  <div className="mb-3">
                    <button
                      className="w-full font-normal px-3 py-2  text-white rounded-full "
                      type="submit" disabled={isDisable} style={{ backgroundColor: colorButton }}
                    >
                      Masuk
                    </button>
                  </div>
                  <div className="mb-3">
                    <p
                      className="text-gray-400 text-center font-medium"
                    >
                      Atau
                    </p>
                  </div>
                  <div className="mb-3">
                    <a
                      className="w-full no-underline text-center font-normal px-3 py-2 bg-gray-100 text-red-500 rounded-full hover:bg-gray-200 text-decoration:none"
                      type="submit" href={'/register'}>

                      Daftar
                    </a>
                  </div>
                  <div>
                    <p className='text-[10px] text-gray-400 text-center'>
                      Dengan mendaftar, saya menyetujui <Link href="/s&k" className='text-red-600 no-underline'>Syarat dan Ketentuan</Link> serta <Link href="/kebijakan-privasi" className='text-red-600 no-underline'>Kebijakan Privasi</Link>.
                    </p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>

  );
};
