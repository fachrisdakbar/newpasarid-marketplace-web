"use client"
import { useRouter } from 'next/navigation';
import React, { useEffect, useState } from 'react'
// import { useNavigate } from 'react-router-dom';

interface SMSVerificationRequest {
  phone: string;
  type: string;
  key: string;
  captcha: string;
  send_type: string;
}
interface CaptchaData {
  img: string;
  sensitive: boolean;
  key: string;
}

export const page = ({
  isDisable= true,
  colorButton = '#D4D4D4'
  // showProps = false,
  // reload = 'none'
}) => {
  const [reload, setReload] = useState("none");
  const [phone, setPhone] = useState('');
  const [showModal, setShowModal] = useState(true);
  const [code, setCode] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [error, setError] = useState<string>('');
  // captcha
  const [key, setKey] = useState('');
  const [captcha, setCaptcha] = useState('');

  // key transfer from captcha api to register api
  useEffect(() => {
    fetch("/api/captcha")
      .then((response) => response.json())
      .then((data) => {
        // fetch key from captcha

        setKey(data.key);
        setCaptcha(data.captcha);
      }).catch(error => console.error('Error fetching key:', error));
  }, []);

  const handlePhoneChange = (event: { target: { value: any; }; }) => {
    setPhone(event.target.value);
  };

  const handleRegisterClick = () => {
    setShowModal(true);
  };
  const handleModalYesClick = async () => {
    const navigate = useRouter();
    const type = "pembeli";
    const send_type = "sms";
    try {
      const smsVerificationData: SMSVerificationRequest = {
        phone,
        key,
        captcha,
        type,
        send_type,
      };
      const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/sms-verification`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(smsVerificationData),
      });
      if (response.ok){
          console.log ("SMS-Verification passed")
          // navigate.push('/register/inputCode')
      } else {
        throw new Error('Failed to verify SMS code');
      }
      

    } catch (err) {
      setError('Failed to verify SMS code. Please try again.');
    }
  };

  return (
    <>
        <button className="w-full text-center font-normal px-3 py-2 bg-[#D4D4D4] text-[#FFFFFF] rounded-full hover:bg-gray-400"
          type="submit" onClick={() => setReload("block")} disabled={isDisable} style={{ backgroundColor: colorButton}}>Daftar</button>
        {/* onClick={handleRegisterClick} */}
        <section className='w-full h-full bg-opacity-50 bg-black fixed inset-0 ' style={{ display: reload }}>
          
          {/* <div className="container "> */}
            <div className="flex justify-center pt-96">
              <div className="w-[280px] bg-white border rounded-xl px-[16px] py-[16px]">
                <p className='text-[16px] flex justify-center text-center font-sans font-semibold'>Verifikasi Nomor HP</p>
                <p className='mt-[18px] align-middle text-[14px] w-[248px] font-normal text-center'>Apakah nomor yang dimasukan sudah benar?</p>
                <div className="mt-[24px] flex justify-between">
                  <button
                    className="w-[116px] h-[32px] font-normal text-[12px] bg-gray-100 text-red-500 rounded-full hover:bg-gray-200"
                    type="submit" onClick={(e) => setReload('none')}
                  // onClick={handleModalNoClick}
                  >
                    Ubah
                  </button>
                  <button
                    className="w-[116px] h-[32px] text-[12px] font-normal bg-red-500 text-white rounded-full hover:bg-red-600"
                    type="submit" onClick={handleModalYesClick}>
                    Benar
                  </button>
                </div>
              </div>
            </div>
          {/* </div> */}
        </section>
    </>
  )
}

export default page
