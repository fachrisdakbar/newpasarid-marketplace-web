"use client"
// import { Link } from 'react-router-dom';
import '../../globals.css'
import Image from 'next/image';
import Trolley from "../../../public/Group 97.svg"
import Pasarid from "../../../public/logoPasarId.svg"
import logo_indonesia from "../../../public/logo_indonesia.svg"
import { useEffect, useState } from 'react';
// import Modal from "./components/Modal"
import Link from 'next/link';
import Password from './components/Password';
import { useRouter } from 'next/navigation';
import { useSearchParams } from 'next/navigation';
// import Captcha from './components/Captcha';
import { getMyStatePhoneProps } from '../../global';
import Swal from 'sweetalert2';

export default function registerUsername() {

  const router = useRouter()
  const [error, setError] = useState('')
  const [name, setName] = useState('');
  const [nameAlert, setNameAlert] = useState('none');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [passAlert, setPassAlert] = useState('none');
  const [numAlert, setNumAlert] = useState('none');
  const [isDisable, setIsDisable] = useState(true);
  const [isNumChecked, setIsNumChecked] = useState(false);
  const [colorButton, setColorButton] = useState('#D4D4D4');
  const [isChecked, setIsChecked] = useState(false);
  const [isCheckedName, setIsCheckedName] = useState(false);
  const [isCheckedPasword, setIsCheckedPassword] = useState(false);
  const [SamePassAlert, setSamePassAlert] = useState('none');
  const [isPasswordsMatch, setIsPasswordsMatch] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [isPasswordConfirmVisible, setIsPasswordConfirmVisible] = useState(false);
  const [type, setType] = useState('');
  const [typeAlert, setTypeAlert] = useState('none');
  const [dropdownCheck, setDropdownCheck] = useState(false)


  const validateButton = () =>{
    const check = /^[A-Za-z]+( [A-Za-z]+)*$/;
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;
    console.log(isChecked);
    if(name != null && name != "" && check.test(name) && password != null && password != "" && passwordRegex.test(password) && confirmPassword != null && confirmPassword != "" && type != "" && isChecked == true ){
      setIsDisable(true);
    }else{
      setIsDisable(false);
    }
  } 


  const nameCheck = (event: { target: { value: any; }; }) => {
    const nameConst = event.target.value;
    const check = /^[A-Za-z]+( [A-Za-z]+)*$/;
    const isNameString = check.test(nameConst);
    setName(nameConst)
    setNameAlert(isNameString ? 'none' : 'block')
  }

  const passwordComplexity = (event: { target: { value: any; }; }) => {

    const passwordConst = event.target.value;
    const check = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;
    const isPasswordComplex = check.test(passwordConst);
    console.log(check.test(passwordConst));

    console.log("ini pw " + passwordConst)
    setPassword(passwordConst)
    setPassAlert(isPasswordComplex ? 'none' : 'block')

  };

  const handleConfirmPasswordChange = (event: { target: { value: any; }; }) => {
    const newConfirmPassword = event.target.value;
    setConfirmPassword(newConfirmPassword);

    // Check if passwords match
    const matching = (newConfirmPassword === password)
    setIsPasswordsMatch(matching)
    setSamePassAlert(matching ? 'none' : 'block')
  };

  const handleTogglePassword = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };
  const handleTogglePasswordConfirm = () => {
    setIsPasswordConfirmVisible(!isPasswordConfirmVisible);
  };
  const handleDropdownChange = (event: { target: { value: any; }; }) => {
    const typeConst = event.target.value;
    console.log("type Const :", typeConst);
    
    const check = (typeConst === 'penjual' || typeConst === 'pembeli')
    setType(typeConst);
    setIsDisable(!check)
    setDropdownCheck(check)
    setTypeAlert(check ? 'none' : 'block')
  };

  const checkBox = async () => {
    setIsChecked(true)
  }

  const check = () => {
    if (isChecked && isCheckedName && isCheckedPasword && isPasswordsMatch && dropdownCheck) {
      setColorButton('#EF3F3A')
      setIsDisable(false)
    }
    else {
      setIsDisable(true)
    }
  }
  const handleModalYesClick = async (event: any) => {
    event.preventDefault()
    try {
      const smsVerificationData = {
        username: "62" + getMyStatePhoneProps(),
        password,
        name,
        type : type,
      };
      const response = await fetch(`/api/register-username`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(smsVerificationData),
      });
      console.log(await response.json())
      if (response.ok) {
        console.log("Register succesfull")
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Berhasil daftar!',
          showConfirmButton: true,
          width: 450,
        }).then(() => {
          router.push('/')
        })

      } else {
        console.log("SMS Verification failed")
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Gagal daftar!',
          showConfirmButton: true,
          width: 450,
        }).then(() => {
          router.push(`/register`);
        })
        throw new Error('Failed to verify SMS code');
      }


    } catch (err) {
      setError('Failed to verify SMS code. Please try again.');
    }
  };


  // useEffect(() => {
  //   check()
  // }, [ isChecked, isDisable, isCheckedName, isCheckedPasword, isPasswordsMatch])

  useEffect(() => {
    // check()
    validateButton();
  }, [ isChecked, name,password,confirmPassword])



  return (
    <section className="min-h-screen flex items-center inset-0 z-30" >
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
      <div className="container ">
        <div className='absolute inset-x-0 top-10 flex justify-center'>
          <Image src={Pasarid} width={204} height={109} alt={''} />
        </div>
        <div className="flex justify-evenly items-center">
          <Image src={Trolley} width="620" height="468" alt={''} className='items-center' />
          <div className="px-[24px] py-[50px] w-[376px]   bg-white rounded-lg shadow-xl ">
            <form onSubmit={handleModalYesClick}>
              <div className="text-left">
                <h2 className="text-[32px] font-bold text-gray-600">Daftar Pengguna</h2>
              </div>
              <div className="mb-1 mt-[10px] flex border rounded-lg">
                {/* logo Indonesia starts */}
                <div className='self-center pl-[11px]'>
                  <Image src={logo_indonesia} alt={''} />
                </div>
                {/* Logo Indonesia ends */}
                <p className='self-center py-[16px] ml-[4px] text-[14px] font-sans text-[#1B1C1E]'>+62</p>
                <p className='self-center px-[8px] mb-1 text-lg font-sans text-[#858585]'>&#9474;</p>
                <input
                  className="py-[16px] text-[14px] bg-white font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners"
                  type="number" value={getMyStatePhoneProps()} disabled
                  placeholder="81234567890"
                >
                </input>
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: numAlert }}>Nomor ponsel belum sesuai</div>
              <div className='mb-2'>
                <p className='text-gray-400 text-[10px] font-sans'>Contoh: 8123456789</p>
              </div>
              <div className="mb-3 flex justify-between border rounded-md px-3 py-2">
                <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                  type="text" id="name"
                  // value={name}
                  onChange={nameCheck}
                  placeholder="Name" required />
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: nameAlert }}>Tolong hanya masukkan huruf</div>
              {/* password starts */}
              <div className="mb-3 flex justify-between border rounded-lg px-[6px]">
                <input
                  className="w-full py-2 px-[10px] hover:border-indigo-300 focus:outline-none focus:ring-0 text-[14px] font-sans"
                  type={isPasswordVisible ? 'text' : 'password'}
                  id="password"
                  value={password}
                  onChange={passwordComplexity}
                  placeholder="Kata Sandi" required
                />
                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePassword}>
                  {isPasswordVisible ? 'visibility' : 'visibility_off'}
                </span>
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: passAlert }}>Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol</div>
              <div className="mb-3 flex justify-between border rounded-lg px-[6px]">
                <input
                  className="w-full py-2 px-[10px] hover:border-indigo-300 focus:outline-none focus:ring-0 text-[14px] font-sans"
                  type={isPasswordConfirmVisible ? 'text' : 'password'}
                  id="password"
                  onChange={handleConfirmPasswordChange}
                  placeholder="Konfirmasi Kata Sandi" required
                />
                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePasswordConfirm}>
                  {isPasswordConfirmVisible ? 'visibility' : 'visibility_off'}
                </span>
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: SamePassAlert }}>Password tidak sama</div>
              {/* password selesai */}
              {/* <div className='text-[10px] text-warna-text-harga-produk'>Nomor kurang dari 12</div>
              <div className='text-[10px] text-warna-text-harga-produk'>Nomor lebih dari 14</div> */}
              <div className="">
                <select id="dropdown" className=' text-sm font-sans w-full mb-3 flex justify-between border rounded-md px-2 py-2 focus:outline-none focus:ring-0 no-spinners'
                  value={type} onChange={handleDropdownChange}
                >
                  <option value="pilih">Pilih opsi</option>
                  <option value="pembeli">Pembeli</option>
                  <option value="penjual">Penjual</option>
                </select>
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: typeAlert }}>Pilih penjual atau pembeli</div>
              <div className='mb-3 flex justify-between'>
                <input type='checkbox' className='w-5 ml-[3px] accent-[#FFB800]' onClick={() => checkBox()} required />
                <p className='text-[10px] text-gray-400 ml-[8px] text-left font-sans'>
                  Dengan mendaftar, saya menyetujui <Link className='text-red-600' href={'s&k'}> Syarat dan Ketentuan</Link> serta <Link href={'kebijakan-privasi'} className='text-red-600'>Kebijakan Privasi</Link>.
                </p>
              </div>
              <button
                className="w-full font-normal px-3 py-2  text-white rounded-full "
                type="submit" disabled={isDisable} style={{ backgroundColor: colorButton }}
              >
                Daftar
              </button>

            </form>
          </div>
        </div>
      </div>

    </section>
  );
};