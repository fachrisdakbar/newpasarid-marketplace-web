"use client"
// import { Link } from 'react-router-dom';
import '../globals.css'
import Image from 'next/image';
import Trolley from "../../public/Group 97.svg"
import Pasarid from "../../public/logoPasarId.svg"
import logo_indonesia from "../../public/logo_indonesia.svg"
import { useEffect, useState } from 'react';
import Modal from "./components/Modal"
import Link from 'next/link';
// import { useRouter } from 'next/navigation';
import { setMyStatePhoneProps,setMyStateKeyProps,setMyStateCaptchaProps } from '../global';


// interface SMSVerificationRequest {
//   phone: string;
//   code: string;
// }

export default function register() {
  // const router = useRouter();

  const [phone, setPhone] = useState('');
  const [imageCaptcha, setImageCaptcha] = useState("");
  const [key, setKey] = useState('');
  const [keyProps, setKeyProps] = useState('')
  const [phoneProps, setphoneProps] = useState('');
  const [captchaProps, setCaptchaProps] = useState('');
  const [alert, setAlert] = useState('none');
  const [isDisable, setIsDisable] = useState(true);
  const [isFilled, setIsFilled] = useState(false);
  const [colorButton, setColorButton] = useState('#D4D4D4');
  const [isChecked, setIsChecked] = useState(false);
  const [captcha, setCaptcha] = useState('');
  const [isFilledCaptcha, setIsFilledCaptcha] = useState(false);


  const handlePhoneChange = (event: { target: { value: any; }; }) => {
    const validPrefixes = [
      '831', '832', '833', '838', '895', '896', '897', '898', '899', '817',
      '818', '819', '859', '878', '877', '814', '815', '816', '855', '856',
      '857', '858', '812', '813', '852', '853', '821', '823', '822', '851',
      '811', '881', '882', '883', '884', '885', '886', '887', '888', '889'
    ];

    const phoneConst = event.target.value;
    console.log("ini angka " + phoneConst);

    // starts angka 8
    const doesNotStartWith8 = !phoneConst.startsWith('8');

    const isValidPrefix = validPrefixes.some((prefix) =>
      phoneConst.startsWith(prefix)
    );

    const isLengthValid = phoneConst.length >= 9 && phoneConst.length <= 14;

    const isValidPhoneNumber =
      !doesNotStartWith8 && isValidPrefix && isLengthValid;
    setPhone(phoneConst)
    setMyStatePhoneProps(phoneConst)
    setIsFilled(isValidPhoneNumber);
    setMyStatePhoneProps(isValidPhoneNumber? phoneConst : '');
    setAlert(isValidPhoneNumber ? 'none' : 'block')

    setIsDisable(!isValidPhoneNumber);
  };


  const captchaCheck = (event: { target: { value: any; }; }) => {
    const captchaConst = event.target.value;
    setCaptcha(captchaConst);
    const isLengthValidCaptcha = captchaConst.length == 6;
    setMyStateCaptchaProps(isLengthValidCaptcha? captchaConst : '');
    setIsFilledCaptcha(isLengthValidCaptcha);
  };

  const checkBox = () => {
    setIsChecked(!isChecked)
    console.log(isChecked);
  }

  const check = () => {
    if (isFilled && isChecked && isFilledCaptcha) {
      
      setColorButton('#EF3F3A')
      setIsDisable(false)
    }
    else {
      setColorButton('#D4D4D4')
      setIsDisable(true)
    }
  }

  useEffect(() => {
    check()
  }, [isChecked, isFilled, isDisable, isFilledCaptcha])

  useEffect(() => {
    fetch('/api/captcha')
      .then((response) => {
        if (!response.ok) {
          throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
        }
        return response.json();
      })
      .then((data) => {
        setKey(data.data['key'])
        setMyStateKeyProps(data.data['key'])
        setImageCaptcha(data.data['img'])
      })
      .catch((error) => {
        console.error('Terjadi kesalahan saat fetching data:', error);
      });
  }, []);


  return (
    <section className="min-h-screen flex items-center inset-0 z-30" >
      <div className="container ">
        <div className='absolute inset-x-0 top-10 flex justify-center'>
          <Image src={Pasarid} width={204} height={109} alt={''} />
        </div>
        <div className="flex justify-evenly items-center">
          <Image src={Trolley} width="620" height="468" alt={''} className='items-center' />
          <div className="px-[24px] py-[50px] w-[376px] h-[430px]  bg-white rounded-lg shadow-xl ">
            <form   >
              <div className="text-left">
                <h2 className="text-[32px] font-bold text-gray-600">Daftar Sekarang</h2>
              </div>
              <div className="mb-1 mt-[10px] flex border rounded-lg">
                {/* logo Indonesia starts */}
                <div className='self-center pl-[11px] py-[14px]'>
                  <Image src={logo_indonesia} alt={''} />
                </div>
                {/* Logo Indonesia ends */}
                <p className='self-center py-[16px] ml-[4px] text-[14px] font-sans text-[#1B1C1E]'>+62</p>
                <p className='self-center px-[8px] mb-1 text-lg font-sans text-[#858585]'>&#9474;</p>
                <input
                  className="py-[16px] text-[14px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners"
                  type="number" value={phone} onChange={handlePhoneChange}
                  placeholder="81234567890" required

                >
                </input>
              </div>
              <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: alert }}>Nomor ponsel belum sesuai</div>
              <div className='mb-2'>
                <p className='text-gray-400 text-[10px] font-sans'>Contoh: 8123456789</p>
              </div>
              {/* <div className='text-[10px] text-warna-text-harga-produk'>Nomor kurang dari 12</div>
              <div className='text-[10px] text-warna-text-harga-produk'>Nomor lebih dari 14</div> */}
              <div className="mb-3">
                {imageCaptcha != "" ? (
                  <div>
                    <img src={imageCaptcha} alt="Captcha" />
                  </div>
                ) : (
                  <p>Mengambil data captcha...</p>
                )}
              </div>
              <div className="mb-3 flex justify-between border rounded-md px-3 py-2">
                <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                  type="text" id="captcha"
                  value={captcha}
                  onChange={captchaCheck}
                  placeholder="Captcha" required />
              </div>
              <div className='mb-3 flex justify-between'>
                <input type='checkbox' className='w-5 ml-[3px] accent-[#FFB800]' onClick={() => checkBox()} required />
                <p className='text-[10px] text-gray-400 ml-[8px] text-left font-sans'>
                  Dengan mendaftar, saya menyetujui <Link className='text-red-600' href={'s&k'}> Syarat dan Ketentuan</Link> serta <Link href={'kebijakan-privasi'} className='text-red-600'>Kebijakan Privasi</Link>.
                </p>
              </div>
              {/* <button className="w-full text-center font-normal px-3 py-2 bg-[#D4D4D4] text-[#FFFFFF] rounded-full hover:bg-gray-400"
                type="submit" onClick={()=>handleRegisterClick}>Daftar </button> */}

            </form>


          </div>
          <div className='absolute z-10 ml-[792px] mt-[320px] w-[328px] '>
            {
              <Modal
                colorButton={colorButton}
                isDisable={isDisable}
                phoneProps={phone}
                keyProps={key}
                captchaProps={captcha}
                
              />
            }

          </div>

        </div>
      </div>

    </section>
  );
};