/** @type {import('next').NextConfig} */
const nextConfig = {}
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  devServer: {
    hot: false,
  },
  reactStrictMode: false,
  images: {
    domains: ["api-newpasarid.bitcorp.id", "api.pasarid-dev.bitcorp.id", "api.pasarid-dev.bitcorp.id/captcha/api/flat", 'assets.pasarid-dev.bitcorp.id']
  },
  source: '/api/elorest/Models/*',
  headers: [
    { key: "Access-Control-Allow-Credentials", value: "true" },
    { key: "Access-Control-Allow-Origin", value: "*" }, // replace this your actual origin
    { key: "Access-Control-Allow-Methods", value: "GET,DELETE,PATCH,POST,PUT" },
    { key: "Access-Control-Allow-Headers", value: "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Tye, pDate, X-Api-Version" },
  ],

}

