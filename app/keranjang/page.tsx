'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '@/app/Accordion/accordion'
import Clipboard from '@/public/Clipboard.svg'
import Image from 'next/image'
import Market from '@/public/toko.svg'
import Location from '@/public/location.svg'
import Delivery from '@/public/delivery.svg'
import Pickup from '@/public/pickup.svg'
import Link from 'next/link'
import Decrement from '@/public/decrement.svg'
import Increment from '@/public/increment.svg'
// import CountdownTimer from '@/CountdownTimer/CountdownTimer'
// import CountdownTimerDelivery from './CountdownTimer/CountdownTimerDelivery'
import { log } from 'console'


interface Cart {
    // penjual_id
    id: number,
    user_id: number,
    nama: string,
    nama_toko: string,
    alamat: string,
    kavling: string,
    pasar: [
        {
            id: number,
            nama: string
        }
    ],
    cart_items: [
        {
            id: number,
            jumlah: number,
            produk: {
                nama: string,
                harga: number,
                harga_setelah_discount: number,
                images: {
                    image: string
                },
                jumlah: number,
            }
        }
    ]
}

const page = () => {

    const [token, setToken] = useState('');
    const [userId, setUserId] = useState('')
    const [idPembeli, setIdPembeli] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [jumlahProduk, setJumlahProduk] = useState(0);
    const [quantity, setQuantity] = useState(0);


    const [loading, setLoading] = useState(true);
    const [items, setItems] = useState<Cart[]>([]);
    const [isCart, setIsCart] = useState(true);
    const [type, setType] = useState('');

    async function fetchData() {
        let url = ''
        if (type === 'pembeli') {
            url = `/api/getUserPembeli`
        } else if (type === 'penjual') {
            url = `/api/getUserPenjual`
        }
        else if (type === 'pemasok') {
            url = `/api/getUserPemasok`
        }
        try {
            const response = await fetch(url, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res.data
            console.log(data);
            setUserId(data.id)
            setIdPembeli(data.pembeli.id)
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        // mengambil data keranjang
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Penjual?with[]=pasar&with[]=cartItems.pasar&with[]=cartItems.produk.images&with[]=cartItems(where=user_id,${userId})&whereHas=cartItems(where=user_id,${userId})&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Cart[] = await response.json();
                setItems(data);
                console.log(data);
                // setJumlahProduk(data.cart_items.produk.jumlah)

                // for (let i = 0; i < data.length; i++) {
                //     for (let j = 0; j < data[i].cart_items.length; j++) {
                //         const jumlahprd = data[i].cart_items[j].produk.jumlah;
                //         setJumlahProduk(jumlahprd);
                //     }
                // }
                if (!data[0]) {
                    setIsCart(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsCart(true)
                    setLoading(false)
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPembeli]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        const typeData = localStorage.getItem('type');
        if (tokenData && typeData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);
            setType(typeData)
        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchCart()
    }, [token, idPembeli, idPenjual])

    // untuk menambahkan produk
    const increment = () => {
        if (jumlahProduk == 0) {
            setQuantity(quantity)
        }
        else if (jumlahProduk! > 0) {
            setQuantity(quantity + 1)
            setJumlahProduk(jumlahProduk - 1)
        }
    }

    // untuk mengurangi produk
    const decrement = () => {
        if (quantity > 0) {
            setQuantity(quantity - 1)
            setJumlahProduk(jumlahProduk + 1)
        }
        else if (quantity == 0) {
            setQuantity(quantity)
        }
    }

    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    <div className='flex flex-col '>
                        {isCart == false &&
                            <div className='flex justify-center items-center flex-col font-roboto pt-[75px] pb-[75px]'>
                                <div className='text-lg font-semibold'>Keranjang</div>
                                <div>Keranjang akan tampil disini</div>
                                <div>
                                    <Image alt='' src={Clipboard} width={100} height={100}
                                        className='w-[300px] h-[300px]'
                                    />
                                </div>
                                <div className='bg-[#788E39] rounded-full text-white font-semibold pt-[11px] pb-[11px] pl-5 pr-5 text-sm cursor-pointer'>
                                    <a href={'/Product'}>Belanja Sekarang</a>
                                </div>
                            </div>
                        }
                    </div>
                    <div className=''>
                        {loading == true ?
                            <div className='flex flex-col justify-center items-center'>
                                <div className='loadingContainer '>
                                    <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                    <div className="loadingAnimation"></div>
                                </div>
                            </div>
                            :
                            <>
                                {isCart == true &&
                                    <div className='w-full grid grid-rows-none gap-3 pl-6 pr-6 pt-6 pb-6 font-roboto'>
                                        <div className='text-lg font-semibold'>Keranjang</div>

                                        {items.map((keranjang) => {
                                            let subtotal = 0
                                            keranjang.cart_items.forEach(item => {
                                                const itemSubtotal = item.produk.harga * item.jumlah;
                                                subtotal += itemSubtotal;
                                            });
                                            // const totals = subtotal + (keranjang.pengiriman?.biaya || 0);
                                            // const total = totals - (keranjang?.pengiriman?.promo_value ? keranjang?.pengiriman?.promo_value : 0) - (keranjang?.promo_value ? keranjang?.promo_value : 0)
                                            return (
                                                // <Link href={`/Pesanan/PesananAktif/[id]`} as={`/Pesanan/PesananAktif/${keranjang.id}`}>
                                                <div key={keranjang.id} className='mb-2 shadow-md p-3'>
                                                    <div className='flex flex-col mt-3'>
                                                        {keranjang.cart_items.map((item: any, index: any) => (
                                                            <div key={index} className='mb-3'>
                                                                <div className='flex flex-row items-center justify-between mb-3'>
                                                                    <div className='flex'>
                                                                        <Image src={Market} alt='' />
                                                                        <div className='text-md font-bold ml-3'>{keranjang.nama_toko}</div>
                                                                    </div>
                                                                    <div className='flex'>
                                                                        <Image src={Location} alt='' />
                                                                        <div className='text-md font-bold ml-3'>{item.pasar.nama}</div>
                                                                    </div>
                                                                </div>
                                                                <div className='flex'>
                                                                    <div>
                                                                        <a href={`/Product/${item.produk_id}`}>
                                                                            <Image src={item.produk.images[0] ? item?.produk?.images[0]?.image : ''} alt='' width={100} height={100}
                                                                                className='w-[150px] h-[150px] rounded-[40px]'
                                                                            />
                                                                        </a>
                                                                    </div>
                                                                    <div className='ml-10'>
                                                                        <div className='mt-3 mb-4'>{item.produk.nama}</div>
                                                                        <div className='mb-4 text-abu-terang text-sm'>{"Rp " + formatNumber(item.produk.harga)}</div>
                                                                        <div className="flex">
                                                                            <div className='flex w-[120px] h-[32px] bg-abu-abu-terang rounded-[16px] justify-between'>
                                                                                <div onClick={decrement} className='ml-2 mt-1 mr-4 mb-1 self-center cursor-pointer w-[22px] h-[22px]'>
                                                                                    <Image width={140} height={140} src={Decrement} alt='' />
                                                                                </div>
                                                                                <div className='self-center text-warna-text-harga-produk text-[12px] font-normal'>{quantity}</div>
                                                                                <div onClick={increment} className='mr-2 mt-1 ml-4 mb-1 self-center cursor-pointer w-[20px] h-[20px]'>
                                                                                    <Image width={140} height={140} src={Increment} alt='' />
                                                                                </div>
                                                                            </div>
                                                                            <div className='font-normal text-[12.25px] ml-2 text-warna-text-sold-produk self-center'>{item.produk.jumlah + " Tersisa"}</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        ))}
                                                    </div>

                                                </div>
                                                // </Link>
                                            )
                                        })}
                                    </div>
                                }
                            </>
                        }
                    </div>
                </div>
            </div >
            <Footer />
        </>
    )
}

export default page