// "use client"
// import React, { useState, useEffect } from 'react';
// import OtpInput from 'react-otp-input';
// import Image from 'next/image';
// import ArrowBack from "../../../public/arrowback_white.svg"
// import { useParams, useSearchParams } from 'next/navigation';
// import { useRouter } from 'next/navigation';
// import Modal from '../components/Modal';
// import { getMyStatePhoneProps, getMyStateKeyProps, getMyStateCaptchaProps,getMyState } from '../../global'

// const Page = () => {
//   const router = useRouter();
//   const [otp, setOtp] = useState<string>('');
//   // const [phone, setPhone] = useState('');
//   const [error, setError] = useState('')
//   // const searchParams = useSearchParams();
//   //   const phone = searchParams.get("phone")
//   // const decryptValue = (encryptedValue: string) => {
//   // Use a proper decryption function here (opposite of btoa)
//   // return atob(encryptedValue);
//   // };

//   // interface SMSVerificationRequest {
//   //   phone: string;
//   //   type: string;
//   //   key: string;
//   //   captcha: string;
//   //   send_type: string;
//   // }

//   // const handleModalYesClick = async (phoneProps: string, keyProps: any, captchaProps: any, router: string[]) => {
//   //   const type = "pembeli";
//   //   const send_type = "sms";
//   //   // const params = new URLSearchParams();
//   //   try {
//   //     const smsVerificationData: SMSVerificationRequest = {
//   //       phone: "62" + phoneProps,
//   //       key: keyProps,
//   //       captcha: captchaProps,
//   //       type,
//   //       send_type,
//   //     };
//   //     const response = await fetch(`/api/sms-verification`, {
//   //       method: 'POST',
//   //       headers: {
//   //         'Content-Type': 'application/json',
//   //       },
//   //       body: JSON.stringify(smsVerificationData),
//   //     });
//   //     console.log(await response.json())
//   //     if (response.ok) {
//   //       const phoneparam = new URLSearchParams({ phone: smsVerificationData.phone }).toString();
//   //       console.log("SMS-Verification passed")
//   //       router.push(`/inputCode?${phoneparam}`);

//   //     } else {
//   //       console.log("SMS Verification failed")
//   //       throw new Error('Failed to verify SMS code');
//   //     }
//   //   } catch (err) {
//   //     setError('Failed to verify SMS code. Please try again.');
//   //   }
//   // };
  
//   const ulangClick = () => {
//     router.push(`/lupaPassword`)
//   }

//   const sendOTP = async () => {
//     // const { phoneProps } = this.props;
//     try {
//       const response = await fetch('/api/password-reset-sms-submition', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//         },  
//         body: JSON.stringify({
//           phone:getMyStatePhoneProps(),
//           key:getMyStateKeyProps(),
//           captcha:getMyStateCaptchaProps(),
//         }),
//       });

//       if (response.status == 200) {
//         console.log("OTP submission successful, redirecting to '/password-baru'");
//         router.push(`/lupaPassword/password-baru`);
//       } else {
//         // Handle error (e.g., display an error message)
//         alert("Failed to submit OTP, WRONG OTP")
//         console.error('Failed to submit OTP');
//       }
//     } catch (error) {
//       console.error('An error occurred during OTP submission:', error);
//     }
//   };

//   useEffect(() => {
//     if (otp.length === 6) {
//       sendOTP();
//     }
//   })

//   return (
//     <div>
//       <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]">
//         <div className='self-center mr-[106px]'>
//           <a href='/lupaPassword'>
//             <Image src={ArrowBack} alt='' />
//           </a>
//         </div>
//         <div>
//           <h3 className='font-medium text-white text-[20px] mr-[100px]'>Verifikasi</h3>
//         </div>
//       </div>

//       <div className='w-screen items-center self-center pt-44'>
//         <div className='m-auto bg-white shadow-2xl rounded-[14px] w-[376px] h-[318px]'>
//           <div className='pt-9 font-medium text-[20px] text-center'>Masukkan Kode Verifikasi</div>
//           <div className='pt-[13px] mb-[52px] font-medium text-[12px] text-center'>Kode verifikasi dikirim melalui {getMyState()} ke
//           {" 0"+getMyStatePhoneProps()} 
//           </div>
//           <div className='font-medium text-[12px] flex justify-center '>
//             <OtpInput
//               value={otp}
//               onChange={setOtp}
//               numInputs={6}
//               renderSeparator={<span> <p className='tracking-[10px] text-transparent'>-</p> </span>}
//               renderInput={(props) => <input {...props}
//                 className='border-b-[1px] border-black focus:ring-0 focus:outline-none'
//               />}
//             />
//           </div>
//           <div className=' font-medium mt-[52px] text-biru-muda text-[12px] text-center cursor-pointer' onClick={ulangClick}>Tidak mendapatkan kode verifikasi? Ulangi Proses</div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Page;