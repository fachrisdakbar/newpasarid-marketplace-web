import * as React from 'react';
import Image from 'next/image'
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Roboto } from 'next/font/google';
import AkunSymbl from "../../public/akun_outlined.svg"
import Wishlist from "../../public/favorite_outlined.svg"
import Pesanan from "../../public/pesanan_outlined.svg"
import './accordion.css'
import Head from 'next/head';
import SignOut from '../SignOut/SignOut';

export default function BasicAccordion({
    isDataDiri = false,
    isChangePass = false,
    isChangePin = false,
    // isPembeli = false,
    isDaftarAlamat = false,
    isPesananAktif = false,
    isPesananSelesai = false,
    isPesananBatal = false,
    isPesananTolak = false,
    isPesananAktifPenjual = false,
    isPesananSelesaiPenjual = false,
    isPesananBatalPenjual = false,
    isPesananTolakPenjual = false,
    isPesananDariPembeliBaru = false,
    isPesananDariPembeliMenyiapkan = false,
    isPesananDariPembeliSedangDikirim = false,
    isPesananDariPembeliSiapDiambil = false,
}) {
    const [isPembeli,setIsPembeli] = React.useState(false);
    const [isPin,setIsPin] = React.useState(false);
    const [token,setToken] = React.useState('');
    React.useEffect(() => {
        const typeUser = localStorage.getItem('type');
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }

        async function fetchDataPembeli() {        
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                console.log(res);
                setIsPin(data.is_credentials_created)
                
            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        async function fetchDataPenjual() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                setIsPin(data.is_credentials_created)
                
            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }

        if(typeUser == 'pembeli'){
            setIsPembeli(true)
            fetchDataPembeli()
        }else{
            setIsPembeli(false)
            fetchDataPenjual()
        }
    }, [token,isPembeli,isPin])

   
    
    return (
        <div>
            <Accordion sx={{ boxShadow: 0 }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >

                    <Image src={Pesanan} alt='' className='mr-2' />
                    <Typography style={{ fontWeight: 'bold', borderBottom: 0 }}>Pesanan</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    {isPembeli &&
                        <Typography sx={{ marginLeft: 4, marginBottom: -5 }}>
                            <div className='mb-[24px]' style={{ color: isPesananAktif ? '#48872A' : 'black' }}><a href='/Pesanan/PesananAktif'>Pesanan Aktif</a></div>
                            <div className='mb-[24px]' style={{ color: isPesananSelesai ? '#48872A' : 'black' }}><a href='/Pesanan/PesananSelesai'>Pesanan Selesai</a></div>
                            <div className='mb-[24px]' style={{ color: isPesananBatal ? '#48872A' : 'black' }}><a href='/Pesanan/PesananBatal'>Pesanan Dibatalkan</a></div>
                            <div className='mb-[24px]' style={{ color: isPesananTolak ? '#48872A' : 'black' }}><a href='/Pesanan/PesananTolak'>Pesanan Ditolak</a></div>
                        </Typography>
                    }
                    {!isPembeli &&
                        <Typography sx={{ marginLeft: 4, marginBottom: -5 }}>
                            <div className='mb-[24px]' style={{ color: isPesananDariPembeliBaru ? '#48872A' : 'black' }}><a href='/PesananDariPembeli/PesananBaru'>Pesanan Baru</a></div>
                            <div className='mb-[24px]' style={{ color: isPesananDariPembeliMenyiapkan ? '#48872A' : 'black' }}><a href='/PesananDariPembeli/PesananMenyiapkan'>Menyiapkan</a></div>
                            <div className='mb-[24px]' style={{ color: isPesananDariPembeliSedangDikirim ? '#48872A' : 'black' }}><a href='/PesananDariPembeli/PesananSedangDikirim'>Sedang Dikirim</a></div>
                            <div className='mb-[24px]' style={{ color: isPesananDariPembeliSiapDiambil ? '#48872A' : 'black' }}><a href='/PesananDariPembeli/PesananSiapDiambil'>Siap Diambil</a></div>
                        </Typography>
                    }
                </AccordionDetails>
            </Accordion>
            {/* <Accordion sx={{ boxShadow: 0, borderBottomColor: 'white' }}> */}
            <AccordionSummary sx={{ borderBottomColor: 'white' }}
            >
                <Image src={Wishlist} alt='' className='mr-2' />
                <Typography style={{ fontWeight: 'bold', }}>Wishlist</Typography>
            </AccordionSummary>
            {/* </Accordion> */}
            <Accordion sx={{ boxShadow: 0 }}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel3a-content"
                    id="panel3a-header"
                >
                    <Image src={AkunSymbl} alt='' className='mr-2' />
                    <Typography sx={{ margin: 0, fontWeight: 'bold' }}>Akun</Typography>
                </AccordionSummary>
                <AccordionDetails >

                    {isPembeli &&
                        <Typography sx={{ marginLeft: 3.3, marginBottom: -5 }}>
                            <div className='mb-[24px]' style={{ color: isDataDiri ? '#48872A' : 'black' }}><a href='/data-diri/'>Data Diri</a></div>
                            <div className='mb-[24px]' style={{ color: isDaftarAlamat ? '#48872A' : 'black' }}><a href='/daftar-alamat'>Daftar Alamat</a></div>
                            <div className='mb-[24px]' style={{ color: isChangePass ? '#48872A' : 'black' }}><a href='/data-diri/changePassword/verifikasi-method'>Ganti Kata Sandi</a></div>
                            {isPin == false &&
                                <div className='mb-[24px]' style={{ color: isChangePin ? '#48872A' : 'black' }}><a href='/data-diri/CreatePIN'>Buat PIN Pembayaran</a></div>
                            }
                            {isPin &&
                                <div className='mb-[24px]' style={{ color: isChangePin ? '#48872A' : 'black' }}><a href='/data-diri/changePin/verifikasi-method'>Ganti Pin</a></div>
                            }
                            <div className='mb-[24px]'><a href='/Bantuan-panduan'>Bantuan</a></div>
                            <div className='mb-[24px]'><a href='/s&k'>Syarat & Ketentuan</a></div>
                            <div className='mb-[24px]'><a href='/kebijakan-privasi'>Kebijakan Privasi</a></div>
                            <div className='mb-[24px]'><a href='/Hubungi-kami'>Hubungi Kami</a></div>
                            <div>
                                <SignOut/>
                            </div>
                        </Typography>}

                    {!isPembeli &&
                        <Typography sx={{ marginLeft: 3.3, marginBottom: -5 }}>
                            <Accordion sx={{ boxShadow:0 }}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel3a-content"
                                id="panel3a-header"
                            >
                                <Typography sx={{ margin: 0, fontWeight: 'semibold' , width:108, marginLeft : -2}}>Pesanan Saya</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography sx={{ marginLeft: 4, marginBottom: -3 }}>
                                    <div className='mb-[24px]' style={{ color: isPesananAktifPenjual ? '#48872A' : 'black' }}><a href='/OrderManagement/PesananAktif'>Aktif</a></div>
                                    <div className='mb-[24px]' style={{ color: isPesananSelesaiPenjual ? '#48872A' : 'black' }}><a href='/OrderManagement/PesananSelesai'>Selesai</a></div>
                                    <div className='mb-[24px]' style={{ color: isPesananTolakPenjual ? '#48872A' : 'black' }}><a href='/OrderManagement/PesananTolak'>Tolak</a></div>
                                    <div className='mb-[24px]' style={{ color: isPesananBatalPenjual ? '#48872A' : 'black' }}><a href='/OrderManagement/PesananBatal'>Batal</a></div>
                                </Typography>
                            </AccordionDetails>
                            </Accordion>
                            <div className='mb-[24px] mt-[18px]' style={{ color: isDataDiri ? '#48872A' : 'black' }}><a href='/data-diri'>Data Diri</a></div>
                            <div className='mb-[24px]'><a href=''>Data Usaha</a></div>
                            <div className='mb-[24px]'><a href=''>Data Rekening</a></div>
                            <div className='mb-[24px]'><a href=''>Daftar Alamat</a></div>
                            <div className='mb-[24px]'><a href=''>Pengaturan Toko</a></div>
                            <div className='mb-[24px]'><a href=''>Ganti PIN Saldo</a></div>
                            <div className='mb-[24px]'><a href=''>Ganti Kata Sandi</a></div>
                            <div className='mb-[24px]'><a href=''>Kode Referal</a></div>
                            <div>
                                <SignOut/>
                            </div>
                        </Typography>}
                </AccordionDetails>
            </Accordion>

        </div>
    )
}
