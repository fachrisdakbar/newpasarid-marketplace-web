'use client'
import React, { useEffect, useState } from 'react'
import Notif from '../../public/notifications.svg'
import Cart from '../../public/cart.svg'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/navigation'

const page = () => {
    const [token, setToken] = useState('');
    const [id, setId] = useState();
    const [idPembeli, setIdPembeli] = useState();
    const [idPenjual, setIdPenjual] = useState();
    const [isPembeli, setIsPembeli] = useState(false);
    const [image, setImage] = useState('');
    const [isMobile, setIsMobile] = useState(false);

    const router = useRouter()
    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        const typeUser = localStorage.getItem('type');

        if (typeUser == 'pembeli') {
            setIsPembeli(true)
        } else {
            setIsPembeli(false)
        }

        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        async function fetchDataPembeli() {
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                console.log(data);
                setIdPembeli(data.pembeli.id)
                setId(data.id)
            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        async function fetchDataPenjual() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res

                setIdPenjual(data.penjual.id)
                setId(data.id)
                // setUsername(data.ukuran)
            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        { isPembeli && fetchDataPembeli() }
        { isPembeli == false && fetchDataPenjual() }

    }, [token, idPembeli, idPenjual]);

    async function getImagePembeli() {
        try {
            console.log("test");

            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Pembeli/${idPembeli}`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            // const data = res.data
            console.log(res);
            setImage(res.image ? res.image : '')
            console.log(res.image);

        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }
    async function getImagePenjual() {
        try {
            console.log("test");

            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Penjual/${idPenjual}`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            // const data = res.data
            console.log(res);
            setImage(res.image ? res.image : '')

        } catch (error) {
            console.error('Error fetching data: ', error);
        }
    }

    useEffect(() => {
        { isPembeli && getImagePembeli() }
        { isPembeli == false && getImagePenjual() }
        console.log(image);
    }, [token, idPembeli, idPenjual, image])

    useEffect(() => {
        console.log(image);

    }, [image])

    const detectUserAgent = () => {
        // detect apakah user menggunakan laptop atau mobile
        const userAgent = window.navigator.userAgent;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) {
            setIsMobile(true)
            router.push(`https://newpasar.id/.well-known/produk.html?key=${id}`)
        } else {
            setIsMobile(false)
            router.push(`https://play.google.com/store/apps/details?id=com.firebase.pasar.dev`)
        }
        console.log("detect");
        
    }

    return (
        // icon notif dan icon profile
        <div className='flex' >
            <div className='h-28 flex items-center justify-center'><Image src={Notif} alt='' width={140} height={140} className='w-[40px] h-[45px] mr-4 cursor-pointer ' /></div>
            <div className='h-28 flex items-center justify-center'onClick={()=>detectUserAgent()}><Image src={Cart} alt='' width={140} height={140} className='w-[38px] h-[45px] mr-4 cursor-pointer rounded-full' /></div>
            <a href={`/data-diri`}>
                <button className='h-28 flex items-center justify-center'><Image src={image!} alt='' width={140} height={140} className='w-[57px] h-[45px] mr-3 cursor-pointer rounded-full'/></button>
            </a>
        </div>
    )
}

export default page