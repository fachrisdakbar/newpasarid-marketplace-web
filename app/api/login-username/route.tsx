import { NextResponse, NextRequest } from "next/server";

export async function POST(req: NextRequest) {
    const json = await req.json();
    console.log({ json });

    try {
        const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/login-username',{
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body :JSON.stringify(json)
        });
        
        if (response.ok) {
          console.log("route response ok")
          const data = await response.text();
          console.log("token: " +data)
          // localStorage.setItem('token', data)
          return NextResponse.json({ token: data });
          
        } else {
          // Handle errors here
          console.log("route response not ok")
          console.error('Error:', response.statusText);
          return NextResponse.json({ error: "Token Not Found"  });
          // return NextResponse.json({ error: "Token Not Found"  });
        }
        
        // if (!response.ok) { 
        //   throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
        // }
        // const data = await response.json();
        // return NextResponse.json({ data });
      } catch (error) {
        console.error('Terjadi kesalahan saat fetching data:', error);
        return NextResponse.json({ message: error });
        
        
    }
  }
