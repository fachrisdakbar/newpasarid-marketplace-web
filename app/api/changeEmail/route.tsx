import { NextResponse, NextRequest } from "next/server";

export async function POST(req: NextRequest) {
    
    const json = await req.json();
    console.log(json.id);
    const {email, id,token,_method} = json
    console.log("json : "+JSON.stringify(json));
    

    try {
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/User/${id}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
            email,_method
            })   
        });
        console.log(response.url);
        const data = await response.json();
        console.log(data);

        if (response.status===200) {
            return NextResponse.json({ data: data });

        } else {
            return NextResponse.json({ error: "Data not found" });
        }
    } catch (error) {
        console.error('Terjadi kesalahan saat fetching data:', error);
        return NextResponse.json({ message: error });
    }
}
