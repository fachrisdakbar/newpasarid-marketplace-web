import { NextResponse, NextRequest } from "next/server";

export async function POST(req: NextRequest) {

    const json = await req.json();
    console.log(json);
    const { old_password, password, confirmation_password, code, token } = json
    console.log("json : " + JSON.stringify(json));


    try {
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/change-password-sms`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
                // 'Accept': 'application/json',
            },
            body: JSON.stringify({
                old_password,
                password,
                confirmation_password,
                code
            })
        });
        console.log(response.url);
        const data = await response.json();
        console.log("data: ", data);
        // knp jd otak atik yg route ini?
        // gasuka ke?
        // yg "data " itu takutnya yg kmrn wfo ituloh kl data doang gamuncul harus dikasi "data", data

        if (response.status === 200) {
            console.log("route response ok")
            const data = await response.json();
            console.log("data: " + data)
            return NextResponse.json({ data: data });

        } else {
            // Handle errors here
            console.log("route response not ok")
            console.error('Error:', response.statusText);
            return NextResponse.json({ error: "Data not found" });

        }
    } catch (error) {
        console.error('Terjadi kesalahan saat fetching data:', error);
        return NextResponse.json({ message: error });
    }
}
