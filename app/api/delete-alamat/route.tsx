// app/api/delete-alamat/route.tsx

import { NextApiRequest, NextApiResponse } from 'next';

export async function deleteAlamat(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'DELETE') {
    try {
      const json = req.body;
      const { id, token } = json;

      const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Alamat/${id}`, {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        const data = await response.json();
        res.status(200).json(data);
      } else {
        console.error('Error:', response.statusText);
        res.status(response.status).json({ error: 'Alamat route error' });
      }
    } catch (error) {
      console.error('Terjadi kesalahan saat fetching data: ', error);
      res.status(500).json({ message: error });
    }
  } else {
    res.status(405).json({ message: 'Method not allowed' });
  }
}
