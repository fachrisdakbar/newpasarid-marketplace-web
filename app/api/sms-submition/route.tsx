import { NextResponse, NextRequest } from "next/server";
// import axios from 'axios';
export async function POST(req: NextRequest) {
  const json = await req.json();
  console.log({ json });
  try {
    const smsSubmAPI = process.env.API_URL_SMS_SUBMITION || "default-api-url";
    const response = await fetch(smsSubmAPI, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(json)
    });
    if (response.status === 200) {
      console.log("route response ok")
      const textResponse = await response.json();
      console.log("message: " + textResponse);
      return NextResponse.json({ message: textResponse });
    } else {
      // Handle errors here
      console.log("route response isn't ok")
      console.error('Error: ', response.statusText);
      return NextResponse.json({ error: "SMS Verification failed" });
    }
  } catch (error) {
    console.error('Terjadi kesalahan saat fetching data:', error);
    return NextResponse.json({ message: error });

  }
}