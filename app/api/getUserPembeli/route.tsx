import { NextResponse, NextRequest } from "next/server";

export async function GET(req: NextRequest) {
    const hoken = req.headers.get('authorization');
    console.log(hoken);
    try {
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
            headers: {
                mode : 'no-cors',
                Authorization: `${hoken}`, 
                'Content-Type': 'application/json',
                // 'Accept': 'application/json',
            },  
            // body: JSON.stringify({}) 
        });
        console.log(response.url);
        const data = await response.json();
        // console.log(data);
        
        console.log(response.status);

        if (response.status===200) {
            
            return NextResponse.json({ data: data });
        } else {
            return NextResponse.json({ error: "Data not found" });

        }
    } catch (error) {
        console.error('Terjadi kesalahan saat fetching data getuser:', error);
        return NextResponse.json({ message: error });
    }
}
