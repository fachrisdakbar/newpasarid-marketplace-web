import { NextResponse } from "next/server";
export async function GET() {
    try {
      const captchaAPI = process.env.API_URL_CAPTCHA || "default-api-url";
        const response = await fetch(captchaAPI, { cache: 'no-store' });
        if (!response.ok) { 
          throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
        }
        const data = await response.json();
        return NextResponse.json({ data });
      } catch (error) {
        console.error('Terjadi kesalahan saat fetching data:', error);
        return NextResponse.json({ message: "Error" }, { status: 500 });
      }
  }