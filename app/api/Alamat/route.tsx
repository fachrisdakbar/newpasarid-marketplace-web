import { NextResponse, NextRequest } from "next/server";

export async function GET(req: NextRequest) {
    const json = await req.json();
    const {id,token} = json
    // console.log({ json });

    try {
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Alamat?where=pembeli_id,${id}&get=*`,{
            headers: {
              Authorization: `Bearer ` + token,
              'Content-Type': 'application/json',
            },
            // body :JSON.stringify(json)
        });
        
        if (response.ok) {
          console.log("route response ok")
          const data = await response.json();
        //   console.log("token: " +data)
          // localStorage.setItem('token', data)
          return NextResponse.json({ data });
          
        } else {
          // Handle errors here
          console.log("route response not ok")
          console.error('Error:', response.statusText);
          return NextResponse.json({ error: "Alamat route error"  });
        }
        
        // if (!response.ok) { 
        //   throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
        // }
        // const data = await response.json();
        // return NextResponse.json({ data });
      } catch (error) {
        console.error('Terjadi kesalahan saat fetching data: ', error);
        return NextResponse.json({ message: error });
        
        
    }
  }
