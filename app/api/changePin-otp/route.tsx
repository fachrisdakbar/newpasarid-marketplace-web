import { NextResponse, NextRequest } from "next/server";

export async function POST(req: NextRequest) {
    
    const json = await req.json();
    console.log(json);
    const {type,phone,key,captcha, token} = json
    console.log("json : "+JSON.stringify(json));
    

    try {
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/payment/send-sms-otp-for-update-pin`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
            type,token,phone,key,captcha
            })   
        });
        console.log(response.url);
        const data = await response.json();
        console.log(data);

        if (response.status===200) {
            console.log("route response ok")
            const data = await response.json();
            console.log("data: " + data)
            return NextResponse.json({ data: data });

        } else {
            // Handle errors here
            console.log("route response not ok")
            console.error('Error:', response.statusText);
            return NextResponse.json({ error: "Data not found" });

        }
    } catch (error) {
        console.error('Terjadi kesalahan saat fetching data:', error);
        return NextResponse.json({ message: error });
    }
}
