import { NextResponse, NextRequest } from "next/server";

export async function POST(req: NextRequest) {

    const json = await req.json();
    const { id, store_order_id, token, } = json
    console.log('=====================================================');
    console.log(id);
    console.log(store_order_id);
    console.log(token);
    console.log('=====================================================');

    try {
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/delivery-booking`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                store_order_id
            })
        });

        console.log(response.url);
        const data = await response.json();
        console.log(data);


        if (response.status === 200) {
            console.log("route response ok")
            const data = await response.json();
            console.log(data)
            return NextResponse.json({ data: data });

        } else {
            // Handle errors here
            console.log("route response not ok")
            console.error('Error:', response.statusText);
            return NextResponse.json({ error: "Data not found" });

        }
    } catch (error) {
        console.error('Terjadi kesalahan saat edit data:', error);
        return NextResponse.json({ message: error });


    }
}
