import { NextResponse, NextRequest } from "next/server";

export async function POST(req: any, res: any) {

    const json = await req.json();
    console.log(json);
    const {order_id,pembeli_id,produk_id,penjual_id,jenis_pengiriman_id,rating_penjual,rating_produk,rating_jenis_pengiriman,comment,token} = json
    console.log("json : " + JSON.stringify(json));
    
    // const data = await req.formData();
    // const image = data.get("image");
    // const id = data.get('id')
    // const pembeli_id = data.get('pembeli_id')
    // const produk_id = data.get('produk_id')
    // const penjual_id = data.get('penjual_id')
    // const jenis_pengiriman_id = data.get('jenis_pengiriman_id')
    // const rating_penjual = data.get('rating_penjual')
    // const rating_produk = data.get('rating_produk')
    // const rating_jenis_pengiriman = data.get('rating_jenis_pengiriman')
    // const comment = data.get('comment')
    // const token = data.get('token')
    console.log("====================");
    console.log("====================");
    
    try {
        // if (!image) {
        //     return NextResponse.json({ error: "No files received." }, { status: 400 });
        // }
        var formdata = new FormData();
        // formdata.append("image", image);
        // formdata.append('id', id!);
        formdata.append("pembeli_id", pembeli_id!);
        formdata.append('produk_id', produk_id!);
        formdata.append('penjual_id', penjual_id!);
        formdata.append('jenis_pengiriman_id', jenis_pengiriman_id);
        formdata.append('rating_penjual', String(rating_penjual ?? ''));
        formdata.append('rating_produk', String(rating_produk ?? ''));
        formdata.append('rating_jenis_pengiriman', String(rating_jenis_pengiriman ?? ''));
        formdata.append('comment', comment);
        formdata.append('token', token);
        // formdata.append('image', image!);

        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Rating`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
                // 'Accept': 'application/json',
            },
            body: 
            // formdata
            JSON.stringify({
                order_id,pembeli_id,produk_id,penjual_id,jenis_pengiriman_id,rating_penjual,rating_produk,rating_jenis_pengiriman,comment
            })
        });
        console.log(response.url);
        const data = await response.json();
        console.log(data);

        if (response.status === 200) {
            console.log("route response ok")
            const data = await response.json();
            console.log (data)
            return NextResponse.json({ data: data });

        } else {
            // Handle errors here
            console.log("route response not ok")
            console.error('Error:', response.statusText);
            return NextResponse.json({ error: "Data not found" });

        }
    } catch (error) {
        console.error('Terjadi kesalahan saat post rating data:', error);
        return NextResponse.json({ message: error });
    }
}
