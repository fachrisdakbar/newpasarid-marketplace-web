import { NextResponse } from "next/server";
import path from "path";

export const POST = async (req: any, res: any) => {
    try {
        const data = await req.formData();

        const image = data.get("image");
        const id = data.get("id");
        const token = data.get("token");
        const _method = data.get("_method");

        console.log("====================");
        console.log(image);
        console.log("====================");
        console.log(_method);
        console.log("====================");
        console.log(token);

        if (!image) {
            return NextResponse.json({ error: "No files received." }, { status: 400 });
        }
        var formdata = new FormData();
        formdata.append("_method", "PUT");
        formdata.append("image", image);

        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Pembeli/${id}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
            },
            body: formdata,
        });

        console.log(response.url);
        console.log(response.body);
        const datanya = await response.json();
        console.log(datanya);
        if(response.ok){
            return NextResponse.json({ message: response.json });
        }
        else {
            // Handle errors here
            console.log("route response not ok")
            console.error('Error:', response.statusText);
            return NextResponse.json({ error: "Data not found" });

        }

    } catch (error) {
        console.error('Terjadi kesalahan saat fetching data pembeli:', error);
        return NextResponse.json({ message: error }, { status: 500 });
    }
};

