'use client'
import React, { useEffect, useState } from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Image from 'next/image';
import Arrow from '../../public/arrow_back.svg'
import Link from 'next/link';

interface Category {
    id: number;
    name: string;
    description: string;
    image: string;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

const page = () => {
    const [categories, setCategories] = useState<Category[]>([]);
    const token = '8970|kyBrnWDwbCUe7CktWwrZUIj5prDZ20ALxNfjerWg';
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 5;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = categories.slice(firstIdx, lastIdx);

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Category?get=*', {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Category[] = await response.json();
                setCategories(data);
                console.log(data);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, []);

    return (
        <>
            <Header />
            <div className='home-content-wrapper shadow-lg pb-12'>
                <div className='flex self-center text-center items-center'>
                    {/* <Link href={'/'}>
                        <Image width={100} height={100} src={Arrow} alt='' className='w-[20px] h-[20px] mr-3' />
                    </Link>
                    <div className='text-[24px] text-abu-abu'>Kategori</div> */}
                     <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                        <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
                        <a href={'/Category'} className='text-[14px] mr-1 no-underline'>Category</a>
                    </div>
                </div>
                <div className='kategori-data mt-6 flex justify-between grid grid-cols-4 gap-4 pl-6 pr-6'>
                    {categories.map((category) => (
                       <a href={`/Category/${category.id}`}>
                        <div key={category.id} className='cursor-pointer hover:bg-slate-200 border-[1.4px] border-abu-abu rounded-[16.51px] w-[225px] h-[100px] flex'>
                            <div className='self-center w-[80px] h-[100px] flex justify-center items-center ml-4 mr-3'>
                                <Image width={100} height={100} src={category.image} alt='' className='w-[60px] h-[60px]' />
                            </div>
                            <div className='font-bold text-base w-[100px] h-[100px] flex items-center mr-3 '>{category.name}</div>
                        </div>
                       </a>
                    ))}
                </div>
                <br />
            </div>
            <Footer />
        </>
    )
}

export default page