'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Image from 'next/image'
import Link from 'next/link'
import { useParams } from 'next/navigation'
import React, { useEffect, useState } from 'react'

interface CategoryProduct {
    id: number;
    nama: string;
    description: string;
    image: string;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
    harga: number;
    sold: number;
    berat: number;
}
interface Category{
    id: number;
    name: string;
    description: string;
    image: string;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

const page = () => {
    const { id } = useParams()
    const [categoriesProducts, setCategoriesProduct] = useState<CategoryProduct[]>([]);
    const [categories, setCategories] = useState();
    const token = '8970|kyBrnWDwbCUe7CktWwrZUIj5prDZ20ALxNfjerWg';
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 5;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = categoriesProducts.slice(firstIdx, lastIdx);

    useEffect(() => {
        async function fetchData() {
            try {
                const res_categoryProduct = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?where=category_id,${id}&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                const res_category = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Category?where=id,${id}&first=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!res_categoryProduct.ok) {
                    throw new Error('Network response was not ok');
                }
                if (!res_category.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: CategoryProduct[] = await res_categoryProduct.json();
                setCategoriesProduct(data);
                console.log(data);

                const datas = await res_category.json();
                setCategories(datas);
                console.log(datas);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, []);

    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }
    

  return (
    <div>
        <Header/>
            <div className='content-detail-wrapper shadow-lg pb-12'>
            <div className='pl-6 pt-12 pb-6 font-roboto font-normal flex text-black'>
                        <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
                        <a href={'/Category'} className='text-[14px] mr-1 no-underline'>Category / </a>
                        <p className='text-[14px]'>{categories?.['name']}</p>
                    </div>
                    <div className='mt-6 flex justify-between grid grid-cols-5 gap-4 pl-6 pr-6'>
                    {datas.map((categoriesProduct) => (
                        <div key={categoriesProduct.id}>
                            <div className='items-center no-underline'>
                                <a className='no-underline' href={`/Product/${categoriesProduct.id}`}>
                                <Image width={140} height={140} src={categoriesProduct.image} alt=''
                                    className='w-[191.08px] h-[191.08px] cursor-pointer' />
                                <div className='font-sans text-warna-text-title-produk font-semibold text-[14.7px] w-[135px] h-[25px] mt-[3.67px] truncate cursor-pointer'>{categoriesProduct.nama + " - " + categoriesProduct.berat + " g"}</div>
                                <div className='flex justify-between items-center'>
                                    <div className='font-sans font-semibold text-warna-text-harga-produk text-[17.5px] flex items-center'>{"Rp " + formatNumber(categoriesProduct.harga)}</div>
                                    <div className='text-[12.25px] font-normal flex items-center text-warna-text-sold-produk h-8'>{categoriesProduct.sold == null ? "0 terjual" : categoriesProduct.sold + " terjual"}</div>
                                </div>
                                </a>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        <Footer/>
    </div>
  )
}

export default page