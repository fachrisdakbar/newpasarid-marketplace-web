'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '../../Accordion/accordion'
import Clipboard from '../../../public/Clipboard.svg'
import Image from 'next/image'
import Market from '../../../public/toko.svg'
import Location from '../../../public/location.svg'
import Delivery from '../../../public/delivery.svg'
import Pickup from '../../../public/pickup.svg'
import Link from 'next/link'
// import CountdownTimer from './CountdownTimer/CountdownTimer'
// import CountdownTimerDelivery from './CountdownTimer/CountdownTimerDelivery'
import { log } from 'console'
import StripesComponent from '../StripComponent/StripesComponent'

interface Order {
    id: number,
    store_order_id: string,
    promo_value: number,
    status: string,
    created_at: string,
    pengiriman_id: number,
    pemasok :{
        nama_toko : string,
    },
    pengiriman: {
        waktu_pengiriman: string,
        promo_value: number
        biaya: number,
        jenis_pengiriman: {
            title: string
        }
    },
    pemasok_order_items: [
        {
            created_at: string,
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            pemasok_produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {

    const [token, setToken] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState<Order[]>([]);
    const [type, setType] = useState('');

    async function fetchData() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/PemasokOrder?with[]=pemasokOrderItems&with[]=pemasok&with[]=pemasokOrderItems.pemasokProduk.pemasokImages&with[]=pembayaran.jenisPembayaran&where[]=status,disiapkan&where[]=penjual_id,${idPenjual}&orderby=updated_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Order[] = await response.json();
                setOrders(data);
                console.log(data);


                // for (let i = 0; i < data.length; i++) {
                //     console.log(data[i].order_items.length);
                // }
                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsOrder(true)
                    setLoading(false)
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPenjual]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPenjual])


    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)


        return formattedDate;
    }

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananAktifPenjual={true}
                    />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    <div className='flex flex-col '>
                        {isOrder == false &&
                            <div className='flex justify-center items-center flex-col font-roboto pt-[75px] pb-[75px]'>
                                <div className='text-lg font-semibold'>Pesanan Aktif</div>
                                <div>Pesanan aktif akan tampil disini</div>
                                <div>
                                    <Image alt='' src={Clipboard} width={100} height={100}
                                        className='w-[300px] h-[300px]'
                                    />
                                </div>
                            </div>
                        }
                    </div>
                    <div className=''>
                        {loading == true ?
                            <div className='flex flex-col justify-center items-center'>
                                <div className='loadingContainer '>
                                    <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                    <div className="loadingAnimation"></div>
                                </div>
                            </div>
                            :
                            <>
                                {isOrder == true &&
                                    <div className='w-full grid grid-rows-none gap-3 pl-6 pr-6 pt-6 pb-6 font-roboto'>
                                        {orders.map((order) => {
                                            let subtotal = 0
                                            order.pemasok_order_items.forEach(item => {
                                                const itemSubtotal = item.pemasok_produk.harga * item.jumlah;
                                                subtotal += itemSubtotal;
                                            });
                                            const total = subtotal
                                            return (
                                                <a href={`/OrderManagement/PesananAktif/${order.id}`}>
                                                    <div key={order.id} className='mb-2 shadow-md p-3'>
                                                        {order.pemasok_order_items.map((item: any, index: any) => (
                                                            <div className='flex justify-between items-center'>
                                                                <div className='text-sm text-abu-abu'>No. Order</div>
                                                                <div className='text-sm text-abu-abu'>{(() => {
                                                                    const formattedDate = formatDate(item.created_at);
                                                                    const justDate = formattedDate.split(' ')
                                                                    return formattedDate.replace('pukul', '').replace(/\./g, ':');
                                                                    
                                                                })()}
                                                                </div>
                                                            </div>
                                                        ))}
                                                        <div className="flex justify-between">
                                                            <div className='font-bold text-sm'>{order.store_order_id}</div>
                                                        </div>
                                                        <div className='flex flex-col mt-3'>
                                                            {order.pemasok_order_items.map((item: any, index: any) => (
                                                                <div key={index} className='mb-3'>
                                                                    <div className='flex flex-row items-center justify-between mb-3'>
                                                                        <div className='flex'>
                                                                            <Image src={Market} alt='' />
                                                                            <div className='text-md font-bold ml-3'>{order.pemasok.nama_toko}</div>
                                                                        </div>
                                                                    </div>
                                                                    <div className='flex'>
                                                                        <div>
                                                                            <Image src={item.pemasok_produk.pemasok_images[0] ? item?.pemasok_produk.pemasok_images[0]?.image : ''} alt='' width={100} height={100}
                                                                                className='w-[150px] h-[150px] rounded-[40px]'
                                                                            />
                                                                        </div>
                                                                        <div className='ml-10'>
                                                                            <div className='mt-3 mb-4 font-semibold'>{item.pemasok_produk.nama}</div>
                                                                            <div className='mb-4 text-abu-terang text-sm'>{"Rp " + formatNumber(item.pemasok_produk.harga)}</div>
                                                                            <div className='flex text-sm font-semibold'>
                                                                                Jumlah
                                                                                <div className='font-semibold ml-1'>{item.jumlah}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className='flex mt-3 mb-3'>
                                                                        <div className='text-sm'>Total Tagihan : </div>
                                                                        <div className='text-sm text-warna-text-harga-produk ml-2 font-semibold'>{"Rp " + formatNumber(total)}</div>
                                                                    </div>
                                                                    <StripesComponent/>
                                                                    <div className='font-semibold bg-yellow-400 p-[10px] flex justify-center items-center mt-4 w-[100px] text-sm'>Disiapkan</div>
                                                                </div>
                                                            ))}
                                                        </div>
                                                    </div>
                                                </a>
                                            )
                                        })}
                                    </div>
                                }
                            </>
                        }
                    </div>
                </div>
            </div >
            <Footer />
        </>
    )
}

export default page