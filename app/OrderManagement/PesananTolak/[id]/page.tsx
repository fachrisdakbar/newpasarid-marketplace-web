'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '../../../Accordion/accordion'
import Clipboard from '../../../public/Clipboard.svg'
import Image from 'next/image'
import Market from '../../../public/toko.svg'
import Location from '../../../public/location.svg'
import Delivery from '../../../public/delivery.svg'
import Pickup from '../../../public/pickup.svg'
import Link from 'next/link'
import LogoUang from '../../../../public/100 uang.svg'
import Saldo from '../../../../public/saldoAyam.svg'
import Briva from '../../../../public/Briva.svg'
import Toko from '../../../../public/toko.svg'
import { useParams } from 'next/navigation'
import StripesComponent from '../../StripComponent/StripesComponent'

interface Order {
    id: number,
    store_order_id: string,
    promo_value: number,
    status: string,
    created_at: string,
    pengiriman_id: number,
    pemasok: {
        nama_toko: string,
    }
    pengiriman: {
        waktu_pengiriman: string,
        promo_value: number
        biaya: number,
        jenis_pengiriman: {
            title: string
        }
    },
    pemasok_order_items: [
        {
            created_at: string,
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            pemasok_produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {

    const [token, setToken] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState();
    const [type, setType] = useState('');
    const [total, setTotal] = useState<number>();
    const { id } = useParams()

    async function fetchData() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/PemasokOrder?with[]=pemasokOrderItems&with[]=pemasokOrderItems.pemasokProduk.pemasokImages&with[]=pembayaran.jenisPembayaran&with[]=pemasok.pasar&where[]=status,ditolak&where[]=penjual_id,${idPenjual}&where[]=id,${id}&orderby=updated_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setOrders(data[0]);
                console.log(data);

                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsOrder(true)
                    setLoading(false)
                }
                let total = 0;
                data[0]?.pemasok_order_items.forEach((item: any) => {
                    const subtotal = item.jumlah * item.pemasok_produk.harga;
                    console.log(subtotal);
                    total += subtotal;
                });
                console.log(total);
                setTotal(total)

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPenjual]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPenjual])


    const formatNumber = (value: any) => {
        if (value === undefined || value === null) {
            // Handle the case when value is undefined or null
            return "N/A";
        }

        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
        });
    };


    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)


        return formattedDate;
    }

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananTolakPenjual={true}
                    />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    <div className='flex flex-col '>
                    </div>
                    <div className=''>
                        {loading == true ?
                            <div className='flex flex-col justify-center items-center'>
                                <div className='loadingContainer '>
                                    <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                    <div className="loadingAnimation"></div>
                                </div>
                            </div>
                            :
                            <>
                        <div className='w-full font-roboto pb-3'>
                            <div className='flex self-center text-center items-center'>
                                <div className='pl-6 pt-6 font-roboto font-normal flex text-black'>
                                    <a href={'/OrderManagement/PesananTolak'} className='text-[14px] mr-1 no-underline'>Pesanan ditolak / </a>
                                    <div className='text-[14px] mr-1 no-underline'>Detail</div>
                                </div>
                            </div>
                            <div className="flex justify-between pt-8 pb-3 pl-5 pr-6">
                                <div className="flex items-center">
                                    <div className='mr-4'>
                                        <Image alt='' src={LogoUang} height={100} width={100} className='w-[50px] h-[50px]' />
                                    </div>
                                    <div>
                                        <div className='text-sm'>Ditolak</div>
                                        {/* <div className='font-semibold text-sm'>{orders?.['status_note']}</div> */}
                                        <div className="flex">
                                            <div className='text-sm mr-4'>Nomor Order :</div>
                                            <div className='text-sm font-semibold'>{orders?.['store_order_id']}</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex items-center">
                                    <div className='mr-3 text-sm'>Tanggal Pesanan : </div>
                                    <div className='text-sm font-semibold'>
                                        <div className='text-sm font-semibold'>{(() => {
                                            const formattedDate = formatDate(orders?.['created_at']);
                                            const justDate = formattedDate.split(' ')
                                            return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                        })()}
                                        </div></div>
                                </div>
                            </div>
                            <div className='flex flex-col pl-6 pr-6 pt-2 pb-4'>
                                <div>
                                    <div className='text-sm'>Metode Pembayaran :</div>
                                    <div className="flex">
                                        {orders?.['pembayaran']['jenis_pembayaran']['title'] == 'Saldo' ?
                                            <div className='mr-8'>
                                                <Image alt='' width={100} height={100} src={Saldo} className='w-[40px] h-[40px]' />
                                            </div>
                                            :
                                            orders?.['pembayaran']['jenis_pembayaran']['title'] == 'Briva' ?
                                                <div className='mr-8'>
                                                    <Image alt='' width={100} height={100} src={Briva} className='w-[40px] h-[40px]' />
                                                </div>
                                                :
                                                ''
                                        }
                                        <div className='text-sm font-semibold flex items-center'>{orders?.['pembayaran']['jenis_pembayaran']['title']}</div>
                                    </div>
                                </div>
                                <div className='flex items-center mt-2'>
                                    <div className='text-sm mr-4'>Kode Pengambilan :</div>
                                    <div className='text-sm font-semibold'>Kode hanya tersedia setelah pesanan di proses</div>
                                </div>
                            </div>
                            <div className='pl-6 pr-6 pb-4'>
                                <div className='font-semibold'>Detail Pesanan ({orders?.['pemasok_order_items']['length']} Barang)</div>
                                <div className="flex mt-3">
                                    <div>
                                        <Image src={Toko} width={100} height={100} alt='' className='w-[15px] h-[15px] mr-2' />
                                    </div>
                                    <div className='text-sm flex items-center'>{orders?.['pemasok']['nama_toko']}</div>
                                </div>
                                <div>
                                    <div className="flex mt-3">
                                        {orders?.['pemasok_order_items']?.map((item: any, index: any) => (
                                            <div className='flex'>
                                                <div className='w-[100px] h-[100px] pr-4'>
                                                    <Image src={item?.['pemasok_produk']['image'] ? item?.['pemasok_produk']['image'] : ''} alt='' width={100} height={100} />
                                                </div>
                                                <div className='flex flex-col'>
                                                    <div>{item?.['pemasok_produk']['nama']}</div>
                                                    <div className='text-abu-terang text-sm mt-1 mb-1'>{"Rp " + formatNumber(item?.['pemasok_produk']['harga'])}</div>
                                                    <div className='font-semibold text-sm'>x{" " + item?.['jumlah']}</div>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                    <div className='text-sm text-abu-terang mt-3'>Catatan : {" " + orders?.['note'] ? orders?.['note'] : ''}</div>
                                </div>
                                <div>
                                    <div className='font-semibold mb-2 mt-3'>Alamat Toko</div>
                                    <div className='text-sm'>{orders?.['pemasok']['pasar']['alamat']}</div>
                                </div>
                                <div>
                                    <div className='text-sm font-semibold mt-3 mb-3'>Rincian tagihan</div>
                                    <div className=' text-sm font-semibold'>Total Tagihan {" Rp " + formatNumber(total)}</div>
                                </div>
                            </div>
                            <div className='pl-6 pr-6'>
                                <StripesComponent />
                            </div>
                        </div>
                        </>
                        }
                    </div>
                </div>
            </div >
            <Footer />
        </>
    )
}

export default page