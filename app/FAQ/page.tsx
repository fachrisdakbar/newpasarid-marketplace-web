"use client"
import React, { useEffect, useState } from 'react'
import cheerio, { AnyNode } from 'cheerio';


interface dataFAQ {
    id: number,
    ask: string,
    question: string
}

export default function FAQ() {
    // const [data, setData] = useState([]);
    const [datas, setDatas] = useState<dataFAQ[]>([]);
    const [ask, setAsk] = useState([]);
    const cheerio = require('cheerio');
    const [question, setQuestion] = useState('');
    const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';

    const extractTextFromHtml = (html: string | AnyNode | AnyNode[] | Buffer) => {
        const $ = cheerio.load(html);
        return $('body').text();
    };

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Faq`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    }
                })
                const value: dataFAQ[] = await response.json();
                setDatas(value)
            } catch (error) {
                console.error('Error fetching key:', error)
            }
        }
        fetchData()
    }, []);


    return (
        <section>
            <div className='content-detail-wrapper'>
            <div className='font-semibold text-[25px] flex justify-center mt-4 mb-4'>FAQ</div>
            {datas.map((data) => (
                <div key={data.id} className=''>
                    <div dangerouslySetInnerHTML={{ __html: data.ask }} className='py-3 px-8' />
                    <div dangerouslySetInnerHTML={{ __html: data.question }} className='py-3 px-8' />
                    <hr className='border-8 border-hijau-logo mb-5 '></hr>
                </div>
            ))}
            </div>

        </section>
    )
}