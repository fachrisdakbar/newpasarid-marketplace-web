'use client'
import Image from 'next/image';
import Link from 'next/link';
import { useSearchParams } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import NotFound from '../../public/Frame 21.svg'

interface Product {
  id: number;
  nama: string;
  harga: number;
  image: string;
  category_id : number;
  deskripsi : string;
  sold: number;
  berat: number;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  deleted_at: null | string;
}

interface Category {
  id: number;
  name: string;
  description: string;
  image: string;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  deleted_at: null | string;
}

const page = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [filteredData, setFilteredData] = useState<Product[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);
  const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';
  const [currPage, setCurrPage] = useState(1);
  const [categoryId, setCategoryId] = useState(0);
  const dataPerPage = 24;
  const lastIdx = dataPerPage * currPage;
  const firstIdx = lastIdx - dataPerPage;
  const datas = products.slice(firstIdx, lastIdx);
  const search = useSearchParams();
  const searchQuery = search ? search.get('q') : null


  useEffect(() => {
    async function fetchData() {
      try {
        // nge get data produk
        const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?get=*', {
          headers: {
            Authorization: `Bearer ${token}`, 
          },
        });
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data: Product[] = await response.json();
        setProducts(data);
        console.log(data);

        // nge get kategori
        const responseCategory = await fetch('https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Category?get=*', {
          headers: {
            Authorization: `Bearer ${token}`, 
          },
        });
        if (!responseCategory.ok) {
          throw new Error('Network response was not ok');
        }
        const categoryName :  Category[] = await responseCategory.json();
        console.log(categoryName);
        console.log(searchQuery);
        
        // untuk mengecek apakah keyword yang diinput user megandung nama yang mirip dengan kategori
        // jika iya, maka di set ke dalam usestate
        for(let i = 0;i<categoryName.length;i++){          
          if(categoryName[i].name.toLowerCase().includes(searchQuery!.toLowerCase())){
            setCategoryId(categoryName[i].id)
            break;
          }
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }
    fetchData();
  }, [searchQuery,categoryId]);


  useEffect(() => {
   filterData(searchQuery,categoryId)
  }, [searchQuery,categoryId,products])

  // untuk mengecek apakah keyword yang diinput sesuai dengan nama produk, atau produk id sama dengan kategori id
  // jika iya, maka akan disimpan ke dalam usestate yang kemudian akan di mapping
  const filterData = (query:any,categoryId:any) => {
    const filtered = products.filter((product) =>
      product.nama?.toLowerCase()?.includes(query.toLowerCase()) || product.category_id === categoryId
    );
    setFilteredData(filtered);
  };

  // untuk format harga
  const formatNumber = (value: any) => {
    return value.toLocaleString('id-ID', {
      useGrouping: true,
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
      minimumIntegerDigits: 1,
      style: 'decimal',
    });
  }
  return (
    <>
      <Header />
      <div className='home-content-wrapper shadow-lg pb-12 pt-6'>
        <div className='mt-6 flex justify-between grid grid-cols-6 gap-4 pl-6 pr-6'>
          {filteredData.length!=0 && filteredData.map((product) => (
            <div key={product.id}>
              <div className='items-center'>
              <a className='no-underline' href={`/Product/${product.id}`}>
                  <Image width={140} height={140} src={product?.image? product.image : ""} alt=''
                    className='w-[191.08px] h-[191.08px] cursor-pointer' />
                  <div className='font-sans text-warna-text-title-produk font-semibold text-[14.7px] w-[135px] h-[25px] mt-[3.67px] truncate cursor-pointer'>{product.nama + " - " + product.berat + " g"}</div>
                  <div className='flex justify-between w-[190px] h-[25px] items-center'>
                    <div className='font-sans font-semibold text-warna-text-harga-produk text-[17.15px]'>{"Rp " + formatNumber(product.harga)}</div>
                    <div className='text-[12.25px] font-normal  text-warna-text-sold-produk mr-4'>{product.sold == null ? "0 terjual" : product.sold + " terjual"}</div>
                  </div>
                </a>
              </div>
            </div>
          ))}
          {filteredData.length == 0 &&
          <div className='w-[75vw] flex flex-col justify-center items-center self-center'>
            <div className='font-semibold text-base mb-3'>Produk Tidak Ditemukan</div>
            <div className='text-sm mb-10'>Gunakan kata kunci lain atau periksa kembali kata kuncinya</div>
          <Image src={NotFound} alt='' height={100} width={100} className='w-[50vw] h-[40vh]'/>
          </div>
          }
        </div>
      </div>
      <Footer />
    </>
  )
}

export default page