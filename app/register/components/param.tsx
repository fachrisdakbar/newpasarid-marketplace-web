import React from 'react'
import { useSearchParams } from 'next/navigation'

export default function Param() {
    const searchParams = useSearchParams();

    const phone = searchParams.get("phone")
  return (
    <div>{phone}</div>
  )
}
