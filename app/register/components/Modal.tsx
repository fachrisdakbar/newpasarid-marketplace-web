"use client"

import Link from 'next/link';
import { useRouter, useSearchParams } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import inputCode from "../inputCode/page"


interface SMSVerificationRequest {
  phone: string;
  type: string;
  key: string;
  captcha: string;
  send_type: string;
}


export const page = ({
  isDisable = true,
  colorButton = '#D4D4D4',
  phoneProps = '',
  captchaProps = '',
  keyProps = ''

}) => {

  const [reload, setReload] = useState("none");
  const [error, setError] = useState<string>('');
  const [key, setKey] = useState('');
  const [captcha, setCaptcha] = useState('');
  const router = useRouter();

  const benarClick = () => {
    router.push('/register/verification-method');
  }

  // const handleModalYesClick = async () => {

  //   const type = "pembeli";
  //   const send_type = "wa";
  //   try {
  //     const smsVerificationData: SMSVerificationRequest = {
  //       phone: "62" + phoneProps,
  //       key: keyProps,
  //       captcha: captchaProps,
  //       type,
  //       send_type,
  //     };
  //     const response = await fetch(`/api/sms-verification`, {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(smsVerificationData),
  //     });
  //     console.log(await response.json())
  //     if (response.ok) {
  //       const phoneparam = new URLSearchParams({ phone: smsVerificationData.phone }).toString();
  //       console.log("phone :" + smsVerificationData.phone);
  //       console.log("captcha :" + captcha);
  //       console.log("SMS-Verification passed")
  //       router.push(`/register/inputCode?${phoneparam}`);

  //     } else {
  //       console.log("SMS Verification failed")
  //       throw new Error('Failed to verify SMS code');
  //     }


  //   } catch (err) {
  //     setError('Failed to verify SMS code. Please try again.');
  //   }
  // };

  
  // useEffect(() => {
  //  console.log(phoneNumber);
   
  // }, [phoneNumber])
  

  return (
    <>

      <button className="w-full text-center font-normal px-3 py-2 bg-[#D4D4D4] text-[#FFFFFF] rounded-full hover:bg-gray-400"
        type="submit" onClick={() => setReload("block")} disabled={isDisable} style={{ backgroundColor: colorButton }}>Daftar</button>
      <section className='w-full h-full bg-opacity-50 bg-black fixed inset-0 ' style={{ display: reload }}>
        <div className="flex justify-center pt-96">
          <div className="w-[280px] bg-white border rounded-xl px-[16px] py-[16px]">
            <p className='text-[16px] flex justify-center text-center font-sans font-semibold'>Verifikasi Nomor HP</p>
            <p className='mt-[18px] align-middle text-[14px] w-[248px] font-normal text-center'>Apakah nomor yang dimasukan sudah benar?</p>
            <div className="mt-[24px] flex justify-between">
              <button
                className="w-[116px] h-[32px] font-normal text-[12px] bg-gray-100 text-red-500 rounded-full hover:bg-gray-200"
                type="submit" onClick={(e) => setReload('none')}
              >
                Ubah
              </button>
              <button
                className="w-[116px] h-[32px] text-[12px] font-normal bg-red-500 text-white rounded-full hover:bg-red-600"
                type="submit" onClick={benarClick}>
                  
                Benar
              </button>
            </div>
          </div>
        </div>
      </section>
      <div>
        {
          
        }
      </div>
    </>
  )
}
export default page