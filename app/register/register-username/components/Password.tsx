import React, { useState } from 'react';

const Password = () => {
    const [password, setPassword] = useState('');
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [isPasswordConfirmVisible, setIsPasswordConfirmVisible] = useState(false);

    const handleTogglePassword = () => {
        setIsPasswordVisible(!isPasswordVisible);
    };
    const handleTogglePasswordConfirm = () => {
        setIsPasswordConfirmVisible(!isPasswordConfirmVisible);
    };

    return (
        <div>
            <div className="mb-3 flex justify-between border rounded-lg px-[6px]">
                <input
                    className="w-full py-2 px-[10px] hover:border-indigo-300 focus:outline-none focus:ring-0 text-[14px] font-sans"
                    type={isPasswordVisible ? 'text' : 'password'}
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Kata Sandi" required
                />
                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePassword}>
                    {isPasswordVisible ? 'visibility' : 'visibility_off'}
                </span>
            </div>
            <div className="mb-3 flex justify-between border rounded-lg px-[6px]">
                <input
                    className="w-full py-2 px-[10px] hover:border-indigo-300 focus:outline-none focus:ring-0 text-[14px] font-sans"
                    type={isPasswordConfirmVisible ? 'text' : 'password'}
                    id="password"
                    // value={password}
                    // onChange={(e) => setPassword(e.target.value)}
                    placeholder="Konfirmasi Kata Sandi" required
                />
                <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePasswordConfirm}>
                    {isPasswordConfirmVisible ? 'visibility' : 'visibility_off'}
                </span>
            </div>
        </div>

    );
};

export default Password;
