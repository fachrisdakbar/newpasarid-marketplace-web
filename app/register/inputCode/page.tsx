"use client"
import React, { useState, useEffect } from 'react';
import OtpInput from 'react-otp-input';
import Image from 'next/image';
import ArrowBack from "../../../public/arrowback_white.svg"
import { useParams, useSearchParams } from 'next/navigation';
import { useRouter } from 'next/navigation';
import Params from "../components/param"
import Modal from '../components/Modal';
import { getMyStatePhoneProps, getMyStateKeyProps, getMyStateCaptchaProps, getMyState } from '../../global';
import Swal from 'sweetalert2';

const Page = ({
  phoneProps = '',
  captchaProps = '',
}) => {
  const router = useRouter();
  const [otp, setOtp] = useState<string>('');
  // const [phone, setPhone] = useState('');
  const [error, setError] = useState('')
  const [type, setType] = useState('')

  // const decryptValue = (encryptedValue: string) => {
  // Use a proper decryption function here (opposite of btoa)
  // return atob(encryptedValue);
  // };

  // interface SMSVerificationRequest {
  //   phone: string;
  //   type: string;
  //   key: string;
  //   captcha: string;
  //   send_type: string;
  // }
  const sendOTP = async () => {
    try {
      const response = await fetch('/api/sms-submition', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          phone: "62" + getMyStatePhoneProps(),
          code: otp,
        }),
      });
      const data = await response.json()
      console.log(data.message.status);
      console.log(data.error);

      if (response.ok && response.status === 200) {
        console.log("OTP submission successful, redirecting to '/register-username'");
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'OTP Berhasil',
          showConfirmButton: false,
          width: 350,
          timer: 2000
        }).then(() => {
          router.push(`/register/register-username`);
        })
      } else {
        Swal.fire({
          position: 'center',
          icon: 'error',
          title: 'Failed to submit OTP, WRONG OTP',
          showConfirmButton: false,
          width: 350,
          timer: 2000
        }).then(() => {
          console.error('Failed to submit OTP');
          router.push(`/register`);
        })
        // alert("Failed to submit OTP, WRONG OTP")
      }
    } catch (error) {
      console.error('An error occurred during OTP submission:', error);
    }
  };

  //   const onResendClick = () => {
  //     if(getMyState()=="sms"){
  //         sendOtpSms()
  //     }
  //     else if(getMyState()=='wa'){
  //         sendOtpWa()
  //     }
  //     setIsSeconds(true);
  // };

  useEffect(() => {
    if (otp.length === 6) {
      sendOTP();
    }
    if (getMyState() == 'wa') {
      setType('whatsapp')
    }
    else if (getMyState() == 'sms') {
      setType('sms')
    }
  })

  return (
    <div>
      <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]">
        <div className='self-center mr-[106px]'>
          <a href='/register'>
            <Image src={ArrowBack} alt='' />
          </a>
        </div>
        <div>
          <h3 className='font-medium text-white text-[20px] mr-[100px]'>Verifikasi</h3>
        </div>
      </div>

      <div className='w-screen items-center self-center pt-44'>
        <div className='m-auto bg-white shadow-2xl rounded-[14px] w-[376px] h-[318px]'>
          <div className='pt-9 font-medium text-[20px] text-center'>Masukkan Kode Verifikasi</div>
          <div className='pt-[13px] mb-[52px] font-medium text-[12px] text-center'>Kode verifikasi dikirim melalui {getMyState()} ke
            {" 0" + getMyStatePhoneProps()}
          </div>
          <div className='font-medium text-[12px] flex justify-center '>
            <OtpInput
              value={otp}
              onChange={setOtp}
              numInputs={6}
              renderSeparator={<span> <p className='tracking-[10px] text-transparent'>-</p> </span>}
              renderInput={(props) => <input {...props}
                className='border-b-[1px] border-black focus:ring-0 focus:outline-none'
              />}
            />
          </div>
          {/* 
            
          */}
          <div className=' font-medium mt-[52px] text-biru-muda text-[12px] text-center cursor-pointer'>Tidak mendapatkan kode verifikasi? Ulangi proses</div>
        </div>
      </div>
    </div>
  );
};

export default Page;