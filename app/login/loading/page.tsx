import React from 'react'

const page = () => {
  return (
    <div className='bg-opacity-50 bg-black fixed inset-0 z-30 flex justify-center items-center'>
      <div className='loadingContainer shadow-md'>
        <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
        <div className="loadingAnimation"></div>
      </div>
    </div>
  )
}

export default page