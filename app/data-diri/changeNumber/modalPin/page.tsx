'use client'
import Footer from '@/app/components/Footer';
import Header from '@/app/components/Header';
import { useEffect, useState } from 'react';
import { getMyState,setMyState } from '@/app/global';

const page = ({
    // isDisplay ='none',
    bg = ''
}) => {
    const [pin, setPin] = useState('');
    const [display, setDisplay] = useState('none');
    const [displayBtn, setDisplayBtn] = useState('flex');

    const handleChange = (e: any) => {
        const value = e.target.value;
        if (/^\d*$/.test(value) && value.length <= 6) {
            setPin(value);
        }
        console.log(value);
    };

    useEffect(() => {
      if(pin.length==6){
        setDisplay('none')
      }
      else{
        console.log("hem");
      }
    }, [pin])
    

    const isClicked =()=>{
        if(bg == '#D4D4D4'){
            console.log('not hit');
            setDisplay('none')
          }
          else if(bg == '#48872A'){
            console.log('hit');
            setDisplay('block')
          }
    }

    return (
        <>

            <button type='submit' onClick={()=>isClicked()}
                className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg cursor-pointer flex justify-center items-center mr-14"
                style={{ backgroundColor: bg}}
            >
                <div>Simpan</div>
            </button>
            <section className='bg-opacity-50 bg-black fixed inset-0 z-30' style={{ display: display}}>
            <div className='pl-[165px] pr-[165px] pb-16 pt-16 flex justify-center items-center h-[100vh]'>
            <div className='font-roboto flex flex-col text-black bg-white w-[500px] h-[400px] rounded-md'>
            <svg onClick={()=>setDisplay('none')} className='mt-2 ml-2 cursor-pointer' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                  </svg>
                <div className='font-semibold text-[25px] mt-16 flex justify-center'>Validasi PIN</div>
                <div className='text-[16px] mb-4 mt-[70px] flex justify-center'>Masukkan PIN anda</div>
                <div className="pin-input">
                    {[1, 2, 3, 4, 5, 6].map((index) => (
                        <div key={index} className={`pin-circle ${index <= pin.length ? 'filled' : ''}`}></div>
                    ))}
                    <input
                        type="password"
                        value={pin}
                        onChange={handleChange}
                        maxLength={6}
                        className="pin-field"
                    />
                </div>
            </div>
            </div>
                </section>
        </>
    );
};

export default page;
