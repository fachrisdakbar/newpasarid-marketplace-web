'use client'
import Footer from '@/app/components/Footer';
import Header from '@/app/components/Header';
import { useEffect, useState } from 'react';
import ModalPin from './modalPin/page'
import { getMyState, setMyState } from '@/app/global';
import Captcha from './captcha/page'
import Accordion from "../../Accordion/accordion"

const PinInput = () => {

    const [isNumber, setIsNumber] = useState();
    const [alert, setAlert] = useState('none');
    const [bg, setBg] = useState('#D4D4D4');


    const handlePhoneChange = (event: { target: { value: any; }; }) => {
        const validPrefixes = [
            '831', '832', '833', '838', '895', '896', '897', '898', '899', '817',
            '818', '819', '859', '878', '877', '814', '815', '816', '855', '856',
            '857', '858', '812', '813', '852', '853', '821', '823', '822', '851',
            '811', '881', '882', '883', '884', '885', '886', '887', '888', '889'
        ];

        const phoneConst = event.target.value;
        setIsNumber(phoneConst);
        const doesNotStartWith8 = !phoneConst.startsWith('8');
        console.log(doesNotStartWith8);

        const isValidPrefix = validPrefixes.some((prefix) =>
            phoneConst.startsWith(prefix)
        );
        console.log(isValidPrefix);
        // doesnotstart == true isvalidprefix == false 

        const isLengthValid = phoneConst.length >= 9 && phoneConst.length <= 14;
        console.log(isLengthValid); // false

        const isValidPhoneNumber = doesNotStartWith8 == false && isValidPrefix && isLengthValid;
        console.log(isValidPhoneNumber); // false

        setAlert(isValidPhoneNumber ? 'none' : 'block')
        setBg(isValidPhoneNumber ? '#48872A' : '#D4D4D4')
        // setIsNumChecked(isValidPhoneNumber)
        // setNumAlert(isValidPhoneNumber ? 'none' : 'block')
        // setIsDisable(!isValidPhoneNumber);
    };




    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        isDataDiri={true}
                    />
                </div>
                <div className="w-[684px] font-roboto flex flex-col ml-[100px]">
                    <div className='font-semibold text-[20px] mb-3'>Masukkan nomor handphone yang baru</div>
                    <div className='flex justify-start items-center mt-2'>
                        {/* <h3 className='text-[16px] h-11 font-normal flex justify-end items-center mr-10'>Nomor Handphone</h3> */}
                        <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                            <p className='self-center py-[16px] ml-[4px] text-[14px] font-sans text-[#1B1C1E]'>62</p>
                            <p className='self-center px-[8px] mb-1 text-lg font-sans text-[#858585]'>&#9474;</p>
                            <input
                                className="py-[16px] text-[14px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners"
                                type="number" value={isNumber} onChange={handlePhoneChange}
                                placeholder="81234567890" required

                            />
                        </div>
                    </div>
                    <div className='text-gray-400 text-[10px] font-sans mt-1'>Contoh: 8123456789</div>
                    <div className='text-[10px] text-warna-text-harga-produk mt-2 mb-1' style={{ display: alert }}>Nomor ponsel belum sesuai</div>
                    <div className='mt-3'>
                        <ModalPin
                            bg={bg}
                        />
                    </div>
                </div>
                <div className='flex justify-center mb-16 mr-[150px]'>
                    <Captcha />
                </div>
            </div>
            <Footer />
        </>
    );
};

export default PinInput;
