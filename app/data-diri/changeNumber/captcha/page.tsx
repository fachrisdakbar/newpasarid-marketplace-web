'use client'
import React, { useEffect, useState } from 'react'
import Refresh from '../../../../public/refresh.svg'
import Image from 'next/image';

const page = () => {
    const [imageCaptcha, setImageCaptcha] = useState("");
    const [key, setKey] = useState('');
    const [refreshCaptcha, setRefrechCaptcha] = useState(0);
    const [captcha, setCaptcha] = useState('');
    const [isFilledCaptcha, setIsFilledCaptcha] = useState(false);

    useEffect(() => {
        fetch('/api/captcha')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
                }
                return response.json();
            })
            .then((data) => {
                setKey(data.data['key']);
                setImageCaptcha(data.data['img'])
            })
            .catch((error) => {
                console.error('Terjadi kesalahan saat fetching data:', error);
            });
    }, [refreshCaptcha])

    const captchaCheck = (event: { target: { value: any; }; }) => {
        const captchaConst = event.target.value;
        setCaptcha(captchaConst);
        const isLengthValidCaptcha = captchaConst.length == 6;
        setIsFilledCaptcha(isLengthValidCaptcha);
    };

    const refresh=()=>{
        console.log("klik");
        setRefrechCaptcha(refreshCaptcha+1)
    }

    return (
        <>
        <section className='bg-opacity-50 bg-black fixed inset-0 z-30' style={{ display: 'none'}}>
           <div className='w-[100vw] flex justify-center items-center'>
          <div className='bg-white flex flex-col justify-center items-center w-[400px] h-[300px] rounded-md'>
          <div className="mb-4  ">
                {imageCaptcha != "" ? (
                    <div className='flex justify-center items-center'>
                        <div>
                        <img src={imageCaptcha} alt="Captcha" />
                        </div>
                        <div onClick={()=>refresh()}>
                            <Image src={Refresh} width={100} height={100} alt=''
                            className='w-[28px] h-[28px] pl-1 cursor-pointer'
                            />
                        </div>
                    </div>
                ) : (
                    <p>Mengambil data captcha...</p>
                )}
            </div>
            <div className=" flex justify-between border rounded-md px-3 py-2">
                <input className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans"
                    type="text" id="captcha"
                    value={captcha}
                    onChange={captchaCheck}
                    placeholder="Captcha" required />
            </div>
          </div>
           </div>
            </section>
        </>
    )
}

export default page