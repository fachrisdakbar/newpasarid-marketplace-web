"use client"
import React from 'react'
import Header from "../components/Header"
import Footer from "../components/Footer"
import { useEffect, useState } from 'react';
import Link from 'next/link';
import Accordion from "../Accordion/accordion"
import Swal from 'sweetalert2'
import { setMyStatePhone } from '../global';
import Image from 'next/image';
import ModalImage from './modalImage/page'
import Loading from '../login/loading/page'
import LoginPage from '@/app/login/page'

export const page = () => {

    const [phone, setPhone] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [nik, setNik] = useState('');
    const [alamat, setAlamat] = useState('');
    const [nameAlert, setNameAlert] = useState('none');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [passAlert, setPassAlert] = useState('none');
    const [numAlert, setNumAlert] = useState('none');
    const [token, setToken] = useState('');
    const [isDisable, setIsDisable] = useState(true);
    const [isEdit, setIsEdit] = useState(false);
    const [isNumChecked, setIsNumChecked] = useState(false);
    const [colorButton, setColorButton] = useState('#D4D4D4');
    const [isChecked, setIsChecked] = useState(false);
    const [isCheckedName, setIsCheckedName] = useState(false);
    const [border, setBorder] = useState('#F4F4F4');
    const [background, setBackground] = useState('#F4F4F4');
    const [textColor, setTextColor] = useState('gray');
    const [cursor, setCursor] = useState('auto');
    const [selectedImage, setSelectedImage] = useState(null);
    const [id, setId] = useState();
    const [idPembeli, setIdPembeli] = useState();
    const [idPenjual, setIdPenjual] = useState();
    const [accordionActive, setAccordionActive] = useState(false);
    const [isPembeli, setIsPembeli] = useState(false); // atur pembeli penjual
    const [visible, setVisible] = useState('none');
    const [imageVisible, setImageVisible] = useState(false);
    const [image, setImage] = useState('');
    const [loading, setLoading] = useState(false);
    const [refresh, setRefresh] = useState(0);

    const handleImageUpload = (e: any) => {
        const image = e.target.files[0];
        const validExtensions = ['jpg', 'jpeg', 'png'];
        const extension = image.name.split('.').pop().toLowerCase();
        console.log(image);
        if (image.size > 10 * 1024 * 1024) {
            Swal.fire({
                icon: 'error',
                title: 'Ukuran file terlalu besar!',
                text: 'Mohon untuk mengunggah file lebih kecil dari 10 MB.',
            });;
            return;
        }
        if (validExtensions.indexOf(extension) === -1) {
            Swal.fire({
                icon: 'error',
                title: 'Tipe jenis file tidak valid!',
                text: 'Mohon unggah file jpg, jpeg, atau png file.',
            });
            return;
        }
        if (image) {
            setSelectedImage(image);
            console.log(image);
        }
    }

    const handlePhoneChange = (event: { target: { value: any; }; }) => {
        const validPrefixes = [
            '831', '832', '833', '838', '895', '896', '897', '898', '899', '817',
            '818', '819', '859', '878', '877', '814', '815', '816', '855', '856',
            '857', '858', '812', '813', '852', '853', '821', '823', '822', '851',
            '811', '881', '882', '883', '884', '885', '886', '887', '888', '889'
        ];

        const phoneConst = event.target.value;
        setPhone(phoneConst);
        const doesNotStartWith8 = !phoneConst.startsWith('8');
        const isValidPrefix = validPrefixes.some((prefix) =>
            phoneConst.startsWith(prefix)
        );
        const isLengthValid = phoneConst.length >= 9 && phoneConst.length <= 14;
        const isValidPhoneNumber =
            !doesNotStartWith8 && isValidPrefix && isLengthValid;
        setIsNumChecked(isValidPhoneNumber)
        setNumAlert(isValidPhoneNumber ? 'none' : 'block')
        setIsDisable(!isValidPhoneNumber);
    };

    const nameCheck = (event: { target: { value: any; }; }) => {

        const nameConst = event.target.value;
        const check = /^[A-Za-z]+( [A-Za-z]+)*$/;
        const isNameString = check.test(nameConst);

        setName(nameConst)
        // console.log(nameConst);

        setNameAlert(isNameString ? 'none' : 'block')
        setIsCheckedName(isNameString)
        setIsDisable(!isNameString)
    }
    const emailHandling = (event: { target: { value: any; }; }) => {
        const emailConst = event.target.value;
        setEmail(emailConst)
    }

    const checkIsEdit = () => {
        setIsEdit(!isEdit)
        if (!isEdit) {
            setVisible('flex')
            setBorder('#48872A')
            setBackground('white')
            setTextColor('#48872A')
            setCursor('pointer')
        }
        else if (isEdit) {
            setVisible('none')
            setBorder('#F4F4F4')
            setBackground('#F4F4F4')
            setTextColor('gray')
            setCursor('auto')
        }
    }

    const editData = (e: { preventDefault: () => void; }) => {
        setLoading(true)
        e.preventDefault()
        async function fetchData() {
            try {
                const response = await fetch(`/api/edit-user/${id}`, {
                    method: 'POST',
                    body: JSON.stringify({
                        id: id,
                        name: name,
                        email: email,
                        phone: phone,
                        token: token,
                        _method: 'PUT',
                    }),
                });
                const responseData = await response.json();
                if (response.ok && responseData) {
                    const data = responseData;
                    console.log('Data:', data);
                    setLoading(false)
                    if (loading == false) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Berhasil Edit Data',
                            showConfirmButton: false,
                            width: 350,
                            timer: 2000
                        }).then(() => {
                            setIsEdit(!isEdit)
                            checkIsEdit()
                            setRefresh(refresh+1)
                        })
                    }
                } else {
                    console.error('Error:', responseData.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()

        async function inputImage() {
            try {
                console.log(selectedImage);

                const formData = new FormData();
                formData.append('id', idPembeli!);
                formData.append('token', token);
                formData.append('_method', 'PUT');
                formData.append('image', selectedImage!);

                console.log(formData);

                const response = await fetch('/api/change-profile', {
                    method: 'POST',
                    body: formData,
                });

                const responseData = await response.json();
                console.log(responseData);

                if (response.ok && responseData) {
                    const data = responseData;
                    console.log(data);
                    setImageVisible(true)
                    setIsEdit(!isEdit)
                } else {
                    console.error('Error:', responseData.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        inputImage()
        getImagePembeli()
        console.log(selectedImage)
    }

    useEffect(() => {
        const path = window.location.pathname;
        const lastPart = path.split('/').filter(Boolean).pop();
        setAccordionActive(true)

        const tokenData = localStorage.getItem('token');
        const typeUser = localStorage.getItem('type');

        if(typeUser == 'pembeli'){
            setIsPembeli(true)
        }else{
            setIsPembeli(false)
        }
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        async function fetchDataPembeli() {
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                console.log(data);
                setIdPembeli(data.pembeli.id)
                setId(data.id)
                setName(data.name)
                // setEmail(data.email)
                setPhone(data.phone)
                setMyStatePhone(data.phone)
                if(data.email == 'support@newpasar.id'){
                    setEmail('')
                }
                else{
                    setEmail(data.email)
                }
            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        async function fetchDataPenjual() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res
                console.log(data);
                console.log(idPenjual);
                
                setIdPenjual(data.penjual.id)
                setId(data.id)
                setName(data.penjual.nama)
                setPhone(data.penjual.phone)
                setMyStatePhone(data.penjual.phone)
                setNik(data.penjual.nik)
                setAlamat(data.penjual.alamat)
                // setUsername(data.ukuran)
            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        {isPembeli && fetchDataPembeli()}
        {isPembeli == false && fetchDataPenjual()}

    }, [token,idPembeli,idPenjual,refresh]);


    async function getImagePembeli() {
        try {
            console.log("test");

            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Pembeli/${idPembeli}`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            // const data = res.data
            console.log(res);
            setImage(res.image ? res.image : '')
            console.log(res.image);

            console.log("ini");

        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }
    async function getImagePenjual() {
        try {
            console.log("test");

            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Penjual/${idPenjual}`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            // const data = res.data
            console.log(res);
            setImage(res.image ? res.image : '')
            console.log(res.image);

            console.log("ini");

        } catch (error) {
            console.error('Error fetching data: ', error);
        }
    }

    useEffect(() => {
        {isPembeli && getImagePembeli()}
        {isPembeli == false && getImagePenjual()}
        console.log(image);
    }, [token,idPembeli,idPenjual,image,selectedImage,refresh])

        useEffect(() => {
         console.log(image);
         
        }, [image])
        

    return (
        <div>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        isDataDiri={accordionActive}
                    />
                </div>
                <div className="w-[684px] font-roboto mr-32">
                    <div className=" ">
                        {isPembeli && 
                        <form onSubmit={editData}>
                            <div className="text-left mb-6">
                                <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Data Diri</h2>
                            </div>
                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Nama Lengkap</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input
                                        className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text"
                                        value={name}
                                        onChange={nameCheck}
                                        disabled={!isEdit}
                                    />
                                </div>
                            </div>

                            <div className='absolute flex justify-center items-center ml-[300px]'>
                                {loading &&
                                    <Loading />}
                            </div>
                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Email</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text" id="name"
                                        value={email}
                                        disabled={true}
                                        onChange={emailHandling}
                                    />
                                    <a href={'/data-diri/changeEmail'} className='font-medium text-border-hijau text-sm flex justify-center items-center'
                                        style={{ display: visible }}>Ubah</a>
                                </div>
                            </div>

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Nomor Handphone</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text" id="name"
                                        value={phone}
                                        disabled={true}
                                        onChange={handlePhoneChange}
                                    />
                                    <a href={'data-diri/changeNumber'} className='font-medium text-border-hijau text-sm flex justify-center items-center'
                                        style={{ display: visible }}>Ubah</a>
                                </div>
                            </div>

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Password</h3>
                                <a href={'../data-diri/changePassword/verifikasi-method'} className='font-medium text-border-hijau text-sm'>Ubah</a>
                            </div>

                            <div className='text-[10px] text-warna-text-harga-produk mt-[-15px] w-[500px] h-[30px] ml-[186px]' style={{ display: passAlert }}>Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol</div>

                            {isEdit &&
                                <div className='ml-[184px] flex'>
                                    <button type='submit' className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-border-hijau cursor-pointer flex justify-center items-center mr-14">
                                        <div>Simpan</div>
                                    </button>
                                    <div onClick={() => checkIsEdit()} className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-abu-terang cursor-pointer flex justify-center items-center">
                                        Batal
                                    </div>
                                </div>
                            }

                            {!isEdit &&
                                <div className='ml-[184px]'>
                                    <div onClick={() => checkIsEdit()}
                                        className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-[#788E39] flex justify-center items-center cursor-pointer">
                                        Edit
                                    </div>
                                </div>}

                        </form>}
                        {isPembeli == false && 
                        <>
                        {/* <form onSubmit={editData}> */}
                            <div className="text-left mb-6">
                                <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Data Diri</h2>
                            </div>
                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>NIK</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text" id="name"
                                        value={nik}
                                        // disabled={true}
                                        readOnly={true}
                                        // onChange={emailHandling}
                                    />
                                    {/* <Link href={'/data-diri/changeEmail'} className='font-medium text-border-hijau text-sm flex justify-center items-center'
                                        style={{ display: visible }}>Ubah</Link> */}
                                </div>
                            </div>
                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Nama Lengkap</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input
                                        className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text"
                                        value={name}
                                        onChange={nameCheck}
                                        disabled={!isEdit}
                                    />
                                </div>
                            </div>

                            <div className='absolute flex justify-center items-center ml-[300px]'>
                                {loading &&
                                    <Loading />}
                            </div>

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Detail Alamat</h3>
                                <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                                    <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                        type="text" id="name"
                                        value={alamat}
                                        disabled={!isEdit}
                                        // onChange={handlePhoneChange}
                                    />
                                    {/* <Link href={'data-diri/changeNumber'} className='font-medium text-border-hijau text-sm flex justify-center items-center'
                                        style={{ display: visible }}>Ubah</Link> */}
                                </div>
                            </div>

                            <div className='flex justify-start items-center mb-6'>
                                <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Password</h3>
                                <a href={'../data-diri/changePassword/verifikasi-method'} className='font-medium text-border-hijau text-sm'>Ubah</a>
                            </div>

                            <div className='text-[10px] text-warna-text-harga-produk mt-[-15px] w-[500px] h-[30px] ml-[186px]' style={{ display: passAlert }}>Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol</div>

                            {isEdit &&
                                <div className='ml-[184px] flex'>
                                    <button type='submit' className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-border-hijau cursor-pointer flex justify-center items-center mr-14">
                                        <div>Simpan</div>
                                    </button>
                                    <div onClick={() => checkIsEdit()} className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-abu-terang cursor-pointer flex justify-center items-center">
                                        Batal
                                    </div>
                                </div>
                            }

                            {!isEdit &&
                                <div className='ml-[184px]'>
                                    <div onClick={() => checkIsEdit()}
                                        className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg bg-[#788E39] flex justify-center items-center cursor-pointer">
                                        Edit
                                    </div>
                                </div>}

                        {/* </form> */}
                        </>
                        }

                    </div>
                </div>
                <div className='flex flex-col'>
                    {isPembeli==false &&
                        <div className="flex justify-end">
                        <LoginPage isSwitchAcc={true} thisLogin={false} phone={phone}/>
                    </div>}

                <div className='bg-white border-[1px] border-neutral-border rounded-lg w-[260px] h-[416px] mt-[56px] flex flex-col'>
                    <ModalImage
                        selectedImage={selectedImage!}
                        image={image!}
                        token = {token}
                        idPembeli = {idPembeli}
                    />
                    <div className='w-[228px] h-[44px] border-[1px] rounded-lg font-medium text-[14px] flex justify-center items-center mb-4 ml-4 mr-4 cursor-pointer'
                        style={{ borderColor: border, backgroundColor: background, color: textColor, cursor: 'pointer' }}
                    >
                        <input type='file'
                            id="imageInput"
                            onChange={handleImageUpload}
                            style={{ display: 'none' }}
                            disabled={!isEdit}
                        />

                        <label htmlFor="imageInput" className='w-[220px] flex justify-center' style={{ cursor: 'pointer' }}>Pilih Foto</label>
                    </div>
                    <div className='font-normal text-[14px] w-[228px] h-[80px] flex justify-center items-center text-abu-terang mb-4 ml-4 mr-4'>Besar file: maksimum 10 MB. Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</div>
                </div>
            </div>
            </div>
            <Footer />
        </div>
    )
}
export default page
