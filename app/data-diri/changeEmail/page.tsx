'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2'
import Loading from '../../login/loading/page'
import { setMyStateEmail,setMyStateToken,setMyStateId } from '@/app/global'
import { useRouter } from 'next/navigation'
import Accordion from "../../Accordion/accordion"

const page = () => {
    const [email, setEmail] = useState('')
    const [background, setBackground] = useState('#D4D4D4');
    const [cursor, setCursor] = useState('auto');
    const [token, setToken] = useState('');
    const [loading, setLoading] = useState(false);
    const [setIsDisplay, isDisplay] = useState('none');
    const [id, setId] = useState();
    const router = useRouter()

    const emailHandling = (e: { target: { value: any; }; }) => {
        console.log(e.target.value);
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        const isValid = regex.test(e.target.value)
        if (!isValid) {
            setBackground('#D4D4D4')
            setCursor('auto')
        }
        else {
            setBackground('#48872A')
            setCursor('pointer')
        }
        console.log(isValid);
        setEmail(e.target.value)
        setMyStateEmail(e.target.value)
    }

    const isClicked = () => {
        if (background == '#D4D4D4') {
            console.log('not hit API');

        }
        else if (background == '#48872A') {
            fetchData()
        }
    }

    async function fetchData() {
        setLoading(true)
        try {
            const response = await fetch(`/api/changeEmail-sendVerification`, {
                method: 'POST',
                body: JSON.stringify({
                    email: email,
                    token: token,
                }),
            })
            const data = await response.json();
            console.log(data.data);
            if (data.data != undefined && data.data.code == 200) {
                console.log("ke send");
                setLoading(false)
                if (loading == false) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Berhasil mengirim kode verifikasi!',
                        showConfirmButton: true,
                        width: 350,
                    }).then(()=>{
                        router.push('/data-diri/changeEmail/kodeVerifikasi')
                    })
                }
            }
            else {
                setLoading(false)
                if(loading==false){
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Gagal mengirim kode verifikasi!',
                        text: 'Periksa kembali email anda',
                        showConfirmButton: true,
                        width: 450,
                    })
                }
                console.log("tidak ke send");
                console.error('Error:', data.message);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            setMyStateToken(extractedValue)
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
    }, [token])

    
    useEffect(() => {
        async function fetchData() {
            console.log("fetchData");
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                const res = await response.json()
                const data = res.data
                console.log(res.data.id);
                setMyStateId(res.data.id)
                setId(res.data.id)
                // setId(res.data.id)
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        console.log(token);
        fetchData()
    }, [token])

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        // isPembeli={isPembeli}
                        isDataDiri={true}
                    />
                </div>
                <div className="w-[684px] font-roboto flex flex-col items-center">
                        <div className='font-semibold text-base'>Masukkan Email baru yang akan dikirimkan kode verifikasi</div>
                        <div className='flex justify-start items-center mt-10 mb-10'>
                            <h3 className='text-[16px] h-11 font-normal flex justify-end items-center mr-10'>Email</h3>
                            <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                            <div className="absolute">
                            {loading &&
                                <Loading />
                            }
                            </div>
                                <input className="hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans w-[500px] h-[30px]"
                                    type="text" id="name"
                                    value={email}
                                    onChange={(e) => emailHandling(e)}
                                    placeholder='example@email.com'
                                    />
                            </div>
                        </div>
                        <div className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg flex justify-center items-center ml-[80px]" onClick={() => isClicked()}
                            style={{ backgroundColor: background, cursor: cursor }}>
                            Simpan
                        </div>
                    </div>
                </div>
            <Footer />
        </>
    )
}

export default page