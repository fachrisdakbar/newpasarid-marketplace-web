'use client'
import React, { useEffect, useState } from 'react'
import ArrowBack from '../../../../public/arrowback_white.svg'
import Image from 'next/image'
import OTPInput from 'react-otp-input'
import { getMyStateEmail,getMyStateToken,getMyStateId } from '@/app/global'
import Swal from 'sweetalert2'
import Loading from '../../../login/loading/page'
import { useRouter } from 'next/navigation'

const page = () => {
    const [otp, setOtp] = useState<string>('');
    const [email, setEmail] = useState('')
    const [token, setToken] = useState('');
    const [loading, setLoading] = useState(false);
    const [id, setId] = useState();
    const router = useRouter()

    async function fetchData() {
        setLoading(true)
        try {
            const response = await fetch(`/api/changeEmail-inputCode`, {
                method: 'POST',
                body: JSON.stringify({
                    email: getMyStateEmail(),
                    token: getMyStateToken(),
                    code: otp
                }),
            })
            const data = await response.json();
            console.log(data.data);
            if (data.data != undefined && data.data.code == 200) {
                console.log("ke send");
                setLoading(false)
                if (loading == false) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Berhasil mengirim kode verifikasi!',
                        showConfirmButton: true,
                        width: 350,
                    }).then(() => {
                        changeEmail()
                    })
                }
            }
            else {
                setLoading(false)
                // changeEmail()
                if (loading == false) {
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'Gagal mengirim kode verifikasi!',
                        text : 'Klik tombol di bawah untuk mengirim ulang kode verifikasi',
                        showConfirmButton: true,
                        width: 450,
                    })
                }
                console.log("tidak ke send");
                console.error('Error:', data.message);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const changeEmail = async () => {
        console.log(id);
        console.log("function changeEmail");
        try {
            const response = await fetch(`/api/changeEmail`, {
                method: 'POST',
                body: JSON.stringify({
                    email: getMyStateEmail(),
                    token: getMyStateToken(),
                    id: getMyStateId(),
                    _method : 'PUT'
                }),
            })
            const res = await response.json()
            console.log(res.error);
            if(res.error == 'Data not found'){
                console.log('error input code');
            }
            else{
                console.log("success input code");
                router.push('/data-diri')
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const resendCode = async() => {
        const response = await fetch(`/api/changeEmail-sendVerification`, {
            method: 'POST',
            body: JSON.stringify({
                email: getMyStateEmail(),
                token: getMyStateToken(),
            }),
        })
    }

    useEffect(() => {
        if (otp.length === 6) {
            fetchData();
            console.log("6");
        }
    }, [otp.length === 6])

    // useEffect(() => {
    //     const tokenData = localStorage.getItem('token');
    //     if (tokenData) {
    //         const parsedData = JSON.parse(tokenData);
    //         const extractedValue = `${parsedData.token}`;
    //         setToken(extractedValue);
    //         console.log(extractedValue);

    //     } else {
    //         console.error('Token data not found in local storage');
    //     }
    // }, [token])



    return (
        <>
            <div>
                <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]">
                    <div className='self-center mr-[106px]'>
                        <a href='/data-diri/changeEmail'>
                            <Image src={ArrowBack} alt='' />
                        </a>
                    </div>
                    <div>
                        <h3 className='font-medium text-white text-[20px] mr-[100px]'>Verifikasi</h3>
                    </div>
                </div>
                <div className='w-screen items-center self-center pt-44'>
                    <div className='m-auto bg-white shadow-2xl rounded-[14px] w-[376px] h-[318px]'>
                        <div className='pt-9 font-medium text-[20px] text-center'>Masukkan Kode Verifikasi</div>
                        <div className='pt-[13px] mb-[52px] font-medium text-[12px] text-center'>Kode verifikasi dikirim ke email {getMyStateEmail()}
                            <div className='absolute ml-[75px]'>
                                {loading && <Loading />}
                            </div>
                        </div>
                        <div className='font-medium text-[12px] flex justify-center '>
                            <OTPInput
                                value={otp}
                                onChange={setOtp}
                                numInputs={6}
                                renderSeparator={<span> <p className='tracking-[10px] text-transparent'>-</p> </span>}
                                renderInput={(props) => <input {...props}
                                    className='border-b-[1px] border-black focus:ring-0 focus:outline-none'
                                />}
                            />
                        </div>
                        {/* 
            
          */}
                        <div className=' font-medium mt-[52px] text-biru-muda text-[12px] text-center cursor-pointer' onClick={() => resendCode()}>Tidak mendapatkan kode verifikasi? Ulangi proses</div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default page