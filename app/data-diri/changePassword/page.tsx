'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Accordion from "../../Accordion/accordion"
import React, { useEffect, useState } from 'react'
import Timer from '../../timer/page'
import Swal from 'sweetalert2'
import { getMyState } from '@/app/global'
import {useRouter} from 'next/navigation'
import { setMyStateReload } from '@/app/global'

const page = () => {
    const [isPasswordVisibleLama, setIsPasswordVisibleLama] = useState(false);
    const [isPasswordVisibleBaru, setIsPasswordVisibleBaru] = useState(false);
    const [isPasswordVisibleKonfirmasi, setIsPasswordVisibleKonfirmasi] = useState(false);
    const [oldPassword, setOldPassword] = useState('');
    const [password, setPassword] = useState('');
    const [confirmation_password, setConfirmation_password] = useState('');
    const [otp, setOtp] = useState('');
    const [checkNewPass, setCheckNewPass] = useState(false);
    const [checkLengthOtp, setCheckLengthOtp] = useState(false);
    const [confirmTrue, setConfirmTrue] = useState(false);
    const [alertNewPass, setAlertNewPass] = useState('none');
    const [backgroundColor, setBackgroundColor] = useState('#D4D4D4');
    const [cursor, setCursor] = useState('auto');
    const [alertConfirm, setAlertConfirm] = useState('auto');
    const [token, setToken] = useState('');
    const [timerExpired, setTimerExpired] = useState(false);
    const [isSeconds, setIsSeconds] = useState(true);
    const [isResendOtp, setIsResendOtp] = useState(true);
    const router = useRouter()

    const handleTogglePasswordLama = () => {
        setIsPasswordVisibleLama(!isPasswordVisibleLama);
    };
    const handleTogglePasswordBaru = () => {
        setIsPasswordVisibleBaru(!isPasswordVisibleBaru);

    };
    const handleTogglePasswordKonfirmasi = () => {
        setIsPasswordVisibleKonfirmasi(!isPasswordVisibleKonfirmasi);
    };

    const checkOldPassword = (event: { target: { value: any; }; }) => {
        setOldPassword(event?.target.value)
        console.log(event.target.value);
    }

    const checkPassword = (event: { target: { value: any; }; }) => {
        const passwordConst = event.target.value;
        const check = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;
        const isPasswordComplex = check.test(passwordConst);
        setCheckNewPass(isPasswordComplex)
        setAlertNewPass(isPasswordComplex ? 'none' : 'block')
        setPassword(event?.target.value)
    }

    const checkConfirmationPassword = (event: { target: { value: any; }; }) => {
        setConfirmation_password(event?.target.value)
        console.log(event.target.value);
    }

    const checkOtp = (event: { target: { value: any; }; }) => {
        setOtp(event?.target.value)
        const len = event.target.value.length
        len == 6 ? setCheckLengthOtp(true) : setCheckLengthOtp(false)
    }

    const isClicked = () => {
        if (backgroundColor == '#D4D4D4') {
            console.log('belum terisi semua')
        }
        else {
            fetchData()
        }
    }

    async function fetchData() {
        try {
            const response = await fetch(`/api/change-password`, {
                method: 'POST',
                body: JSON.stringify({
                    old_password: oldPassword,
                    password: password,
                    confirmation_password: confirmation_password,
                    code: otp,
                    token: token, 
                }),
            })
            const data = await response.json();
            console.log(data.error);
            if (data.error == 'Data not found') {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Terjadi kesalahan saat mengubah password',
                    showConfirmButton: true,
                    width: 450,
                })
                console.error('Error:', data.message);
                // setIsSeconds(false)
                setIsResendOtp(true);
                console.log(isSeconds);
            }
            else if (data.error == undefined) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Berhasil mengubah password',
                    showConfirmButton: true,
                    width: 350,
                })
                .then(() => {
                    localStorage.removeItem('token')
                    setMyStateReload(true)
                    router.push('/')
                })
                setIsSeconds(false)
                setIsResendOtp(false);
                console.log(isSeconds);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    }

    const onTimeExpired = () => {
        setIsSeconds(false);
    };

    const sendOtpSms = () => {
        async function fetchData() {
            try {
                const response = await fetch(`/api/changePassword-send-otp`, {
                    method: 'POST',
                    body: JSON.stringify({
                        send_type: 'sms',
                        token: token,
                    }),
                })
                const data = await response.json();
                if (response.ok && data) {
                    console.log(data);
                } else {
                    console.error('Error:', data.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
    }
    const sendOtpWa = () => {
        async function fetchData() {
            try {
                const response = await fetch(`/api/changePassword-send-otp`, {
                    method: 'POST',
                    body: JSON.stringify({
                        send_type: 'wa',
                        token: token,
                    }),
                })
                const data = await response.json();
                if (response.ok && data) {
                    console.log(data);
                } else {
                    console.error('Error:', data.message);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
    }

    const onResendClick = () => {
        if(getMyState()=="sms"){
            sendOtpSms()
        }
        else if(getMyState()=='wa'){
            sendOtpWa()
        }
        setIsSeconds(true);
    };

    useEffect(() => {
        console.log(getMyState());
        
        if (oldPassword && checkNewPass && confirmTrue && checkLengthOtp && timerExpired == false && alertConfirm == 'none') {
            setBackgroundColor('#48872A')
            setCursor('pointer')
        }
        else {
            setBackgroundColor('#D4D4D4')
            setCursor('auto')
        }
    }, [oldPassword, checkNewPass, confirmTrue, checkLengthOtp, timerExpired,alertConfirm])

    useEffect(() => {
        if (password != confirmation_password && confirmation_password.length >= 1) {
            setAlertConfirm('block')
            // setBackgroundColor('#D4D4D4')
        }
        else if (password == confirmation_password) {
            setAlertConfirm('none')
            // setBackgroundColor('#48872A')
            if (confirmation_password.length >= 8) {
                setConfirmTrue(true)
            }
        }
    }, [password, confirmation_password, confirmTrue])
    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
    }, [token])

    useEffect(() => {
        console.log(timerExpired);
    }, [timerExpired])

    useEffect(() => {
     setBackgroundColor('#D4D4D4')
    }, [])
    

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        isChangePass={true}
                    />
                </div>
                <div className="w-[684px] font-roboto">
                    <div className="text-left mb-6">
                        <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Ganti Kata Sandi</h2>
                    </div>
                    <div className='font-roboto text-[16px] font-normal mb-4'>Kata sandi harus mengandung huruf dan angka. Sedikitnya 6 karakter. Masukkan kata sandi untuk akun pasar id. Kata sandi baru harus berbeda dengan kata sandi lama.</div>
                    <div className='flex justify-start items-center mb-6'>
                        <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Kata Sandi Lama</h3>
                        <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                            <input
                                className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                type={isPasswordVisibleLama ? 'text' : 'password'}
                                placeholder='Masukkan kata sandi lama'
                                value={oldPassword}
                                onChange={checkOldPassword}
                            >
                            </input>
                            <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePasswordLama}>
                                {isPasswordVisibleLama ? 'visibility' : 'visibility_off'}
                            </span>
                        </div>
                    </div>

                    <div className='flex justify-start items-center mb-6'>
                        <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Kata Sandi Baru</h3>
                        <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                            <input
                                className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                type={isPasswordVisibleBaru ? 'text' : 'password'}
                                placeholder='Masukkan kata sandi baru'
                                value={password}
                                onChange={checkPassword}
                            >
                            </input>
                            <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePasswordBaru}>
                                {isPasswordVisibleBaru ? 'visibility' : 'visibility_off'}
                            </span>
                        </div>
                    </div>
                    <div className='text-[10px] text-warna-text-harga-produk mt-[-20px] mb-3 ml-[185px]'
                        style={{ display: alertNewPass }}>
                        Minimal kata sandi 8 karakter terdiri dari huruf besar, huruf kecil, angka, dan simbol
                    </div>

                    <div className='flex justify-start items-center mb-6'>
                        <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Konfirmasi</h3>
                        <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                            <input
                                className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                type={isPasswordVisibleKonfirmasi ? 'text' : 'password'}
                                placeholder='Konfirmasi kata sandi baru'
                                value={confirmation_password}
                                onChange={checkConfirmationPassword}
                            >
                            </input>
                            <span className="material-symbols-outlined self-center" style={{ cursor: 'pointer' }} onClick={handleTogglePasswordKonfirmasi}>
                                {isPasswordVisibleKonfirmasi ? 'visibility' : 'visibility_off'}
                            </span>
                        </div>
                    </div>
                    <div className='text-[10px] text-warna-text-harga-produk mt-[-20px] mb-3 ml-[185px]'
                        style={{ display: alertConfirm }}>
                        Password tidak sesuai!
                    </div>
                    <div className='flex justify-start items-center mb-6'>
                        <h3 className='text-[16px] w-36 h-11 font-normal flex justify-end items-center mr-10'>Kode OTP</h3>
                        <div className="w-[500px] h-[45px] py-[7px] px-[10px] flex border rounded-lg">
                            <input
                                className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[500px] h-[30px]"
                                type='text'
                                placeholder='Masukkan kode OTP'
                                value={otp}
                                onChange={checkOtp}
                            >
                            </input>
                        </div>
                    </div>
                    <div className="flex justify-between">
                    <Timer onTimeExpired={onTimeExpired} isSeconds={isSeconds} />
                    {isSeconds == false && isResendOtp &&
                        <button className='font-semibold text-xs text-biru-muda mt-[-32px]' onClick={()=>onResendClick()}>Kirim ulang kode OTP!</button>
                    }
                    </div>
                    <div className='ml-[184px] flex' onClick={() => isClicked()}>
                        <div className="w-[120px] h-[40px] font-medium text-sm text-white rounded-lg flex justify-center items-center mr-14"
                            style={{ backgroundColor: backgroundColor, cursor: cursor }}>
                            Simpan
                        </div>

                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default page