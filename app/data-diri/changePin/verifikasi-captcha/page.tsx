"use client"
import Image from 'next/image';
import React, { useEffect, useState } from 'react'
import Mail from "../../../..//public/mail_outline.svg"
import Whatsapp from "../../../../public/whatsapp.svg"
import ArrowBack from "../../../../public/arrowback_white.svg"
import { useRouter } from 'next/navigation';
import Loading from '../../../login/loading/page'
import { setMyStateCaptchaProps } from '@/app/global';
import { setMyStateKeyProps } from '@/app/global';
import { setMyStatePhone } from '@/app/global';
import { getMyState } from '@/app/global';
import OTPInput from 'react-otp-input';
import Swal from 'sweetalert2';

const page = () => {
    const router = useRouter()
    const [key, setKey] = useState('');
    const [otp, setOtp] = useState<string>('');
    const [imageCaptcha, setImageCaptcha] = useState("");
    const [token, setToken] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [loading, setLoading] = useState(false);
    const [captcha, setCaptcha] = useState('');
    const [refreshCaptcha, setRefrechCaptcha] = useState(0);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }

        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
                    headers: {
                        Authorization: `Bearer ` + token,
                    },
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                console.log(data.phone);
                setPhoneNumber(data.phone)

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token])

    useEffect(() => {
        fetch('/api/captcha')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
                }
                return response.json();
            })
            .then((data) => {
                setKey(data.data['key']);
                setImageCaptcha(data.data['img'])
            })
            .catch((error) => {
                console.error('Terjadi kesalahan saat fetching data:', error);
            });
    }, [refreshCaptcha])

    useEffect(() => {
        if (otp.length === 6) {
            // setMyStateCaptchaProps(otp)
            // setMyStateKeyProps(key)
            setMyStatePhone(phoneNumber);
            if(getMyState()=='wa'){
                sendOtpWa()
            }
            else if(getMyState()=='sms'){
                sendOtpSms()
            }
            else{
                console.log("error");
                
            }
        }
    }, [otp.length === 6])

    const sendOtpSms = () => {
        async function fetchData() {
            setLoading(true)
            try {
                const response = await fetch(`/api/changePin-otp`, {
                    method: 'POST',
                    body: JSON.stringify({
                        phone :phoneNumber,
                        key : key,
                        captcha : otp,
                        type: 'sms',
                        token: token,
                    }),
                })
                const data = await response.json();
                console.log(data);
                
                if (data.error!='Data not found') {
                    router.push('/data-diri/changePin')
                    setLoading(false)
                } else {
                    setLoading(false)
                    console.log('error');
                    console.error('Error:', data.message);
                    setRefrechCaptcha(refreshCaptcha+1);
                    Swal.fire({
                        icon: 'error',
                        title: 'Captcha salah!',
                        text: 'Coba cek kembali data anda.',
                        timer: 2000,
                        width: 350,
                        showConfirmButton: true,
                      });
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
    }
    const sendOtpWa = () => {
        async function fetchData() {
            setLoading(true)
            try {
                const response = await fetch(`/api/changePin-otp`, {
                    method: 'POST',
                    body: JSON.stringify({
                        phone :phoneNumber,
                        key : key,
                        captcha :otp,
                        type: 'wa',
                        token: token,
                    }),
                })
                const data = await response.json();
                if (data.error!='Data not found') {
                    router.push('/data-diri/changePin')
                    setLoading(false)
                } else {
                    setLoading(false)
                    console.log('error');
                    console.error('Error:', data.message);
                    setRefrechCaptcha(refreshCaptcha+1);
                    Swal.fire({
                        icon: 'error',
                        title: 'Captcha salah!',
                        text: 'Coba cek kembali data anda.',
                        timer: 2000,
                        width: 350,
                        showConfirmButton: true,
                      });
                }   
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
    }
    return (
        <>
            <div>
                <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                    <div className='self-center mr-[106px]'>
                        <button onClick={router.back}>
                            <Image src={ArrowBack} alt='' className='fill-current text-white' />
                        </button>
                    </div>
                    <div>
                        <h3 className='font-medium text-white text-[20px] mr-[100px]'>Verifikasi</h3>
                    </div>
                </div>
                <section className="min-h-screen flex items-center justify-center" >
                    <div className=" border bg-white rounded-lg w-[376px] shadow-lg ">
                        <div className="mt-[36px] mb-[20px]">
                            <h2 className="text-[20px] text-center font-semibold text-[#303030]">Masukkan Captcha</h2>
                            <div className='w-[191px] mx-auto'>
                    <div className="absolute">
                    {loading && <Loading />}
                    </div>
                                <p className="text-center font-semibold text-[12px] font-sans">Pastikan captcha sesuai dengan gambar dibawah</p>
                            </div>
                        </div>
                        <div className="mb-3 flex justify-center">
                            {imageCaptcha != "" ? (
                                <div>
                                    <img src={imageCaptcha} alt="Captcha" />
                                </div>
                            ) : (
                                <p>Mengambil data captcha...</p>
                            )}
                        </div>
                        <div className="mb-3 flex justify-center">
                        <OTPInput
                                value={otp}
                                onChange={setOtp}
                                numInputs={6}
                                renderSeparator={<span> <p className='tracking-[10px] text-transparent'>-</p> </span>}
                                renderInput={(props) => <input {...props}
                                    className='border-b-[1px] border-black focus:ring-0 focus:outline-none'
                                />}
                            />
                        </div>
                    </div>
                </section >
                <p className='text-center pb-[52px] font-roboto font-medium'>© 2023, PT. Bringing Inti Teknologi.</p>
            </div>
        </>
    )
}

export default page