'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Accordion from "../../Accordion/accordion"
import React, { useEffect, useState } from 'react'
import CaptchaModal from './components/modal'
import { setMyState } from '@/app/global'
import { getMyStatePhone } from '@/app/global'
import OTPInput from 'react-otp-input'
import { useRouter } from 'next/navigation'
import Swal from 'sweetalert2'
import Loading from '../../login/loading/page'


const page = () => {
      const [pin, setPin] = useState('');
      const [isDisplay, setIsDisplay] = useState('none');
      const [otp, setOtp] = useState<string>('');
      const [loading, setLoading] = useState(false);
      const [token, setToken] = useState('');
      const [phoneNumber, setPhoneNumber] = useState('');
      const router = useRouter()

      useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }

        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
                    headers: {
                        Authorization: `Bearer ` + token,
                    },
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                console.log(data.phone);
                setPhoneNumber(data.phone)

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token,phoneNumber])


    const handleChange = (e: any) => {
        const value = e.target.value;
        if (/^\d*$/.test(value) && value.length <= 6) {
            setPin(value);
        }
        console.log(value);
    };

    useEffect(() => {
        if (pin.length === 6) {
            setIsDisplay('flex')
        }
    }, [pin.length === 6])

    
    useEffect(() => {
        if (otp.length === 6) {
            updatePin();
        }
    }, [otp.length === 6])

    const updatePin=()=>{
        async function fetchData() {
            setLoading(true)
            try {
                const response = await fetch(`/api/update-pin`, {
                    method: 'POST',
                    body: JSON.stringify({
                        phone :phoneNumber,
                        pin : pin,
                        code : otp,
                        token: token,
                    }),
                })
                const data = await response.json();
                console.log(data);
                
                if (data.error!='Data not found') {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Berhasil mengubah PIN',
                        timer: 2500,
                        width: 350,
                        showConfirmButton: true,
                      }).then(()=>{
                          router.push('/data-diri')
                      })
                    setLoading(false)
                } else {
                    setLoading(false)
                    console.log('error');
                    console.error('Error:', data.message);
                    Swal.fire({
                        icon: 'error',
                        title: 'Kode OTP salah!',
                        text: 'Coba cek kembali data anda.',
                        timer: 2000,
                        width: 350,
                        showConfirmButton: true,
                      });
                }   
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData()
    }

    return (
        <>
            <div>
            </div>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        isChangePin={true}
                    />
                </div>
                <div className="w-[684px] font-roboto flex flex-col items-center">
                    <div className="text-left mb-6">
                        <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Ubah PIN Pembayaran</h2>
                    </div>
                    <div className='font-roboto text-[14px] font-normal mb-4'>Masukkan 6 digit angka untuk melanjutkan proses ubah PIN Pembayaran</div>
                    <div className="pin-input">
                        {[1, 2, 3, 4, 5, 6].map((index) => (
                            <div key={index} className={`pin-circle ${index <= pin.length ? 'filled' : ''}`}></div>
                        ))}
                        <input
                            type="password"
                            value={pin}
                            onChange={handleChange}
                            maxLength={6}
                            className="pin-field"
                        />
                    </div>
                </div>
                <div className='bg-opacity-50 bg-black fixed inset-0 z-30 flex justify-center items-center' style={{ display: isDisplay }}>
                    <div className='bg-white w-[450px] h-[250px] rounded-md'>
                        <div className='ml-2 mt-3'>
                            <svg onClick={(e) => setIsDisplay('none')} className='self-center cursor-pointer' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                            </svg>
                        </div>
                <div className="absolute">
                    {loading && <Loading />}
                    </div>
                        <div className='flex flex-col items-center text-center'>
                            <div className='font-semibold text-base mb-3 mt-3'>Validasi Kode verifikasi</div>
                            <div className='text-sm'>Masukkan kode verifikasi yang telah dikirim ke <span className='font-semibold'>{phoneNumber}</span>. <br />Pasikan kode verifikasi sesuai dengan kode verifikasi yang telah dikirim</div>
                        </div>
                        <div className="mt-4 flex justify-center">
                        <OTPInput
                                value={otp}
                                onChange={setOtp}
                                numInputs={6}
                                renderSeparator={<span> <p className='tracking-[10px] text-transparent'>-</p> </span>}
                                renderInput={(props) => <input {...props}
                                    className='border-b-[1px] border-black focus:ring-0 focus:outline-none'
                                />}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default page