import React, { useEffect, useState } from 'react';

const CaptchaModal = ({
    // show, onClose, onCaptchaComplete
}) => {
    const [captcha, setCaptcha] = useState(''); // Captcha value
    const [key, setKey] = useState('')
    const [imageCaptcha, setImageCaptcha] = useState("")

    const handleSubmit = () => {
        // Assemble the captcha payload
        const captchaPayload = {
            phone: 'phone', // Replace with actual phone value
            key: 'key', // Replace with actual key value
            captcha,
            type: 'type', // Replace with actual type value
        };

        // Perform captcha verification (e.g., make an API call)

        // If captcha verification is successful, notify the parent component
        // onCaptchaComplete(captchaPayload);
    };
    useEffect(() => {
        fetch('/api/captcha')
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`Fetch error: ${response.status} ${response.statusText}`);
                }
                return response.json();
            })
            .then((data) => {
                setKey(data.data['key']);
                setImageCaptcha(data.data['img'])
            })
            .catch((error) => {
                console.error('Terjadi kesalahan saat fetching data:', error);
            });
    }, [])
    return (
        // show
        //  && 
         (
        <div className="captcha-modal">
            {/* Captcha input and verification logic */}
            <input type="text" value={captcha} onChange={(e) => setCaptcha(e.target.value)} />
            <button onClick={handleSubmit}>Submit</button>
            {/* <button onClick={onClose}>Close</button> */}
        </div>
        )
    );
};

export default CaptchaModal;