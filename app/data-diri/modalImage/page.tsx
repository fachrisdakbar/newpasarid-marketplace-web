'use client'
import Image from 'next/image'
import React, { useEffect, useState } from 'react'

const page = ({
  selectedImage = null,
  image = '',
  idPembeli = -0,
  token = '',
}) => {
  const [reload, setReload] = useState("none");
  // const [image, setImage] = useState("");

//   useEffect(() => {
//     async function getImagePembeli() {
//         try {
//             console.log("test");

//             const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Pembeli/${idPembeli}`, {
//                 headers: {
//                     // mode : 'no-cors',
//                     Authorization: `Bearer ${token}`,
//                     'Content-Type': 'application/json',
//                 }
//             });
//             if (!response.ok) {
//                 console.log("error");
//                 throw new Error('Network response was not ok');
//             }
//             const res = await response.json();
//             // const data = res.data
//             console.log(res);
//             setImage(res.image)
//             console.log(res.image);

//             console.log("ini");

//         } catch (error) {
//             console.error('Error fetching data a:', error);
//         }
//     }
//     getImagePembeli()
//     console.log(image);
// }, [token,idPembeli,image,selectedImage])

  return (
    <>
      <div className='w-[228px] h-[228px] flex justify-center items-center mb-4 mt-4 ml-4 mr-4 cursor-pointer'
        onClick={() => setReload('block')}>

        {
          selectedImage ?
            <img src={(URL.createObjectURL(selectedImage))} alt="Selected" className='w-[220px] h-[200px]' />
            :
            <Image width={100} height={100} src={image} alt="Selected" className='w-[220px] h-[200px]' />
            // null
        }

      </div>

      <section className='bg-opacity-50 bg-black fixed inset-0 z-30' style={{ display: reload }}>
        <div className='h-[100vh] flex justify-center items-center'>
          <div className='bg-white w-[400px] h-[400px] rounded-md flex justify-center items-center flex-col'>
            <svg onClick={(e) => setReload('none')} className='mr-[370px] cursor-pointer mb-[10px]' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
            </svg>
            <Image src={image} width={100} height={100} alt='' className='w-[350px] h-[350px] pb-[20px]' />
          </div>
        </div>
      </section>

    </>
  )
}

export default page