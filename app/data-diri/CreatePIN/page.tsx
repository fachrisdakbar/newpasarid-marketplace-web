'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Accordion from "../../Accordion/accordion"
import React, { useEffect, useState } from 'react'
import Swal from 'sweetalert2'
import Loading from '../../login/loading/page'
import { useRouter } from 'next/navigation'

const page = () => {
    const [loading, setLoading] = useState(false);
    const [phone, setPhone] = useState('')
    const [token, setToken] = useState('')
    const router = useRouter()
    const [pin, setPin] = useState('');

    const handleChange = (e: any) => {
        const value = e.target.value;
        if (/^\d*$/.test(value) && value.length <= 6) {
            setPin(value);
        }
        console.log(value);
    };

    const utf8Encode = (text: string): Uint8Array => {
        const encoder = new TextEncoder();
        return encoder.encode(text);
    };

    const sha256Hash = (data: Uint8Array): Promise<string> => {
        return crypto.subtle.digest('SHA-256', data)
            .then(buffer => Array.from(new Uint8Array(buffer))
                .map(byte => byte.toString(16).padStart(2, '0')).join(''));
    };

    const encryptPin = async (pin: string): Promise<string> => {
        const pinUtf8 = utf8Encode(pin);
        const sha256HashValue = await sha256Hash(pinUtf8);

        return sha256HashValue;
    };

    async function fetchDataPembeli() {
        try {
            const response = await fetch(`/api/getUserPembeli`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res.data
            console.log(res);
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }
    async function fetchDataPenjual() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }


    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        async function fetchDataPembeli() {
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                console.log(data);
                setPhone(data.phone)

            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        fetchDataPembeli()
        if (pin.length === 6) {
            encryptPin(pin).then(encryptedPin => {
                console.log("Encrypted PIN:", encryptedPin);

                async function CreatePIN() {
                    try {
                        console.log("test");
                        console.log();

                        const response = await fetch(`/api/CreatePIN`, {
                            method: 'POST',
                            body: JSON.stringify({
                                pin: encryptedPin,
                                token: token,
                                phone: phone
                            })
                        });
                        if (!response.ok) {
                            console.log("error");
                            throw new Error('Network response was not ok');
                        }
                        else if (response.status == 200) {
                            setLoading(false)
                            if (loading == false) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Berhasil membuat PIN Pembayaran',
                                    showConfirmButton: false,
                                    width: 350,
                                    timer: 2000
                                }).then(() => {
                                    router.push('/data-diri')
                                })
                            }
                        }
                        const res = await response.json();

                    } catch (error) {
                        console.error('Error fetching data a:', error);
                    }
                }
                CreatePIN()
            });
        } else {
        }
    }, [pin, token]);

    return (
        <>
            <div>
            </div>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] h-[855px] px-20" >
                <div className='w-[256px]  mr-10 '>
                    <Accordion
                        // isPembeli={isPembeli}
                        isChangePin={true}
                    />
                </div>
                <div className="w-[684px] font-roboto flex flex-col items-center">
                    <div className="text-left mb-6">
                        <h2 className="text-[20px] font-bold text-black-#0A0A0A text-left font-roboto">Buat PIN Pembayaran</h2>
                    </div>
                    <div className='font-roboto text-[14px] font-normal mb-4'>Masukkan 6 digit angka untuk membuat PIN Pembayaran menggunakan saldo</div>
                    <div className="pin-input">
                        {[1, 2, 3, 4, 5, 6].map((index) => (
                            <div key={index} className={`pin-circle ${index <= pin.length ? 'filled' : ''}`}></div>
                        ))}
                        <input
                            type="password"
                            value={pin}
                            onChange={handleChange}
                            maxLength={6}
                            className="pin-field"
                        />
                    </div>
                    {loading &&
                        <Loading />
                    }
                </div>
            </div>
            <Footer />
        </>
    )
}

export default page