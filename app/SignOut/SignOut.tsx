"use client"
import { useRouter } from 'next/navigation'
import React, { useState } from 'react'
import Loading from '../login/loading/page'

const SignOut = () => {
    const [isDisplay, setIsDisplay] = useState('none')
    const [loading, setLoading] = useState(false);
    const router = useRouter();
    const signOut = () => {
        setLoading(true);
        localStorage.removeItem('token')
        localStorage.removeItem('type')
        localStorage.removeItem('isLoggedIn')
        router.push('/')
        setLoading(false);
    }
    return (
        <div>
            <div onClick={() => setIsDisplay('block')} style={{ cursor: 'pointer' }}>Keluar</div>
            <section className='bg-opacity-50 bg-black fixed inset-0 z-30' style={{ display: isDisplay }}>
                {loading &&
                    <Loading />
                }
                <div className='w-[100vw] h-[100vh] flex justify-center items-center'>
                    <div className='bg-white h-[200px] w-[400px] flex justify-center rounded-md'>
                        <div className=''>
                            <div className='pt-6 font-semibold flex justify-center'>Konfirmasi</div>
                            <div className='text-xs pt-4 flex justify-center mb-11'>Keluar akun?</div>
                            <div className='flex text-xs self-end font-semibold'>
                                <button className='bg-abu-abu-terang text-[#788E39] p-[10px] w-[150px] rounded-full mr-6' onClick={() => setIsDisplay('none')}>Tidak</button>
                                <button className='bg-[#788E39] text-white p-[10px] w-[150px] rounded-full' onClick={() => signOut()}>Ya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default SignOut