"use client"
import React, { useEffect, useState } from 'react'
import cheerio, { AnyNode } from 'cheerio';
import ArrowBack from '../../public/arrowback_white.svg'
import { useRouter } from 'next/navigation';
import Image from 'next/image';


export default function syaratKetentuan() {
    const [title, setTitle] = useState('');
    const cheerio = require('cheerio');
    const [description, setDescription] = useState('');
    const [textContent, setTextContent] = useState('');

    const extractTextFromHtml = (html: string | AnyNode | AnyNode[] | Buffer) => {
        const $ = cheerio.load(html);
        return $('body').text();
    };
    // untuk nge get data syarat ketentuan
    useEffect(() => {
        fetch("https://api.pasarid-dev.bitcorp.id/api/content?type=syarat-ketentuan")
            .then((response) => response.json())
            .then((data) => {

                setTitle(data.title);
                setDescription(data.description);
            })
            .catch(error => console.error('Error fetching key:', error));
    }, []);
    const router = useRouter()

    return (
        <>
        <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                <div className='self-center mr-[106px]'>
                    <button onClick={router.back}>
                        <Image src={ArrowBack} alt='' className='fill-current text-white' />
                    </button>
                </div>
                <div>
                    <h3 className='font-medium text-white text-[20px] mr-[100px]'>Syarat & Ketentuan</h3>
                </div>
            </div>
        <section>
            <div dangerouslySetInnerHTML={{ __html: description }} className='py-5 px-5' />
        </section>
        </>
    )
}