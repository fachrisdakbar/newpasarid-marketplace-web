import React from 'react'
import { useEffect } from 'react'
import Image from 'next/image'
import Head from 'next/head'
import Script from 'next/script'
import 'bootstrap/dist/css/bootstrap.css'
import "react-responsive-carousel/lib/styles/carousel.min.css";
// import { Carousel } from 'react-responsive-carousel';
import Categories from './Content/Category'
import Market from './Content/Market'
import Product from './Content/Product'
import Banner from './Content/Banner'
import Link from 'next/link'
import Saldo from './Saldo'


const HomeContent = ({ }) => {
  return (
    <>
      <div className='home-content-wrapper'>
        <Banner />

        <div className='mt-3'>
          <Saldo />
        </div>

        <div className="kategori flex justify-between pt-5">
          <div className='text-[24px] font-semibold'>Kategori</div>
          <a href={'/Category'} className='no-underline font-semibold text-[20px] text-[#788E39] cursor-pointer'>Lihat Semua</a>
        </div>
        <Categories />

        <div className="flex justify-between pt-5">
          <div className='text-[24px] font-semibold'>Pasar Terdekat</div>
          <a href={'/market'} className='no-underline font-semibold text-[20px] text-[#788E39] cursor-pointer'>Lihat Semua</a>
        </div>
        <Market />

        <div className="flex justify-between pt-5">
          <div className='text-[24px] font-semibold'>Produk Terbaru</div>
          <a href={'/Product'} className='no-underline font-semibold text-[20px] text-[#788E39] cursor-pointer'>Lihat Semua</a>
        </div>
        <Product />
        <div className='mt-[60.48px]'></div>
      </div>
    </>
  )

}


export default HomeContent