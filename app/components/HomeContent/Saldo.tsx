'use client'
import React, { useEffect, useState } from 'react'
import Wallet from '../../../public/wallet.svg'
import History from '../../../public/history.svg'
import Plus from '../../../public/Plus.svg'
import BRIVA from '../../../public/Briva.svg'
import Image from 'next/image'
import { useRouter } from 'next/navigation'

interface AlamatData {
    id: number,
    nama: string; // Assuming alamat_id_utama is a number, adjust the type accordingly
    // Define other properties here if necessary
}

const Saldo = ({ }) => {
    const [token, setToken] = useState('');
    const [isPembeli, setIsPembeli] = useState(false);
    const [loggedin, setLoggedin] = useState(true);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [saldo, setSaldo] = useState('');
    const [alamatId, setAlamatId] = useState();
    const [idPembeli, setIdPembeli] = useState();
    const [alamat, setAlamat] = useState('');
    const [isDisplay, setIsDisplay] = useState('none');
    const [displayHistory, setDisplayHistory] = useState('none');
    const [va, setVa] = useState('');
    const [loading, setLoading] = useState(false);
    const route = useRouter();

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        const typeUser = localStorage.getItem('type');

        if (typeUser == 'pembeli') {
            setIsPembeli(true)
        } else if (typeUser == 'penjual') {
            setIsPembeli(false)
        }
        else {
            setLoggedin(false);
        }

        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        async function fetchDataPembeli() {
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                console.log(data);
                setVa(data.pembeli.va)
                setIdPembeli(data.pembeli.id)
                setAlamatId(data.pembeli.alamat_id_utama)
                setPhoneNumber(data.pembeli.phone)

            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        async function fetchDataPenjual() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res
                console.log();

            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        async function fetchData() {
            try {
                setLoading(true);
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/payment/get-balance?phone=${phoneNumber}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                console.log(data);
                setSaldo(data.response.amount)
                setLoading(false);


            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        async function fetchDataAlamat() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Alamat?where=pembeli_id,${idPembeli}&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: AlamatData[] = await response.json(); // Specify the type of data as AlamatData[]
                console.log(data);

                // Loop through the data array to find alamat_id_utama equal to 431
                data.forEach((item: AlamatData) => { // Specify the type of item as AlamatData
                    if (item.id === alamatId) {
                        // Do something with the item where alamat_id_utama is 431
                        console.log('Found item with alamat_id_utama 431:', item);
                        setAlamat(item.nama);
                    }
                });

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        { isPembeli && fetchDataPembeli() && fetchDataAlamat() }
        { isPembeli == false && fetchDataPenjual() }
        fetchData();
        // fetchDataAlamat();

    }, [token, phoneNumber, loggedin, isPembeli, idPembeli]);

    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    return (
        <>
            <div className='flex justify-between'>
                <div className='bg-[#D6E6A9] w-[35vw] h-[10vh] flex items-center justify-between pl-5 pr-5' style={{ borderRadius: '10px' }}>
                    <div className="flex flex-col">
                        <div className='flex items-center text-[#788E39]'>
                            <div className=''>
                                <Image src={Wallet} alt='' width={140} height={140}
                                    className='w-[15px] h-[15px] mr-2'
                                />
                            </div>
                            <div className='text-base font-medium'>Saldo</div>
                        </div>
                        <div className='font-semibold text-[#788E39] text-base'>{loggedin ?
                            loading == true ?
                                <div className='flex flex-col justify-center items-center'>
                                    <div className='loadingContainerSaldo'>
                                        <div className='text-base font-medium'>Loading...</div>
                                    </div>
                                </div>
                                :
                                "Rp " + formatNumber(saldo) : '-'}</div>
                    </div>
                    <div className='flex justify-center items-center gap-10'>
                        <div className="flex flex-col cursor-pointer" onClick={() => loggedin ? route.push('/RiwayatSaldo') : ''}>
                            <div className='flex items-center justify-center text-[#788E39]'>
                                <div className='text-base'>
                                    <Image src={History} alt='' width={140} height={140}
                                        className='w-[25px] h-[25px]'
                                    />
                                </div>
                            </div>
                            <div className='font-semibold text-[#788E39] text-base'>Riwayat</div>
                        </div>
                        <div className="flex flex-col cursor-pointer" onClick={() => { loggedin ? route.push('/IsiSaldo') : '' }}>
                            <div className='flex items-center justify-center text-[#788E39]'>
                                <div className='text-base'>
                                    <Image src={Plus} alt='' width={140} height={140}
                                        className='w-[20px] h-[23px]'
                                    />
                                </div>
                            </div>
                            <div className='font-semibold text-[#788E39] text-base'>Isi Ulang</div>
                        </div>
                    </div>
                </div>
                <div className='bg-[#D6E6A9] w-[35vw] h-[10vh] pl-5 pr-5 flex items-center justify-between text-sm font-semibold text-[#788E39] cursor-pointer'
                    style={{ borderRadius: '10px' }}
                    onClick={() => { loggedin ? route.push('/daftar-alamat') : '' }}>
                    <div>
                        Alamat pengiriman
                    </div>
                    {loggedin ?
                        <div className='flex items-center'>
                            <div className='mr-2'>
                                {alamat}
                            </div>
                            <span className="material-symbols-outlined">
                                expand_more
                            </span>
                        </div>

                        : '-'
                    }
                </div>
            </div>
        </>
    )
}

export default Saldo