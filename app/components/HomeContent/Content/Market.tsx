"use client"
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';

interface Market {
  id: number;
  nama: string;
  distance: number;
  image: string;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  deleted_at: null | string;
}

function Market() {
  const [markets, setMarkets] = useState<Market[]>([]);
  const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';
  const [currPage, setCurrPage] = useState(1);
  const dataPerPage = 5;
  const lastIdx = dataPerPage * currPage;
  const firstIdx = lastIdx - dataPerPage;
  const datas = markets.slice(firstIdx, lastIdx);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/nearby/pasar?lat=-6.230437&long=106.780375&radius=10&distance=km', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data: Market[] = await response.json();
        setMarkets(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }
    fetchData();
  }, []);

  return (
    <div className='kategori-data mt-6 flex justify-between'>
      {datas.map((market) => (
    <a className='no-underline' href={`/market/${market.id}`}>
        <div key={market.id} className=''>
          <div className='items-center'>
            {market.image &&
              <Image width={140} height={140} src={market.image} alt=''
                className='w-[12vw] h-[20vh] rounded-xl cursor-pointer' />}
            <div>
              <p className='text-[14px] font-medium w-[12vw]  mt-3 cursor-pointer text-black'>{market.nama}</p>
              <div className='text-[#788E39] text-[14px] mt-[-10px]'>{(Math.floor(market.distance * 100) / 100) + " km"}</div>
            </div>
          </div>
        </div>
        </a>
      ))}
    </div>
  );
}

export default Market;
