"use client"
import React from 'react';
import Carousel from 'react-material-ui-carousel'
import Image from 'next/image';
import { useEffect, useState } from 'react';

interface Product {
    id: number;
    nama: string;
    harga: number;
    image: string;
    sold: number;
    berat: number;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

function Banner() {

    const [products, setProducts] = useState<Product[]>([]);
    const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 5;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = products.slice(firstIdx, lastIdx);

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Banner', {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Product[] = await response.json();
                setProducts(data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        fetchData();
    }, []);

    return (
        <Carousel
        indicators={false}
        animation='slide'
        navButtonsAlwaysVisible={true}
        >
            {
                datas.map((item) => (
                    <div key={item.id}>
                        <div className='items-center'>
                            <Image width={140} height={140} src={item.image ? item.image : ''} alt='' className='w-[100%] h-[377px] cursor-pointer' />
                        </div>
                    </div>
                ))
            }
        </Carousel>
    )
}

export default Banner