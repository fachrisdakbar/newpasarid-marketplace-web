"use client"
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useState } from 'react';

interface Category {
  id: number;
  name: string;
  description: string;
  image: string;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  deleted_at: null | string;
}

function Categories() {
  const [categories, setCategories] = useState<Category[]>([]);
  const token = '8970|kyBrnWDwbCUe7CktWwrZUIj5prDZ20ALxNfjerWg';
  const [currPage, setCurrPage] = useState(1);
  const dataPerPage = 5;
  const lastIdx = dataPerPage * currPage;
  const firstIdx = lastIdx - dataPerPage;
  const datas = categories.slice(firstIdx, lastIdx);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Category?get=*', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data: Category[] = await response.json();
        setCategories(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }
    fetchData();
  }, []);

  return (
    <div className='kategori-data mt-6 flex justify-between'>
      {datas.map((category) => (
        <a className='no-underline flex flex-col items-center' href={`/Category/${category.id}`}>
          <div key={category.id} className='cursor-pointer flex items-center justify-center shadow-md w-[10vw] h-[16vh]' style={{ borderRadius:'10px'}}>
            <div className='flex items-center'>
              <Image width={100} height={100} src={category.image} alt='' className='w-[7vw] h-[10vh] ' />
            </div>
          </div>
            <div className='no-underline text-black font-bold text-[14.16px] mt-3 flex items-center'>{category.name}</div>
        </a>
      ))}
    </div>
  );
}

export default Categories;
