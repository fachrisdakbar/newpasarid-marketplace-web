'use client'
import Image from 'next/image'
import React, { useEffect, useState } from 'react'
import Logo from '../../public/logoPasarId.svg'
import LoginPage from '../../app/login/page'
import Profile from '../Profile/page'
import Link from 'next/link'
import { useRouter, useSearchParams } from 'next/navigation'
import useSWR from "swr";

const Header = () => {
    const router = useRouter();
    const [searchQuery, setSearchQuery] = useState("");
    const search = useSearchParams();
    const [thisLogin, setThisLogin] = useState(true);


    const updateQueryStringParameter = (uri: any, key: any, value: any) => {
        const re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        const separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    const onSearch = (event: React.FormEvent) => {
        event.preventDefault();
        const encodedSearchQuery = encodeURI(searchQuery);
        router.push(`../../search?q=${encodedSearchQuery}`)
        const updatedURL = updateQueryStringParameter(window.location.href, 'q', encodedSearchQuery);
    }

    return (
        <div className='box shadow-md'>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className='headerProperty flex justify-between'>
                <div className='wrapperLogo w-auto h-28 flex items-center'>
                    <a href='/'>
                        <Image
                            src={Logo}
                            alt=''
                            className='img-logo w-32 h-16'
                        />
                    </a>
                </div>
                <div className='container-search justify-center p-[12px] items-center self-center mr-6'>
                    <form onSubmit={onSearch}>
                        <div className='w-[80vw] searchBar flex items-center border border-gray-300 rounded-md border-solid p-2'>
                            <span className="mr-2 material-symbols-outlined">search</span>
                            <div className=''>
                                <input
                                    value={searchQuery}
                                    onChange={(event) => setSearchQuery(event.target.value)}
                                    placeholder="Cari disini"
                                    className='font-roboto font-normal placing focus:ring-0 focus:outline-none text-xs' />
                            </div>
                        </div>
                    </form>
                </div>
                <div>
                    <LoginPage
                        thisLogin={thisLogin}
                    />
                </div>
            </div>
        </div>
    )
}

export default Header