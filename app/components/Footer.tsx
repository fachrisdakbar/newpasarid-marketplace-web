import Image from 'next/image'
import React from 'react'
import Facebook from '../../public/devicon_facebook.svg'
import Instagram from '../../public/skill-icons_instagram.svg'
import Twitter from '../../public/fa6-brands_square-x-twitter.svg'
import Tiktok from '../../public/logos_tiktok-icon.svg'
import Logo from '../../public/logoPasarId.svg'
import Playstore from '../../public/google-play.svg'
import Appstore from '../../public/app-store.svg'
import Link from 'next/link'


const Footer = () => {
  return (
    <div className='footer-box flex justify-between pt-6 pb-[44.93px]'>
      <div className='footer-wrapper pl-[7%] pr-[7%] flex justify-between'>
        <div className='footer-left'>
          <a className='no-underline text-black font-bold text-[20px] font-roboto cursor-pointer'>Bantuan dan Panduan</a>
          <a href='../../Bantuan-panduan' className='no-underline text-black font-medium text-[16px] font-roboto cursor-pointer'>Bantuan dan Panduan</a>
          <a href='../../s&k' className='no-underline text-black font-medium text-[16px] font-roboto cursor-pointer'>Syarat dan Ketentuan</a>
          <a href='../../kebijakan-privasi' className='no-underline text-black font-medium text-[16px] font-roboto cursor-pointer'>Kebijakan Privasi</a>
          <a href='../../Hubungi-kami' className='no-underline text-black font-medium text-[16px] font- cursor-pointer'>Hubungi Kami</a>
          <a href='../../FAQ' className='no-underline text-black font-medium text-[16px] font-roboto cursor-pointer'>FAQ</a>
          <a className="no-underline text-black font-bold text-[20px] mt-8 font-roboto">Ikuti Kami</a>
          <div className='flex gap-4'>
            <div className=''>
              {/* <Link href={''}> */}
              <Image src={Facebook} alt='' className='w-[30px] h-[30px]' />
              {/* </Link> */}
            </div>
            <div className=''>
              {/* <Link href={''}> */}
              <Image src={Instagram} alt='' className='w-[30px] h-[30px]' />
              {/* </Link> */}
            </div>
            <div className=''>
              {/* <Link href={''}> */}
              <Image src={Twitter} alt='' className='w-[30px] h-[30px]' />
              {/* </Link> */}
            </div>
            <div className=''>
              {/* <Link href={''}> */}
              <Image src={Tiktok} alt='' className='w-[30px] h-[30px]' />
              {/* </Link> */}
            </div>
          </div>
        </div>
        <div className='footer-right self-center text-center'>
          <div className='footer-logo self-center text-center'>
            {/* <Link href='https://pasar.id/'> */}
              <Image src={Logo} alt='' className='w-[256px] h-[136.93px] mt-[-1.75vh]' />
            {/* </Link> */}
          </div>
          <div className='flex self-center text-center gap-5 mb-[-1vh]'>
            <div className='playAppStore cursor-pointer'>
              <Image src={Playstore} alt='' className='w-32 h-10' />
            </div>
            <div className='playAppStore cursor-pointer'>
              <Image src={Appstore} alt='' className='w-32 h-10' />
            </div>
          </div>
          <div>
            <div className='font-semibold font-roboto text-[16px] mt-[1.5vh]'>Mitra Kami</div>
            <div className='font-medium font-roboto text-[20px] mb-[1vh]'>Bank Rakyat Indonesia</div>
          </div>
          <div className='font-medium font-roboto text-sm mt-[1vh]'>© 2023, PT. Bringin Inti Teknologi.</div>
        </div>
      </div>
    </div>
  )
}

export default Footer