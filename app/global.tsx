let myState: string = '';
let myStatePhone: string = '';
let myStatePhoneProps: string = '';
let myStateKeyProps: string = '';
let myStateCaptchaProps: string = '';
let myStateNama: string = '';
let myStateNamaPenerima: string = '';
let myStateAlamat: string = '';
let myStateReload: boolean;
let myStateEmail: string;
let myStateToken: string;
let myStateId: number;


export const setMyState = (value: string) => {
  myState = value;
}

export const getMyState = () => {
  return myState;
}
export const setMyStatePhone = (value: string) => {
  myStatePhone = value;
}

export const getMyStatePhone = () => {
  return myStatePhone;
}
export const setMyStatePhoneProps = (value: string) => {
  myStatePhoneProps = value;
}

export const getMyStatePhoneProps = () => {
  return myStatePhoneProps;
}

export const setMyStateKeyProps = (value: string) => {
  myStateKeyProps = value;
}

export const getMyStateKeyProps = () => {
  return myStateKeyProps;
}

export const setMyStateCaptchaProps = (value: string) => {
  myStateCaptchaProps = value;
}

export const getMyStateCaptchaProps = () => {
  return myStateCaptchaProps;
}
export const setMyStateNama = (value: string) => {
  myStateNama = value;
}

export const getMyStateNama = () => {
  return myStateNama;
}
export const setMyStateNamaPenerima = (value: string) => {
  myStateNamaPenerima = value;
}

export const getMyStateNamaPenerima = () => {
  return myStateNamaPenerima;
}
export const setMyStateAlamat = (value: string) => {
  myStateAlamat = value;
}

export const getMyStateAlamat = () => {
  return myStateAlamat;
}
export const setMyStateReload = (value: boolean) => {
  myStateReload = value;
}

export const getMyStateReload = () => {
  return myStateReload;
}
export const setMyStateEmail = (value: string) => {
  myStateEmail = value;
}

export const getMyStateEmail = () => {
  return myStateEmail;
}
export const setMyStateToken = (value: string) => {
  myStateToken = value;
}

export const getMyStateToken = () => {
  return myStateToken;
}
export const setMyStateId = (value: number) => {
  myStateId = value;
}

export const getMyStateId = () => {
  return myStateId;
}
