'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Image from 'next/image';
import Link from 'next/link';
import { useParams } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import Toko from '../../../../public/toko.svg'
import Star from '../../../../public/star.svg'
import Clock from '../../../../public/clock.svg'
import Modal from './Modal';

interface Product {
  id: number;
  nama: string;
  deskripsi: string;
  harga: number,
  sold: number,
  berat: number,
  image: string;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  deleted_at: null | string;
}
interface Product {
  id: number;
  nama: string;
  deskripsi: string;
  harga: number,
  sold: number,
  berat: number,
  image: string;
  created_by: number;
  updated_by: number;
  created_at: string;
  updated_at: string;
  deleted_at: null | string;
  penjual: {
    nama_toko: string,
  }
}

const page = () => {

  const [products, setProducts] = useState<Product[]>([]);
  const token = '8970|kyBrnWDwbCUe7CktWwrZUIj5prDZ20ALxNfjerWg';
  const [currPage, setCurrPage] = useState(1);
  const [rating, setRating] = useState(0);
  const [visible, setVisible] = useState(true);
  const [shopAvailable, setShopAvailable] = useState("");
  const [namaToko, setNamaToko] = useState();
  const [totalSold, setTotalSold] = useState(0);
  const dataPerPage = 100;
  const lastIdx = dataPerPage * currPage;
  const firstIdx = lastIdx - dataPerPage;
  const datas = products.slice(firstIdx, lastIdx);
  const { id, subId } = useParams();

  useEffect(() => {
    async function fetchData() {
      try {
        const responseToko = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Penjual?where=id,${subId}&with=pasar&get=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        // const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?where=penjual_id,${subId}&get=*`, {
        //   headers: {
        //     Authorization: `Bearer ${token}`,
        //   },
        // });
        const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?with[]=penjual(withCount=produk)&where=pasar_id,${id}&where=penjual_id,${subId}&get=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        const data = await response.json();
        console.log(data);
        const dataToko = await responseToko.json();
        setRating((Math.floor(dataToko[0].rating * 10) / 10))

        setNamaToko(dataToko[0]);
        setProducts(data);
        // console.log(dataToko);
        const now = new Date();
        const hours = now.getHours().toString().padStart(2, '0');
        const minutes = now.getMinutes().toString().padStart(2, '0');
        const jamBuka = `${hours}:${minutes}` >= dataToko[0].jam_buka
        const jamTutup = `${hours}:${minutes}` <= dataToko[0].jam_tutup
        const isActive = dataToko[0].status == "Aktif" ? true : false

        let temp = null;
        for (let i = 0; i < data.length; i++) {
          temp = data[i]?.sold! + temp!;
          console.log("data ke " + [i] + " " + data[i]?.sold);
          console.log("hasil " + temp);
        }

        setTotalSold(temp);

        if (isActive) {
          if (jamBuka && jamTutup) {
            setShopAvailable("Buka")
          }
          else {
            setShopAvailable("Tutup")
          }
        }

      } catch (error) {
        console.error('Error fetching data:', error);
      }
    }
    fetchData();
  }, []);

  const formatNumber = (value: any) => {
    return value.toLocaleString('id-ID', {
      useGrouping: true,
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
      minimumIntegerDigits: 1,
      style: 'decimal',
    });
  }

  const jamToko = () => {
    if (shopAvailable == "Buka") {
      return <div className='text-[10px] font-medium bg-green-700 rounded-xl text-white p-[6px]'>Buka</div>
    } else {
      return <div className='text-[10px] font-medium bg-gray-500 rounded-xl text-white p-[6px]'>Tutup</div>
    }
  }

  return (
    <>
      <Header />
      <div className='bg-gradient-to-t from-hijau pt-[33px] pb-[33px]'>
        <div className='shadow-lg ml-[165px] mr-[165px] bg-white'>
          <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
            <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
            <a href={'/market'} className='text-[14px] mr-1 no-underline'>market  </a>
            <a href={`/market/${id}`} className='text-[14px] mr-1 no-underline'>{namaToko ? " / " + namaToko?.['pasar']['nama'] : ''}</a>
            <a href={'/market'} className='text-[14px] mr-1 no-underline'>{" / " + products[0]?.penjual.nama_toko}</a>
          </div>

          <div className="flex flex-col items-center mt-5">
            <div className='flex flex-col justify-center items-center self-center'>

              <Modal
                namaToko={namaToko}
              />
              <div className='flex flex-col '>
                <div className="flex">
                  <div className='font-semibold font-roboto text-[30px] mb-1'>{namaToko?.['nama_toko']}</div>
                  <div className='flex items-center ml-2'>{jamToko()}</div>
                </div>
                <div className='text-green-700 text-[12px] border-solid border-green-700 border-[1.5px] flex justify-center p-1 rounded-md mb-2'>{namaToko?.['kategori_toko']}</div>
                <div className="flex mb-2">
                  <div>
                    <Image src={Clock} alt='' width={100} height={100} className='w-[15px] h-[15px] mr-2' />
                  </div>
                  <div className='text-[12px]'>{namaToko?.['jam_buka'] + "  -  "}</div>
                  <div className='text-[12px]'>{namaToko?.['jam_tutup']}</div>
                </div>
                <div className="flex">
                  <div>
                    <Image src={Toko} alt='' width={140} height={140}
                      className='w-[20px] h-[20px]'
                    />
                  </div>
                  <div className='font-semibold text-[16px] text-warna-text-title-produk ml-2'>{namaToko?.['pasar']['nama']}</div>
                </div>
                <div>
                  <div className='flex h-[30px] items-center mb-10 '>
                    <div className='flex'>
                      <Image src={Star} alt='' width={140} height={140}
                        className='w-[12px] h-[12px] mr-[6px]'
                      />
                      <div className='text-warna-text-title-produk text-[12px] font-normal'>{rating + " / 5"}</div>
                    </div>
                    <div className='mr-2 ml-2'>|</div>
                    <div className='text-warna-text-title-produk text-[12px] font-normal'>{totalSold + " produk terjual"}</div>

                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>

      <div className='home-content-wrapper shadow-lg mt-3'>
        <div className='flex justify-between pt-6'>
          <div className='ml-7 font-normal text-[24px] text-abu-abu'>Produk Dari Toko</div>
        </div>

        <div className='flex justify-between grid grid-cols-5 gap-4 pl-7 pr-7'>
          {datas.map((product) => (
            <div key={product.id}>
              <div className='items-center no-underline mb-6 mt-6'>
                <a className='no-underline' href={`/Product/${product.id}`}>
                  <Image width={140} height={140} src={product.image ? product.image : ""} alt=''
                    className='w-[191.08px] h-[191.08px] cursor-pointer' />
                  <div className='font-sans text-warna-text-title-produk font-semibold text-[14.7px] w-[135px] h-[25px] mt-[3.67px] truncate cursor-pointer'>{product.nama + " - " + product.berat + " g"}</div>
                  <div className='flex justify-between items-center'>
                    <div className='font-sans font-semibold text-warna-text-harga-produk text-[17.5px] flex items-center'>{"Rp " + formatNumber(product ? product?.harga : 0)}</div>
                    <div className='text-[12.25px] font-normal flex items-center text-warna-text-sold-produk h-8 mr-4'>{product.sold == null ? "0 terjual" : product.sold + " terjual"}</div>
                  </div>
                </a>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Footer />
    </>
  )
}

export default page