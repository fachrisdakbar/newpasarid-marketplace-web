'use client'
import NoData from '@/app/market/[id]/NoData';
import Footer from '@/app/components/Footer';
import Header from '@/app/components/Header';
import Image from 'next/image';
import Link from 'next/link';
import { useParams } from 'next/navigation';
import React, { useEffect, useState } from 'react'

interface Toko {
    id: number;
    nama: string;
    penjual: {
        id: number,
        image: string,
        nama_toko: string,
        jam_buka: string,
        jam_tutup: string,
        alamat: string,
        kategori_toko: string,
        produk_count: number,
    }
    description: string;
    image: string;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

const page = () => {
    const [tokos, setTokos] = useState<Toko[]>([]);
    const token = '8970|kyBrnWDwbCUe7CktWwrZUIj5prDZ20ALxNfjerWg';
    const [currPage, setCurrPage] = useState(1);
    const [visible, setVisible] = useState(true);
    const [namaPasar, setNamaPasar] = useState("");
    const dataPerPage = 100;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = tokos.slice(firstIdx, lastIdx);
    const { id } = useParams();

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?with[]=penjual(withCount=produk)&where=pasar_id,${id}&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                const responseNamaPasar = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Pasar?withCount=penjuals&where=id,${id}&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setTokos(data);
                console.log(data);
                
                const dataNamaPasar = await responseNamaPasar.json()
                console.log(data.length);

                setNamaPasar(dataNamaPasar[0].nama)
                if (data.length != 0) {
                    setVisible(true)
                    console.log("true");

                }
                else if (data.length == 0) {
                    setVisible(false)
                    console.log("false");
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, []);

    return (
        <>
            <Header />
            <div className='home-content-wrapper shadow-lg pb-12'>
                <div className='flex self-center text-center items-center'>
                    {/* <Link href={'/'}>
                        <Image width={100} height={100} src={Arrow} alt='' className='w-[20px] h-[20px] mr-3' />
                    </Link>
                    <div className='text-[24px] text-abu-abu'>Produk</div> */}
                    <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                        <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
                        <a href={'/market'} className='text-[14px] mr-1 no-underline'>Market</a>
                        <div className='text-[14px]'>{" / " + namaPasar}</div>
                    </div>
                </div>
                {
                    visible ?
                        <div className='mt-6 flex justify-between grid grid-cols-3 gap-5 pl-6 pr-6'>
                            {datas.map((toko) => (
                                <a className='no-underline' href={`/market/${id}/${toko.penjual ? toko?.penjual?.id : ""}`}>
                                    <div key={toko.penjual ? toko.penjual.id : ""} className='flex flex-row'>
                                        <Image width={140} height={140} src={toko.penjual ? toko.penjual.image : ""} alt=''
                                            className='w-[180px] h-[165px] cursor-pointer rounded-md' />
                                        <div className='self-center'>
                                            <div className='font-sans font-semibold text-black text-[15px] ml-4 flex items-center'>{toko.penjual ? toko.penjual.nama_toko : ""}</div>
                                            <div className="">
                                                <div className='font-sans font-semibold text-abu-abu text-[12px] ml-4 mt-1 w-[100px] flex items-center'>{toko.penjual ? toko.penjual.produk_count + " produk" : ""}</div>
                                                <div className='text-[12px] font-normal flex items-center text-center justify-center ml-4 p-1 mt-2 text-green-700 border-[2px] rounded-md border-green-700'>{toko.penjual ? toko.penjual.kategori_toko : ""}</div>
                                            </div>
                                        </div>
                                    </div>
                                 </a>
                            ))}
                        </div>
                        :
                        <NoData />
                }
            </div>
            <Footer />
        </>

    )
}

export default page