import Image from 'next/image'
import React from 'react'
import NotFound from '../../../public/notFound.svg'

const NoData = () => {
  return (
    <div className='h-[40vh] pl-6 pr-6 flex flex-col justify-center items-center self-center'>
      <div className="w-[400px] h-[350px]">
        <Image src={NotFound} alt='' width={140} height={140} className='w-[320px] h-[320px] ml-8'/>
      </div>
        <div className='font-medium text-base absolute mt-[37vh]'>Pasar ini belum memiliki toko yang tersedia</div>
    </div>
  )
}

export default NoData