'use client'
import React, { useEffect, useState } from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Arrow from '../../public/arrow_back.svg'
import Image from 'next/image';
import Link from 'next/link';
import Location from '../../public/icon-location.svg'

interface Market {
    id: number;
    nama: string;
    distance: number;
    image: string;
    alamat: string,
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

const page = () => {
    const [markets, setMarkets] = useState<Market[]>([]);
    const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 7;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = markets.slice(firstIdx, lastIdx);

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/nearby/pasar?lat=-6.230437&long=106.780375&radius=10&distance=km', {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Market[] = await response.json();
                setMarkets(data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, []);

    return (
        <>
            <Header />
            <div className='home-content-wrapper shadow-lg pb-12'>
                <div className='flex self-center text-center items-center'>
                    {/* <Link href={'/'}>
                        <Image width={100} height={100} src={Arrow} alt='' className='w-[20px] h-[20px] mr-3' />
                    </Link>
                    <div className='text-[24px] text-abu-abu'>Pasar</div> */}
                    <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                        <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
                        <a href={'/market'} className='text-[14px] mr-1 no-underline'>Market</a>
                    </div>
                </div>
                <div className='mt-6 flex justify-between grid grid-cols-3 gap-5 mb-12 pl-6 pr-6'>
                    {markets.map((market) => (
                        <a className='no-underline' href={`/market/${market.id}`}>
                        <div key={market.id} className='self-center'>
                            <div className='flex'>
                                {market.image &&
                                    <Image width={140} height={140} src={market.image} alt=''
                                        className='w-[140px] h-[140px] rounded-xl cursor-pointer'/>}
                                <div>
                                    <p className='font-semibold text-[14px] w-[150px] mt-4 cursor-pointer ml-4'>{market.nama}</p>
                                    <p className='font-normal text-[12px] text-abu-abu ml-4 truncate w-[162px] mt-2'>{market.alamat}</p>
                                    <div className="flex mt-2 ml-4 mb-2 w-[200px]">
                                        <div className='font-normal text-merah-lihat-semua  text-[12px] mr-1'>{(Math.floor(market.distance * 100) / 100) + " km"}</div>
                                        <div className='text-[12px]'>|</div>
                                        <div className='text-merah-lihat-semua font-semibold text-[12px] ml-1'>*jumlah pedagang*</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                    ))}
                </div>
            </div>
            <Footer />
        </>
    )
}

export default page