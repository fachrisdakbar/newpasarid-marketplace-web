'use client'
import Image from 'next/image'
import React from 'react'
import LogoContactUs from '../../public/hubungi-kami.svg'
import Mail from '../../public/mail.svg'
import Phone from '../../public/phone.svg'
import Line from '../../public/Line 2.svg'
import ArrowBack from '../../public/arrowback_white.svg'
import Underline from '../../public/underline-contact-us.svg'
import { useRouter } from 'next/navigation'

const page = () => {
    const router = useRouter()
    return (
        <>
            <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                    <div className='self-center mr-[106px]'>
                        <button onClick={router.back}>
                            <Image src={ArrowBack} alt='' className='fill-current text-white' />
                        </button>
                    </div>
                    <div>
                        <h3 className='font-medium text-white text-[20px] mr-[100px]'>Hubungi Kami</h3>
                    </div>
                </div>
        <div className='contact-wrapper mr-[165px] ml-[165px] h-[100vh]'>
            <div className="wrapper-contact-us pt-32 flex flex-col items-center">
                <div className="image mb-7">
                    <Image src={LogoContactUs} width={140} height={140} alt='' className='w-[100vw] h-[35vh]' />
                </div>
                <div className='font-semibold text-2xl '>Contact Us</div>
                <div className='mb-10'>
                <Image src={Underline} width={140} height={140} alt='' className='w-[46px] h-[16px]' />
                </div>
                <div className='flex w-[30vw] justify-between'>
                    <div className='email flex'>
                        <div className='self-center'>
                        <Image src={Mail} width={140} height={140} alt='' className='w-[25px] h-[20px] mr-5' />
                        </div>
                        <div>
                            <div className='text-lg font-semibold'>Email</div>
                            <div className='text-sm font-semibold'>support@newpasar.id</div>
                        </div>
                    </div>
                    <div className="line">
                        <Image src={Line} alt='' width={140} height={140} className='w-[20px] h-[40px]'/>
                    </div>
                    <div className='phone flex'>
                        <div className='self-center'>
                        <Image src={Phone} width={140} height={140} alt='' className='w-[25px] h-[20px] mr-5' />
                        </div>
                        <div>
                            <div className='font-semibold text-lg'>Phone</div>
                            <div className='font-semibold text-sm'>+62 8129 0099 630</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </>
    )
}

export default page