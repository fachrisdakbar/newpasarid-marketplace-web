'use client'
import React, { useEffect, useState } from 'react'
import BRIVA from '../../public/Briva.svg'
import { Accordion, AccordionDetails, AccordionSummary, Typography } from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Image from 'next/image';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Accordions from '../Accordion/accordion'

const page = ({ }) => {
    const [va, setVa] = useState('')
    const [token, setToken] = useState('');

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        const typeUser = localStorage.getItem('type');

        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }

        async function fetchDataPembeli() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setVa(data.pembeli.va)

            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }
        fetchDataPembeli()
    }, [token])


    return (
        <div>
            <Header />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordions />
                </div>
                <div className='flex justify-center items-center pb-5 shadow-lg'>
                    <div className='bg-white w-[58vw]'>
                        <div className="flex pl-12 font-bold text-base pb-3 pt-3 w-[50vw] justify-center">
                            Isi Ulang Saldo
                        </div>
                        <div className='bg-abu-abu-terang ml-12 mr-10 rounded-lg mb-5'>
                            <div className='flex pb-2 text-[14px] font-medium items-center justify-between pl-2 pr-2 pt-1'>
                                <div>Akun Virtual BRI</div>
                                <div>
                                    <Image src={BRIVA} alt='' width={140} height={140}
                                        className='w-[90px] h-[50px] mr-2'
                                    />
                                </div>
                            </div>
                            <div className="flex text-[14px] justify-between pb-3 font-medium pl-2 pr-2">
                                <div>No. Akun Virtual</div>
                                <div>{va}</div>
                            </div>
                        </div>
                        <Accordion sx={{ boxShadow: 0 }}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography style={{ fontWeight: 'bold', borderBottom: 0, fontSize: '14px' }} className='pl-8 pr-10'>BRImo</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography sx={{ marginLeft: 4 }}>
                                    <div className='bg-abu-abu-terang rounded-lg text-sm text-abu-abu'>
                                        <div className="pl-4 pr-4 pt-2 pb-2">
                                            <div>1. Login pada aplikasi BRImo (masukkan Username dan Password).</div>
                                            <div>2. Pilih menu BRIVA.</div>
                                            <div>3. Pilih sumber dana kemudian masukkan kode BRIVA untuk pembayaran tagihan anda yang akan dibayarkan. (Contoh : 230740000110810).</div>
                                            <div>4. Pada halaman konfirmasi, pastikan detail pembayaran sudah sesuai (nomor BRIVA dan jumlah pembayaran).</div>
                                            <div>5. Ikuti instruksi untuk menyelesaikan transaksi.</div>
                                            <div>6. Simpan bukti transaksi sebagai bukti pembayaran.</div>
                                        </div>
                                    </div>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion sx={{ boxShadow: 0 }}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography style={{ fontWeight: 'bold', borderBottom: 0, fontSize: '14px' }} className='text-sm pl-8 pr-10'>Bayar BRIVA melalui ATM BRI</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography sx={{ marginLeft: 4 }}>
                                    <div className='bg-abu-abu-terang rounded-lg text-sm text-abu-abu'>
                                        <div className="pl-4 pr-4 pt-2 pb-2">
                                            <div>1. Masukkan kartu ATM dan PIN BRI Anda.</div>
                                            <div>2. Pilih menu Transaksi Lain {'>'} Pembayaran {'>'} Lainnya {'>'} BRIVA.</div>
                                            <div>3. Masukkan kode BRIVA untuk pembayaran tagihan Anda yang akan dibayarkan. (Contoh : 230740000110810).</div>
                                            <div>4. Pada halaman konfirmasi, pastikan detail pembayaran sudah sesuai (nomor BRIVA dan jumlah pembayaran).</div>
                                            <div>5. Ikuti instruksi untuk menyelesaikan transaksi.</div>
                                            <div>6. Simpan bukti transaksi sebagai bukti pembayaran.</div>
                                        </div>
                                    </div>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion sx={{ boxShadow: 0 }}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography style={{ fontWeight: 'bold', borderBottom: 0, fontSize: '14px' }} className='text-sm pl-8 pr-10'>Bayar BRIVA melalui ATM Bank Lain</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography sx={{ marginLeft: 4, marginBottom: '-10px' }}>
                                    <div className='bg-abu-abu-terang rounded-lg text-sm text-abu-abu'>
                                        <div className="pl-4 pr-4 pt-2 pb-2">
                                            <div>1. Masukkan kartu ATM dan PIN BRI Anda.</div>
                                            <div>2. Pilih menu Transaksi Lain {'>'} Transfer {'>'} Ke Rekening Bank Lain.</div>
                                            <div>3. Masukkan kode bank (002).</div>
                                            <div>4. Masukkan nominal yang akan dibayarkan (sesuai tagihan).</div>
                                            <div>5. Masukkan kode BRIVA untuk pembayaran tagihan Anda yang akan dibayarkan. (Contoh : 23074000011810).</div>
                                            <div>6. Pilih rekening yang akan didebet.</div>
                                            <div>7. Pada halaman konfirmasi, pastikan detail pembayaran sudah sesuai (nomor BRIVA dan jumlah pembayaran).</div>
                                            <div>8. Ikuti instruksi untuk menyelesaikan transaksi.</div>
                                            <div>9. Simpan struk transaksi sebagai bukti pembayaran.</div>
                                        </div>
                                    </div>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                        <Accordion sx={{ boxShadow: 0 }}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography style={{ fontWeight: 'bold', borderBottom: 0, fontSize: '14px' }} className='text-sm pl-8 pr-10'>Bayar BRIVA melalui Teller BRI</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Typography sx={{ marginLeft: 4, marginBottom: '10px' }}>
                                    <div className='bg-abu-abu-terang rounded-lg text-sm text-abu-abu'>
                                        <div className="pl-4 pr-4 pt-2 pb-2">
                                            <div>1. Datang ke Teller BRI di seluruh Unit Kerja BANK BRI terdekat dengan membawa nomor BRIVA</div>
                                            <div>   a. Mengisi form sesuai ketentuan BANK BRI.</div>
                                            <div>   b. Teller menerima form dan uang sesuai dengan tagihan yang akan dibayarkan.</div>
                                            <div>2. Teller BRI memproses pembukuan pembayaran melalui BRIVA.</div>
                                            <div>3. Teller memberikan bukti transfer yang sudah tervalidasi.</div>
                                        </div>
                                    </div>
                                </Typography>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default page