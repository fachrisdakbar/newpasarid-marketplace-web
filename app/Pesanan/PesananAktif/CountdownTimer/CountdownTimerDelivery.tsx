import Link from 'next/link';
import React, { useState, useEffect } from 'react';
import ModalTagihan from '../ModalTagihan/ModalTagihan';

const CountdownTimerDelivery = ({ noDisplay = false, total = 0, pembayaran, va, id, pengiriman_id, type, typeBayar, created_at }: any) => {
  const storageKey = `countdownTimer_${id}`; // Unique key for each instance
  const storedRemainingTime = localStorage.getItem(storageKey);
  const initialRemainingTime = storedRemainingTime ? parseInt(storedRemainingTime, 10) : calculateRemainingTime();
  const [remainingTime, setRemainingTime] = useState(initialRemainingTime);
  const [isVisible, setIsVisible] = useState(true);
  const [isDisplay, setIsDisplay] = useState('block');
  const [marginRight, setMarginRight] = useState('');
  let interval: any;
  let remain: any;

  function calculateRemainingTime(customTime?: Date) {
    const targetTime = new Date(created_at);
    const currentTime = customTime || new Date();
    const targetTimeMs = targetTime.getTime();
    const currentTimeMs = currentTime.getTime();
    const difference = targetTimeMs - currentTimeMs;

    return Math.max(difference, 0);
  }

  const checked = () => {
    let timerId = localStorage.getItem(`timer_${id}`) == 'hasStopped'

    if (localStorage.getItem(`timer_${id}`) == 'hasStopped') {
      return null;
    }
    else if (localStorage.getItem(`timer_${id}`) != 'hasStopped') {
      return setRemainingTime((prevTime) => Math.max(prevTime - 1000, 0));
    }
  }

  useEffect(() => {
    if (!noDisplay) {
      setMarginRight('730px')
    }
    else {
      setMarginRight('805px')
    }
    if (pengiriman_id) {
      if (typeBayar === 'ditagih') {
        let updatedTime = new Date();

        setTimeout(() => {
          updatedTime = new Date(updatedTime.getTime());
          setRemainingTime(calculateRemainingTime(updatedTime));
        });
      }
    }
    if (localStorage.getItem(`timer_${id}`) == 'hasStopped') {
      setIsVisible(false)
      setIsDisplay('none');
    }

    const timeDifferenceSeconds =
      (currentHours - providedHours) * 3600 +
      (currentMinutes - providedMinutes) * 60 +
      (currentSeconds - providedSeconds) - 12;

    let adjustedTimeRemainingMinutes = 59 - Math.floor(timeDifferenceSeconds / 60);
    let adjustedTimeRemainingSeconds = (60 - (timeDifferenceSeconds % 60) - 1 + 60) % 60;

    adjustedTimeRemainingMinutes = Math.max(adjustedTimeRemainingMinutes, 0);
    adjustedTimeRemainingSeconds = Math.max(adjustedTimeRemainingSeconds, 0);

    if (adjustedTimeRemainingMinutes == 0 && adjustedTimeRemainingSeconds == 0) {
      localStorage.setItem(`timer_${id}`, 'hasStopped')
      setIsVisible(false);
      setIsDisplay('none')
    }

    if (!isVisible) {
      setIsDisplay('none');
    } else {
      setIsDisplay('block');
    }

    // console.log('Minutes:', adjustedTimeRemainingMinutes, 'Seconds:', adjustedTimeRemainingSeconds);

    interval = setInterval(() => {
      if (adjustedTimeRemainingMinutes == 0 && adjustedTimeRemainingSeconds == 0) {
        clearInterval(interval);
        setIsVisible(false);
        setIsDisplay('none')
      } else {
        // setRemainingTime((prevTime) => Math.max(prevTime - 1000, 0));
        checked()

      }
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [remainingTime, isDisplay, isVisible]);


  const providedTime = created_at.split('T')[1].split('.')[0]; // Extract time from provided timestamp
  const [providedHours, providedMinutes, providedSeconds] = providedTime.split(':').map(Number);
  const currentTime = new Date();
  const currentHours = currentTime.getHours();
  const currentMinutes = currentTime.getMinutes();
  const currentSeconds = currentTime.getSeconds();

  const timeDifferenceSeconds =
    (currentHours - providedHours) * 3600 +
    (currentMinutes - providedMinutes) * 60 +
    (currentSeconds - providedSeconds) - 12;

  let adjustedTimeRemainingMinutes = 59 - Math.floor(timeDifferenceSeconds / 60);
  let adjustedTimeRemainingSeconds = (60 - (timeDifferenceSeconds % 60) - 1 + 60) % 60;

  adjustedTimeRemainingMinutes = Math.max(adjustedTimeRemainingMinutes, 0);
  adjustedTimeRemainingSeconds = Math.max(adjustedTimeRemainingSeconds, 0);

  return (
    <>
      <div style={{ display: isDisplay }}>
        <div className='flex'>
          <div className='flex items-center bg-merah-terang p-2 rounded-lg mb-3 text-sm justify-between'>
            <div className='text-abu-abu' style={{ marginRight: marginRight }}>Selesaikan pembayaran dalam</div>
            <div className='text-merah-lihat-semua font-semibold'>
              {adjustedTimeRemainingMinutes.toString().padStart(2, '0')}:{adjustedTimeRemainingSeconds.toString().padStart(2, '0')}
            </div>
          </div>
          {noDisplay == true ?
            ""
            :
            <ModalTagihan
              totals={total}
              va={va}
              pembayaran={pembayaran}
              type={type}
              created_at={created_at}
            />
          }
        </div>
      </div>

      {isDisplay == 'none' ?
        <div className='bg-abu-abu-terang flex items-center rounded-lg p-2 mb-3 w-[1070px]'>
          <div><span className="material-symbols-outlined flex items-center justify-center mr-3 mt-1">info</span></div>
          <div className='text-sm text-abu-abu'>Waktu pembayaran telah berakhir</div>
        </div>
        : ""}
    </>
  );
};

export default CountdownTimerDelivery;
