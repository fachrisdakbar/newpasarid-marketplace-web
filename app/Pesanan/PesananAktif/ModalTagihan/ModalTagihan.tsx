'use client'
import React, { useEffect, useState } from 'react'
import CaraPembayaran from '../CaraPembayaran/CaraPembayaran';
import CountdownTimer from '../CountdownTimer/CountdownTimer';
import CountdownTimerDelivery from '../CountdownTimer/CountdownTimerDelivery';
import Image from 'next/image';
import Briva from '../../../../public/Briva.svg'
import Saldo from '../../../../public/saldoAyam.svg'

const ModalTagihan = ({
    va,
    pembayaran,
    totals,
    type,
    created_at,
}: any) => {
    const [isDisplay, setIsDisplay] = useState('none');

    const formatNumber = (value: any) => {
        if (value !== undefined && value !== null) {
            return value.toLocaleString('id-ID', {
                useGrouping: true,
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
                minimumIntegerDigits: 1,
                style: 'decimal',
            });
        }
        return ''; // Return an empty string if value is undefined or null
    }
    useEffect(() => {
      console.log("type : " + type);
      
    }, [])
    

    return (
        <>
            <div>
                <div onClick={() => setIsDisplay('block')} className='text-sm cursor-pointer font-semibold mt-[8px] ml-1 items-center'>Lihat Tagihan</div>
            </div>

            <div className='bg-opacity-50 bg-black fixed inset-0 z-30' style={{ display: isDisplay }}>
                <div className='h-[100vh] flex justify-center items-center'>
                    <div className='bg-white w-[74vw] mb-2 rounded-lg pb-4'>
                        <svg onClick={(e) => setIsDisplay('none')} className='self-center cursor-pointer ml-4 mt-4 mb-2' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                        </svg>
                        <div className='pl-10 pr-10 pt-2'>
                            {/* {type == 'PickUp' ?
                                <CountdownTimer
                                    noDisplay={true}
                                    noModalTagihan={true}
                                    // id={orders?.['id']}
                                    created_at={created_at}
                                    /> :
                                    <CountdownTimerDelivery
                                    noDisplay={true}
                                    noModalTagihan={true}
                                    // id={orders?.['id']}
                                    created_at={created_at}
                                />} */}
                            {/* <div className='text-abu-abu text-xs mt-[-15px]'>Pesanan akan otomatis dibatalkan apabila 1 jam belum dibayar</div> */}

                            <div className='text-sm mt-[5px]'>Metode Pembayaran</div>
                            <div className='flex text-xs'>
                                <div className='font-bold mr-4 flex justify-center items-center'>{pembayaran}</div>
                                <div className='flex justify-center items-center'>{pembayaran == 'BRIVA' ? <Image src={Briva} alt='' width={100} height={100} /> : <Image src={Saldo} alt='' width={100} height={100} />}</div>
                            </div>

                            <div className="flex">
                            <div className='text-sm mt-1 mr-2 mb-2 flex items-center'>Virtual Account</div>
                            <div className='font-semibold text-[13px] flex items-center'>{va}</div>
                            </div>

                            <div className="flex mt-1">
                                <div className='text-sm mt-1 mr-2 mb-2'>Total Tagihan</div>
                                <div className='font-semibold text-[13px] flex items-center'>{"Rp " + formatNumber(totals)}</div>
                            </div>
                        </div>
                        <div className='flex justify-center'>
                            <div className='font-semibold text-sm'>Cara Pembayaran</div>
                        </div>
                        <div className='flex pb-2'>
                            <div className='pl-10 pr-10 pt-2 pb-2'>
                                <div className='text-xs font-semibold'>BRImo</div>
                                <div className='bg-abu-abu-terang rounded-lg text-xs text-abu-abu'>
                                    <div className="pl-4 pr-4 pt-2 pb-2">
                                        <div>1. Login pada aplikasi BRImo (masukkan Username dan Password).</div>
                                        <div>2. Pilih menu BRIVA.</div>
                                        <div>3. Pilih sumber dana kemudian masukkan kode BRIVA untuk pembayaran tagihan anda yang akan dibayarkan. (Contoh : 230740000110810).</div>
                                        <div>4. Pada halaman konfirmasi, pastikan detail pembayaran sudah sesuai (nomor BRIVA dan jumlah pembayaran).</div>
                                        <div>5. Ikuti instruksi untuk menyelesaikan transaksi.</div>
                                        <div>6. Simpan bukti transaksi sebagai bukti pembayaran.</div>
                                    </div>
                                </div>
                            </div>
                            <div className='pl-10 pr-10 pt-2 pb-2'>
                                <div className='text-xs font-semibold'>Bayar BRIVA melalui ATM BRI</div>
                                <div className='bg-abu-abu-terang rounded-lg text-xs text-abu-abu'>
                                    <div className="pl-4 pr-4 pt-2 pb-2">
                                        <div>1. Masukkan kartu ATM dan PIN BRI Anda.</div>
                                        <div>2. Pilih menu Transaksi Lain {'>'} Pembayaran {'>'} Lainnya {'>'} BRIVA.</div>
                                        <div>3. Masukkan kode BRIVA untuk pembayaran tagihan Anda yang akan dibayarkan. (Contoh : 230740000110810).</div>
                                        <div>4. Pada halaman konfirmasi, pastikan detail pembayaran sudah sesuai (nomor BRIVA dan jumlah pembayaran).</div>
                                        <div>5. Ikuti instruksi untuk menyelesaikan transaksi.</div>
                                        <div>6. Simpan bukti transaksi sebagai bukti pembayaran.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='flex'>
                            <div className='pl-10 pr-10 pt-2 pb-2'>
                                <div className='text-xs font-semibold'>Bayar BRIVA melalui ATM Bank Lain</div>
                                <div className='bg-abu-abu-terang rounded-lg text-xs text-abu-abu'>
                                    <div className="pl-4 pr-4 pt-2 pb-2">
                                        <div>1. Masukkan kartu ATM dan PIN BRI Anda.</div>
                                        <div>2. Pilih menu Transaksi Lain {'>'} Transfer {'>'} Ke Rekening Bank Lain.</div>
                                        <div>3. Masukkan kode bank (002).</div>
                                        <div>4. Masukkan nominal yang akan dibayarkan (sesuai tagihan).</div>
                                        <div>5. Masukkan kode BRIVA untuk pembayaran tagihan Anda yang akan dibayarkan. (Contoh : 23074000011810).</div>
                                        <div>6. Pilih rekening yang akan didebet.</div>
                                        <div>7. Pada halaman konfirmasi, pastikan detail pembayaran sudah sesuai (nomor BRIVA dan jumlah pembayaran).</div>
                                        <div>8. Ikuti instruksi untuk menyelesaikan transaksi.</div>
                                        <div>9. Simpan struk transaksi sebagai bukti pembayaran.</div>
                                    </div>
                                </div>
                            </div>
                            <div className='ml-20 pr-10 pt-2 pb-4'>
                                <div className='text-xs font-semibold'>Bayar BRIVA melalui Teller BRI</div>
                                <div className='bg-abu-abu-terang rounded-lg text-abu-abu'>
                                    <div className="pl-4 pr-4 pt-2 pb-2 text-xs">
                                        <div>1. Datang ke Teller BRI di seluruh Unit Kerja BANK BRI terdekat dengan membawa nomor BRIVA</div>
                                        <div>   a. Mengisi form sesuai ketentuan BANK BRI.</div>
                                        <div>   b. Teller menerima form dan uang sesuai dengan tagihan yang akan dibayarkan.</div>
                                        <div>2. Teller BRI memproses pembukuan pembayaran melalui BRIVA.</div>
                                        <div>3. Teller memberikan bukti transfer yang sudah tervalidasi.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}

export default ModalTagihan