'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '../../../Accordion/accordion'
import Link from 'next/link'
import Image from 'next/image'
import Delivery from '../../../../public/delivery.svg'
import Pickup from '../../../../public/pickup.svg'
import Market from '../../../../public/toko.svg'
import Location from '../../../../public/location.svg'
import Saldo from '../../../../public/saldoAyam.svg'
import Briva from '../../../../public/Briva.svg'
import { useParams } from 'next/navigation'

const page = () => {
    const [token, setToken] = useState('');
    const [idPembeli, setIdPembeli] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState();
    const [display, setDisplay] = useState('none');
    const [totals, setTotal] = useState<number>();
    const { id } = useParams()

    async function fetchData() {
        try {
            const response = await fetch(`/api/getUserPembeli`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res.data
            console.log(data);
            if (data) {
                setLoading(false)
            }
            else if (data == undefined) {
                setLoading(true)
            }
            setIdPembeli(data.pembeli.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.penjual&with[]=orderItems.pasar&with[]=pengiriman&with[]=orderItems.produk.images&with[]=pengiriman.jenisPengiriman&with[]=pembeli&with[]=pembayaran.jenisPembayaran&where[]=status,dibatalkan&where[]=id,${id}&where[]=pembeli_id,${idPembeli}&orderby=updated_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setOrders(data[0]);
                console.log(data);

                let total = 0;
                data[0]?.order_items.forEach((item: any) => {
                    const subtotal = item.jumlah * item.produk.harga;
                    console.log(subtotal);
                    total += subtotal;
                });
                console.log(total);


                // Add pengiriman.biaya to the total
                total += data[0]?.pengiriman?.biaya || 0;

                console.log('Total:', total);
                setTotal(total);

                for (let i = 0; i < data.length; i++) {
                    console.log(data[i].order_items.length);
                }

                if (!data[0].store_order_id) {
                    setIsOrder(false)
                }
                else if (data[0].store_order_id) {
                    setIsOrder(true)
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPembeli]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
    }, [token, idPembeli])

    const formatNumber = (value: any) => {
        if (value !== undefined && value !== null) {
            return value.toLocaleString('id-ID', {
                useGrouping: true,
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
                minimumIntegerDigits: 1,
                style: 'decimal',
            });
        }
        return ''; // Return an empty string if value is undefined or null
    }


    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)


        return formattedDate;
    }
    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananBatal={true}
                    />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    <div className='flex self-center text-center items-center'>
                        <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                            <a href={'/Pesanan/PesananBatal'} className='text-[14px] mr-1 no-underline'>Pesanan dibatalkan / </a>
                            <div className='text-[14px] mr-1 no-underline'>Detail</div>
                        </div>
                    </div>
                    <div className='pt-3 pb-3 pl-6 pr-6'>
                        {loading == true ?
                            <div className='flex flex-col justify-center items-center'>
                                <div className='loadingContainer '>
                                    <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                    <div className="loadingAnimation"></div>
                                </div>
                            </div>
                            :
                            <>
                                <div>
                                    <div className='flex justify-between p-3'>
                                        <div className='flex'>
                                            {
                                                orders?.['pengiriman_id'] ?
                                                    <div className='bg-hijau w-[80px] p-2 rounded-[100px] flex justify-center items-center'>
                                                        <Image src={Delivery} alt='' width={100} height={100}
                                                            className='w-[50px] h-[50px]'
                                                        />
                                                    </div>
                                                    :
                                                    <div className='bg-blue-200 w-[80px] p-2 rounded-[100px] flex justify-center items-center'>
                                                        <Image src={Pickup} alt='' width={100} height={100}
                                                            className='w-[50px] h-[50px]'
                                                        />
                                                    </div>
                                            }
                                            <div className='ml-4 flex flex-col justify-center'>
                                                <div className='text-sm text-abu-terang'>Status Pesanan</div>
                                                <div className='font-[550] text-sm'>{orders?.['status']}</div>
                                            </div>
                                        </div>
                                        <div className='flex flex-col justify-center items-end'>
                                            <div className='text-sm text-abu-abu'>Tanggal Pesanan</div>
                                            <div>
                                                <div className='text-sm font-semibold'>{(() => {
                                                    const formattedDate = formatDate(orders?.['created_at']);
                                                    const justDate = formattedDate.split(' ')
                                                    return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                                })()}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex justify-between items-center ml-1 pl-3 pr-3">
                                        <div className=''>
                                            <div className='text-sm text-abu-abu'>Metode Pembayaran</div>
                                            {orders?.['pembayaran'] == null ?
                                                ''
                                                :
                                                orders?.['pembayaran']['jenis_pembayaran']['title'] == 'Briva' ?
                                                    <div className='flex items-center'>
                                                        <Image src={Briva} alt='' width={100} height={100} />
                                                        <div className='text-base font-semibold ml-4'>BRIVA</div>
                                                    </div>
                                                    :
                                                    <div className='flex items-center'>
                                                        <Image src={Saldo} alt='' width={100} height={100}
                                                            className='w-[50px] h-[50px]'
                                                        />
                                                        <div className='text-base font-semibold ml-4'>Saldo</div>
                                                    </div>
                                            }
                                        </div>
                                        <div className='flex flex-col items-end'>
                                            <div className='text-sm text-abu-abu'>Nomor Order</div>
                                            <div className='font-[550] text-sm'>{orders?.['store_order_id']}</div>
                                            {
                                                orders?.['pengiriman_id'] ?
                                                    ''
                                                    :
                                                    <div className='flex flex-col items-end mt-1'>
                                                        <div className='text-sm mb-[1px] text-abu-abu'>Kode Pengambilan</div>
                                                        {orders?.['order_items']['pickup_code'] ?
                                                            <div className='text-xs'>{orders?.['order_items']['pickup_code']}</div>
                                                            :
                                                            <div className='text-sm'>Kode pengambilan akan ditampilkan setelah pesanan diproses</div>
                                                        }
                                                    </div>
                                            }
                                        </div>
                                    </div>
                                    <div className='shadow-md p-3 mt-3'>
                                        <div className='flex text-xs text-abu-abu mb-2'>
                                            <div>Detail Pesanan</div>
                                            <div className='ml-2'>{"(" + orders?.['order_items']['length'] + " Produk)"}</div>
                                        </div>
                                        {/* abaikan merahnya */}
                                        {orders?.['order_items']?.map((item: any, index: any) => (
                                            <div key={index}>
                                                <div className='flex flex-row items-center justify-between mb-3'>
                                                    <div className='flex'>
                                                        <Image src={Market} alt='' />
                                                        <div className='text-md font-bold ml-3'>{item.produk.penjual.nama_toko}</div>
                                                    </div>
                                                    <div className='flex'>
                                                        <Image src={Location} alt='' />
                                                        <div className='text-md font-bold ml-3'>{item.pasar.nama}</div>
                                                    </div>
                                                </div>
                                                <div className='flex'>
                                                    <div>
                                                        <Image src={item.produk ? item?.produk?.images[0]?.image : ""} alt='' width={100} height={100}
                                                            className='w-[150px] h-[150px] rounded-[40px] mb-3'
                                                        />
                                                    </div>
                                                    <div className='ml-10'>
                                                        <div className='mt-3 mb-2'>{item.produk.nama}</div>
                                                        <div className='mb-2 text-abu-terang text-sm'>{"Rp " + formatNumber(item.produk.harga)}</div>
                                                        <div className='flex text-sm mb-2'>
                                                            Jumlah
                                                            <div className='text-warna-text-harga-produk ml-1'>{item.jumlah}</div>
                                                        </div>
                                                        <div className='flex'>
                                                            <div className='text-sm text-abu-abu'>Total</div>
                                                            <div className='text-merah-lihat-semua font-semibold text-sm ml-2'>{"Rp " + formatNumber(item.produk.harga * item.jumlah)}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                        <div className='flex justify-between'>
                                            <div className='flex'>
                                                <div className='text-sm text-abu-abu'>Catatan :</div>
                                                <div className='text-sm text-abu-abu ml-1'>
                                                    {orders?.['note'] ? orders?.['note'] : '-'}
                                                </div>
                                            </div>
                                            {orders?.['pengiriman_id'] ?
                                                <div className=' flex text-sm'>
                                                    <div className='text-abu-abu'>Nama Penerima : </div>
                                                    <div className='text-abu-abu ml-1'>{orders?.['pengiriman']['alamat_nama_penerima']}</div>
                                                </div>
                                                : ""
                                            }
                                        </div>
                                    </div>
                                    {
                                        orders?.['pengiriman_id'] ?
                                            <>
                                                <div className='pl-3 pr-3 mt-3 flex justify-between'>
                                                    <div className='text-sm'>
                                                        <div className='text-abu-abu'>Alamat Pengiriman</div>
                                                        <div className='font-semibold'>{orders?.['pengiriman']['alamat_nama']}</div>
                                                    </div>
                                                    <div className='text-sm'>
                                                        <div className='text-abu-abu'>Nomor Ponsel</div>
                                                        <div className='font-semibold'>{orders?.['pembeli']['phone']}</div>
                                                    </div>
                                                </div>
                                                <div className='flex justify-between mt-3 pl-3 pr-3'>
                                                    <div className='text-sm'>
                                                        <div className='text-abu-abu'>Kurir Pengiriman</div>
                                                        <div className='font-semibold'>{orders?.['pengiriman']['jenis_pengiriman']['title']}</div>
                                                    </div>
                                                    <div className='text-sm flex flex-col items-end'>
                                                        <div className='text-abu-abu'>Waktu Pengiriman</div>
                                                        <div className='font-semibold'>
                                                            <div className='text-sm font-semibold'>{(() => {
                                                                const formattedDate = formatDate(orders?.['pengiriman']['waktu_pengiriman']);
                                                                const justDate = formattedDate.split(' ')
                                                                return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                                            })()}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='pl-3 pr-3 flex justify-between mt-3'>
                                                    <div>
                                                        <div className='text-abu-abu text-sm mb-1'>Rincian Tagihan</div>
                                                        <div className='flex'>
                                                            <div className='font-semibold text-sm  w-[100px]'>Ongkir</div>
                                                            {orders?.['pengiriman']['promo_value'] == null ?
                                                                <div className='text-sm font-semibold'>{"Rp " + formatNumber(orders?.['pengiriman']['biaya'])}</div>
                                                                :

                                                                <div className='flex'>
                                                                    <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                                                        <div className='text-sm text-abu-terang mr-1' style={{ textDecoration: 'line-through' }}>
                                                                            {"Rp " + formatNumber(orders?.['pengiriman']['biaya'])}
                                                                        </div>
                                                                    </div>
                                                                    <div className='text-sm font-semibold'>{"Rp " + formatNumber(orders?.['pengiriman']['biaya'] - orders?.['pengiriman']['promo_value'])}</div>
                                                                </div>
                                                            }
                                                        </div>
                                                        {orders?.['promo_value'] ?
                                                            <div className='flex mt-2'>
                                                                <div className='text-sm font-semibold w-[100px]'>Diskon Order</div>
                                                                <div className='text-sm font-semibold'>{'Rp ' + formatNumber(orders?.['promo_value'])}</div>
                                                            </div>
                                                            :
                                                            ''
                                                        }
                                                        {orders?.['cashback_promo_value'] ?
                                                            <div className='flex mt-2'>
                                                                <div className='text-sm font-semibold w-[100px]'>Cashback</div>
                                                                <div className='text-sm font-semibold'>{'Rp ' + formatNumber(orders?.['cashback_promo_value'])}</div>
                                                            </div>
                                                            :
                                                            ''
                                                        }
                                                    </div>
                                                </div>
                                                <div className='flex pl-3 pr-3 mt-3'>
                                                    <div className='text-sm w-[100px]'>Total</div>
                                                    <div className='text-merah-lihat-semua text-sm font-semibold'>{"Rp " + formatNumber(totals)}</div>
                                                </div>
                                            </>
                                            :
                                            <div>
                                                {/* abaikan merahnya */}
                                                {/* {orders?.['order_items']?.map((item: any, index: any) => ( */}
                                                <>
                                                    <div className='flex justify-between mt-3'>
                                                        <div className='text-sm'>
                                                            <div className='text-abu-abu'>Toko</div>
                                                            <div className='font-semibold'>{orders?.['order_items']['0']['produk']['penjual']['nama_toko']}</div>
                                                            <div className='text-abu-abu'>{orders?.['order_items']['0']['produk']['penjual']['kavling']}</div>
                                                        </div>
                                                        <div className='flex flex-col text-sm items-end'>
                                                            <div className='text-abu-abu'>Nama Pemilik Toko</div>
                                                            <div className='font-semibold'>{orders?.['order_items']['0']['produk']['penjual']['nama']}</div>
                                                        </div>
                                                    </div>
                                                    <div className='flex justify-between mt-3'>
                                                        <div>
                                                            <div className='text-sm'>Jam Operasional Toko</div>
                                                            <div className='flex font-semibold text-sm'>
                                                                <div>{orders?.['order_items']['0']['produk']['penjual']['jam_buka']}</div>
                                                                <div className='ml-1 mr-1'>-</div>
                                                                <div>{orders?.['order_items']['0']['produk']['penjual']['jam_tutup']}</div>
                                                            </div>
                                                        </div>
                                                        <div className='flex flex-col items-end'>
                                                            <div className='text-sm'>Total</div>
                                                            <div className='text-merah-lihat-semua text-sm font-semibold'>{"Rp " + formatNumber(totals)}</div>
                                                        </div>
                                                    </div>
                                                </>
                                                {/* ))} */}
                                            </div>
                                    }
                                </div>
                            </>
                        }
                    </div>
                </div>
            </div >
            <Footer />
        </>
    )
}

export default page