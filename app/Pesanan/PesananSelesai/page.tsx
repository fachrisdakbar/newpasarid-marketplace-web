'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useEffect, useState } from 'react'
import Accordion from '../../Accordion/accordion'
import Clipboard from '../../../public/Clipboard.svg'
import Image from 'next/image'
import Market from '../../../public/toko.svg'
import Location from '../../../public/location.svg'
import Delivery from '../../../public/delivery.svg'
import Pickup from '../../../public/pickup.svg'
import Link from 'next/link'
import { useRouter } from 'next/navigation'

interface Order {
    id: number,
    store_order_id: string,
    created_at: string,
    pengiriman_id: number,
    promo_value : number,
    pengiriman: {
        waktu_pengiriman: string,
        promo_value : number,
        biaya: number,
        jenis_pengiriman: {
            title: string
        }
    },
    order_items: [
        {
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {
    const router = useRouter()
    const [token, setToken] = useState('');
    const [idPembeli, setIdPembeli] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState<Order[]>([]);


    async function fetchData() {
        try {
            const response = await fetch(`/api/getUserPembeli`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res.data
            console.log(data);
            setIdPembeli(data.pembeli.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.penjual&with[]=orderItems.pasar&with[]=pengiriman&with[]=orderItems.produk.images&with[]=pengiriman.jenisPengiriman&with[]=pembayaran.jenisPembayaran&where[]=status,selesai&where[]=pembeli_id,${idPembeli}&orderby=updated_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Order[] = await response.json();
                setOrders(data);

                console.log(data);

                for (let i = 0; i < data.length; i++) {
                    console.log(data[i].order_items.length);
                }


                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setLoading(false)
                    setIsOrder(true)
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPembeli]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPembeli])


    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)


        return formattedDate;
    }

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananSelesai={true}
                    />
                </div>
                <div className='w-[100vw] shadow-lg font-roboto'>
                    <div className='flex flex-col '>
                        {isOrder == false &&
                            <div className='flex justify-center items-center flex-col font-roboto pt-[75px] pb-[75px]'>
                                <div className='text-lg font-semibold'>Pesanan Selesai</div>
                                <div>Pesanan selesai akan tampil disini</div>
                                <div>
                                    <Image alt='' src={Clipboard} width={100} height={100}
                                        className='w-[300px] h-[300px]'
                                    />
                                </div>
                            </div>
                        }
                    </div>
                    <div className=''>
                        {loading == true ?
                            <div className='flex flex-col justify-center items-center'>
                                <div className='loadingContainer '>
                                    <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                    <div className="loadingAnimation"></div>
                                </div>
                            </div>
                            :
                            <>
                                {isOrder == true &&
                                    <div className='w-full grid grid-rows-none gap-3 pl-6 pr-6 pt-6 pb-6 font-roboto'>
                                        {orders.map((order) => {
                                            let subtotal = 0
                                            order.order_items.forEach(item => {
                                                const itemSubtotal = item.produk.harga * item.jumlah;
                                                subtotal += itemSubtotal;
                                            });

                                            const totals = subtotal + (order.pengiriman?.biaya || 0);
                                            const total = totals - (order?.pengiriman?.promo_value ? order?.pengiriman?.promo_value : 0) - (order?.promo_value ? order?.promo_value : 0)
                                            return (
                                                <a href={`/Pesanan/PesananSelesai/${order.id}`}>
                                                    <div key={order.id} className='mb-2 shadow-md p-3'>
                                                        <div className='flex justify-between items-center'>
                                                            <div className='text-sm text-abu-abu'>No.Order</div>
                                                            <div className='text-sm text-abu-abu'>{(() => {
                                                                const formattedDate = formatDate(order.created_at);
                                                                const justDate = formattedDate.split(' ')
                                                                return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                                            })()}
                                                            </div>
                                                        </div>
                                                        <div className="flex justify-between">
                                                            <div className='font-bold text-sm'>{order.store_order_id}</div>
                                                        </div>
                                                        <div className='flex flex-col mt-3'>
                                                            {order.order_items.map((item: any, index: any) => (
                                                                <div key={index} className='mb-3'>
                                                                    <div className='flex flex-row items-center justify-between mb-3'>
                                                                        <div className='flex'>
                                                                            <Image src={Market} alt='' />
                                                                            <div className='text-md font-bold ml-3'>{item.produk.penjual? item.produk.penjual.nama_toko : ''}</div>
                                                                        </div>
                                                                        <div className='flex'>
                                                                            <Image src={Location} alt='' />
                                                                            <div className='text-md font-bold ml-3'>{item.pasar.nama}</div>
                                                                        </div>
                                                                    </div>
                                                                    <div className='flex'>
                                                                        <div>
                                                                            <a href={`/Product/${item.produk_id}`}>
                                                                                <Image src={item.produk?.images[0]?.image} alt='' width={100} height={100}
                                                                                    className='w-[150px] h-[150px] rounded-[40px]'
                                                                                />
                                                                            </a>
                                                                        </div>
                                                                        <div className='ml-10'>
                                                                            <div className='mt-3 mb-4'>{item.produk.nama}</div>
                                                                            <div className='mb-4 text-abu-terang text-sm'>{"Rp " + formatNumber(item.produk.harga)}</div>
                                                                            <div className='flex text-sm'>
                                                                                Jumlah
                                                                                <div className='text-warna-text-harga-produk ml-1'>{item.jumlah}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            ))}
                                                        </div>
                                                        {order.pengiriman ?
                                                            <div>
                                                                <div className='text-sm font-[550]'>Rincian</div>
                                                                <div className='flex mt-2'>
                                                                    <div className='text-sm text-abu-abu'>Kurir - {order.pengiriman.jenis_pengiriman.title}</div>
                                                                </div>
                                                                <div className="flex mt-2">
                                                                    <div className='text-sm text-abu-abu mr-2'>Ongkir</div>
                                                                    <div className='text-sm text-abu-abu'>{"Rp " + formatNumber(order.pengiriman.biaya)}</div>
                                                                </div>
                                                            </div>
                                                            :
                                                            ''
                                                        }
                                                        <div className='flex justify-end'>
                                                            <div className='text-sm text-abu-abu mr-1'>Total :</div>
                                                            <div className='text-merah-lihat-semua text-sm font-[550]'> {"Rp " + formatNumber(total)}</div>
                                                        </div>
                                                        {order.pengiriman_id ?
                                                            <div className='flex justify-between mt-2'>
                                                                <div className='flex mt-2 items-center'>
                                                                    <div>
                                                                        <Image src={Delivery} alt='' width={100} height={100}
                                                                            className='w-[25px] h-[25px] mr-4'
                                                                        />
                                                                    </div>
                                                                    <div className='text-sm font-[550] text-border-hijau'>Delivery</div>
                                                                </div>
                                                                <div>
                                                                    <a className='text-white text-sm rounded-xl px-4 p-2 bg-[#48872A] font-[550]' href={`/Pesanan/PesananSelesai/rating/${order.id}`}>Nilai</a>
                                                                    <button className='text-[#6a8f57] text-sm rounded-xl p-2 bg-[#ddf3d3] font-[550] ml-2'>Selesai</button>

                                                                </div>
                                                            </div>
                                                            :
                                                            <div className="flex mt-2 items-center">
                                                                <div>
                                                                    <Image src={Pickup} alt='' width={100} height={100}
                                                                        className='w-[25px] h-[25px] mr-4'
                                                                    />
                                                                </div>
                                                                <div className='text-sm font-[550] text-biru-muda'>Pickup</div>
                                                            </div>
                                                        }
                                                    </div>
                                                </a>
                                            )
                                        })}
                                    </div>
                                }
                            </>
                        }
                    </div>
                </div>
            </div >
            <Footer />
        </>
    )
}

export default page