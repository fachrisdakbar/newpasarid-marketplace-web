'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import React, { useState } from 'react'

const page = ({
    nama = '',
    namaPenerima = '',
    long = '',
    lat = '',
    alamat = ''
}) => {
    const [isVisible, setIsVisible] = useState('none')
    const [name, setName] = useState(nama)
    const [nameReceiver, setNameReceiver] = useState(namaPenerima)
    const [address, setAddress] = useState(alamat)

    const inputName=(event: { target: { value: any; }; })=>{
        setName(nama)
        setName(event.target.value)
        console.log(event.target.value);
    }
    const inputNameReceiver=(event: { target: { value: any; }; })=>{
        setNameReceiver(namaPenerima)
        setNameReceiver(event.target.value)
        console.log(event.target.value);
    }
    const inputAddress=(event: { target: { value: any; }; })=>{
        setAddress(alamat)
        setAddress(event.target.value)
        console.log(event.target.value);
    }

    const saveEdit = async() =>{
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/api/elorest/Models/Alamat/379`)
        } catch (error) {
            
        }
    }

    return (
        <>
            <div onClick={()=>setIsVisible('block')}>
                <button className="text-[#48872A] font-medium ">
                    Ubah
                </button>
            </div>
            <section className='w-[100vw] h-[100vh] bg-opacity-50 bg-black fixed inset-0 z-30 flex justify-center items-center self-center' style={{ display: isVisible }}>
                <div className='flex justify-center items-center self-center h-[100vh]'>
                    <div className='w-[584px] h-[560px] bg-white rounded-2xl'>
                        <svg onClick={() => setIsVisible('none')} className='self-center cursor-pointer mt-3 ml-3 absolute' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                        </svg>
                        <div className='font-semibold font-roboto text-xl ml-10 mt-10 mb-6 w-[200px]'>Ubah Alamat</div>
                        <div className='w-[504px] h-[532px] ml-10 mr-10 mb-10'>
                            <div className='flex mb-6'>
                                <div className='mr-6'>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Nama</label>
                                    <div className="w-[240px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[240px] h-[30px]"
                                            // placeholder='Nama Penerima'
                                            value={name}
                                            onChange={inputName}
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Nama Penerima</label>
                                    <div className="w-[240px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[240px] h-[30px]"
                                            // placeholder='Contoh : 08123456789'
                                            value={namaPenerima}
                                            onChange={inputNameReceiver}
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div>
                            </div>
                            {/* <div className='flex mb-6'>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Lokasi</label>
                                    <div className="w-[504px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[504px] h-[30px]"
                                            placeholder='Provinsi, Kota, Kecamatan, Kelurahan'
                                        >
                                        </input>
                                    </div>
                                </div>
                            </div> */}
                            <div className='flex mb-6'>
                                <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Alamat</label>
                                    <div className="w-[504px] h-[80px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[504px] h-[30px]"
                                            placeholder='Nama Jalan, Gedung, No Rumah'
                                            type='text'
                                            value={alamat}
                                            onChange={inputAddress}
                                        >
                                        </input>
                                    </div>
                                </div>
                            </div>
                            {/* <div className='flex mb-6'> */}
                                {/* <div className='mr-6'>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Label Alamat</label>
                                    <div className="w-[240px] h-[44px] py-[7px] px-[10px] flex border rounded-lg">
                                        <input
                                            className=" text-[16px] font-sans hover:border-indigo-30000 focus:outline-none focus:ring-0 no-spinners w-[240px] h-[30px]"
                                            placeholder='Label Alamat'
                                            type='text'
                                        >
                                        </input>
                                    </div>
                                </div> */}
                                {/* <div>
                                    <label className='font-medium text-xs font-roboto text-abu-terang'>Tandai Sebagai</label>
                                    <div className="w-[240px] h-[44px] flex">
                                        <div className='w-[120px] h-[44px] flex justify-center items-center cursor-pointer'
                                            onClick={() => clickHouse()}
                                            style={{ backgroundColor: isGreeenHouse, borderTopLeftRadius: '40px', borderBottomLeftRadius: '40px' }}>
                                            <Image src={Rumah} alt='' width={100} height={100} className='w-5 h-5' />
                                            <div className='font-normal text-white font-roboto text-sm ml-2'>Rumah</div>
                                        </div>
                                        <div className='w-[120px] h-[44px] flex justify-center items-center cursor-pointer'
                                            onClick={() => clickToko()}
                                            style={{ backgroundColor: isGreeenToko, borderTopRightRadius: '40px', borderBottomRightRadius: '40px' }}>
                                            <Image src={Toko} alt='' width={100} height={100} className='w-5 h-5' />
                                            <div className='font-normal text-white font-roboto text-sm ml-2'>Toko</div>
                                        </div>
                                    </div>
                                </div> */}
                            {/* </div> */}
                            <div className='w-[504px] h-[128px] mb-10 bg-blue-300 flex justify-center items-center'>
                                map
                            </div>
                            <div className='w-[504px] h-[44px] bg-border-hijau text-sm font-roboto font-medium text-white flex justify-center items-center rounded-lg'
                            onClick={()=>saveEdit()}
                            >Simpan Alamat</div>
                        </div>
                    </div>
                </div>
            </section>

        </>
    )
}

export default page