"use client"
import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import Header from '../components/Header';
import Footer from '../components/Footer';
// import Accordion from './components/accordion';
import Accordion from '../Accordion/accordion'
import AlamatBaru from './alamat-baru/page'
import EditAlamat from './edit-alamat/page'
import { setMyStateNama, setMyStateNamaPenerima, setMyStateAlamat } from '../global';

interface Alamat {
    id: number;
    nama: string;
    nama_penerima: string;
    alamat: string;
    longitude: string;
    latitude: string;
}

const Page = () => {
    const [searchQuery, setSearchQuery] = useState('');
    const [value, setValue] = useState();
    const [addresses, setAddresses] = useState<Alamat[]>([]);
    const [filteredAddresses, setFilteredAddresses] = useState<Alamat[]>([]);
    const [selectedAddressIndex, setSelectedAddressIndex] = useState<number | null>(null);
    const [id, setId] = useState();
    const [alamatId, setAlamatId] = useState();
    const [token, setToken] = useState('');
    // const [idPembeli, setIdPembeli] = useState('');


    // const onSearch = (e: any) => {
    //     // const searchTerm = e.target.value
    //     setSearchQuery(e)

    //     console.log('Search query:', searchQuery);{{  }}
    //     const querylc = searchQuery.toLowerCase();
    //     console.log('Filtering address:', querylc);
    //     if (searchQuery !== '') {
    //         const searchResults = addresses.filter((address) => {
    //             address.nama?.toLowerCase().includes(querylc) ||
    //                 address.nama_penerima?.toLowerCase().includes(querylc) ||
    //                 address.alamat?.toLowerCase().includes(querylc)
    //         });
    //         setFilteredAddresses(searchResults);
    //         console.log('Search results: ', searchResults);
    //     } else {
    //         setFilteredAddresses(addresses)
    //     }
    //     // if (query === '') {
    //     //     setFilteredAddresses(addresses);
    //     // } else {
    //     //     setFilteredAddresses(searchResults);
    //     // }
    // };

    const handleClick = (index: number) => {
        // Toggle the selected address
        if (index === selectedAddressIndex) {
            setSelectedAddressIndex(null);
        } else {
            setSelectedAddressIndex(index);
        }
    };

    const deleteAlamat = async (alamatid: number) => {
        try {
            const tokenData = localStorage.getItem('token');
            if (tokenData) {
                console.log(tokenData);

                const parsedData = JSON.parse(tokenData);
                console.log(parsedData.token);

                const extractedValue = `${parsedData.token}`;
                setToken(extractedValue);
                console.log("token adalah : ", extractedValue)
                console.log(id);

            } else {
                console.error('Token data not found in local storage');
            }
            const response = await fetch(`/api/delete-alamat`, {
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ` + token,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    token: token,
                    id: alamatid,
                })
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            } else {
                const data = await response.json();
                console.log("data ", data);
            }
        } catch (error) {
            console.error('Error deleting address:', error);
        }
    }



    useEffect(() => {
        async function fetchData() {
            try {
                const tokenData = localStorage.getItem('token');
                if (tokenData) {
                    console.log(tokenData);

                    const parsedData = JSON.parse(tokenData);
                    console.log(parsedData.token);

                    const extractedValue = `${parsedData.token}`;
                    setToken(extractedValue);
                    console.log("token adalah : ", extractedValue)
                } else {
                    console.error('Token data not found in local storage');
                }
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=pembeli`, {
                    headers: {
                        Authorization: `Bearer ` + token,
                    },
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                console.log(data);
                setId(data.pembeli.id);
                setAlamatId(data.pembeli.alamat_id_utama)
                // console.log("pembeli id", id)
                setValue(data)
                // console.log("value", value)
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        fetchData();
    }, [token, id, alamatId]);

    // useEffect(() => {
    //     async function fetchAlamatData() {
    //         try {
    //             // if (id) {
    //                 // https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Alamat?where=pembeli_id,${id}&get=*
    //             const response = await fetch(`/api/Alamat`, {
    //                 headers: {
    //                     Authorization: `Bearer ` + token,
    //                 },
    //                 body : JSON.stringify({
    //                     id : id,
    //                 })
    //             });
    //             if (!response.ok) {
    //                 throw new Error('Network response was not ok');
    //             }
    //             const data: Alamat[] = await response.json();
    //             console.log("data", data);

    //             // console.log(responseData);

    //             if (response) {
    //                 // const data: Alamat[] = responseData;
    //                 console.log("data", data);
    //                 setAddresses(data)
    //                 console.log("addresses", addresses);

    //                 setFilteredAddresses(data);
    //             }
    //             // console.log('Data:', data);
    //             // } else {
    //             //     console.error('No data received from the API.');
    //             // }
    //             // }
    //         } catch (error) {
    //             console.error('Error fetching data:', error);
    //         }
    //     }
    //     fetchAlamatData();
    // }, [id, token]);

    useEffect(() => {
        async function fetchAlamatData() {
            try {
                // if (id) {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Alamat?where=pembeli_id,${id}&get=*`, {
                    headers: {
                        Authorization: `Bearer ` + token,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Alamat[] = await response.json();
                console.log("data", data);

                // console.log(responseData);

                if (response) {
                    // const data: Alamat[] = responseData;
                    console.log("data", data);
                    setAddresses(data)
                    console.log("addresses", addresses);

                    setFilteredAddresses(data);
                }
                // console.log('Data:', data);
                // } else {
                //     console.error('No data received from the API.');
                // }
                // }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchAlamatData();
    }, [id, token]);


    // useEffect(() => {
    //     if (filteredAddresses.length === 0 && addresses.length === 0) {
    //         setAddresses(filteredAddresses);
    //     }
    // }, [filteredAddresses]);

    const containerStyles = (index: number, id: number) => ({
        border: '1px solid #ccc',
        padding: '16px 24px',
        backgroundColor: id === alamatId ? '#e6ffe6' : 'white',
        outline: selectedAddressIndex === index ? '2px solid green' : 'none',
    });

    return (
        <div>
            <Header />
            <link
                rel="stylesheet"
                href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"
            />
            <div className="flex px-20 mt-[75px] mb-[75px]">
                <div className="w-[256px] mr-10">
                    <Accordion
                        isDaftarAlamat={true}
                    // isPembeli={true}
                    />
                </div>
                <div className="w-[984px]">
                    <div>
                        <h3 className="font-bold text-[20px]">Daftar Alamat</h3>
                    </div>
                    <div className="flex justify-between mt-6">
                        <div>
                            <div className="w-[328px] searchBar flex items-center border border-gray-300 rounded-lg border-solid p-3">
                                <span className="mr-2 material-symbols-outlined">search</span>
                                <input
                                    // value={searchQuery}
                                    onChange={(e) => setSearchQuery(e.target.value)}
                                    placeholder="Cari alamat"
                                    className="font-roboto font-normal placing focus:ring-0 focus:outline-none text-sm"
                                />
                            </div>
                        </div>
                        {/* <AlamatBaru /> */}
                    </div>

                    {addresses.filter((address) => {
                        return searchQuery.toLowerCase() === '' ? address : address.nama.toLowerCase().includes(searchQuery) ||
                            address.nama_penerima.toLowerCase().includes(searchQuery) ||
                            address.alamat.toLowerCase().includes(searchQuery)
                    }).map((address, index) => (
                        <div
                            key={index}
                            style={containerStyles(index, address.id)}
                            onClick={() => handleClick(index)}
                            className="mt-6 rounded-md"
                        >
                            <div className="flex items-center">
                                <div className="text-base font-medium">{address.nama}</div>
                                {alamatId == address.id ?
                                    <div className='ml-4 text-xs' style={{ border: '1px solid green', padding:'5px', borderRadius: '5px'}}>
                                        Utama
                                    </div>
                                    : ''}
                            </div>
                            <div className="mt-4 flex">
                                <p className="mr-2 text-[20px] font-medium">{address.nama_penerima}</p>
                                <p className="self-center"> | </p>
                                <p className="ml-2 text-[14px] self-center">{value!['phone']}</p>
                            </div>
                            <div className="mt-2">
                                {address.alamat}
                                {/* {address.daerah}, {address.kecamatan}, {address.kota}, {address.provinsi}, {address.kode_pos} */}
                            </div>
                            {/* <div className="text-[#828282] mt-2">
                                {address.daerah} - {address.kecamatan} - {address.kota} - {address.provinsi} - {address.kode_pos}
                            </div> */}
                            <div className="mt-4 flex ">
                                {/* <EditAlamat

                                    nama={address.nama}
                                    namaPenerima={address.nama_penerima}
                                    lat={address.latitude}
                                    long={address.longitude}
                                    alamat={address.alamat}
                                /> */}
                                {/* <button className="font-medium text-[#828282] ml-4 " onClick={() => deleteAlamat(address.id)}>
                                    Hapus {address.id}
                                </button> */}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Page;
