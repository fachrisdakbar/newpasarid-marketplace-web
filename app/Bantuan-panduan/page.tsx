"use client"
import React, { useEffect, useState } from 'react'
import cheerio, { AnyNode } from 'cheerio';
import ArrowBack from '../../public/arrowback_white.svg'
import { useRouter } from 'next/navigation';
import Image from 'next/image';


// interface Bantuan-panduan {
//     id: number,
//     ask: string,
//     question: string
// }

export default function Bantuan() {
    const [description, setDescription] = useState([]);
    const cheerio = require('cheerio');
    const [title, setTitle] = useState('');
    const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';

    const extractTextFromHtml = (html: string | AnyNode | AnyNode[] | Buffer) => {
        const $ = cheerio.load(html);
        return $('body').text();
    };
    const router = useRouter()

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/content?type=bantuan`, 
                    // headers: {
                    //     Authorization: `Bearer ${token}`,
                    // }
                )
                const value = await response.json();
                setTitle(value.title)
                setDescription(value.description)
            } catch (error) {
                console.error('Error fetching key:', error)
            }
        }
        fetchData()
    }, []);


    return (
        <>
            <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                    <div className='self-center mr-[106px]'>
                        <button onClick={router.back}>
                            <Image src={ArrowBack} alt='' className='fill-current text-white' />
                        </button>
                    </div>
                    <div>
                        <h3 className='font-medium text-white text-[20px] mr-[100px]'>Bantuan</h3>
                    </div>
                </div>
        <section className='content-detail-wrapper'>
         <div dangerouslySetInnerHTML={{ __html: title }} className='font-semibold mb-5' />
         <div dangerouslySetInnerHTML={{ __html: description }}/>
        </section>
        </>
    )
}