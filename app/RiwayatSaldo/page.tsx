'use client'
import React, { useEffect, useState } from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Accordion from '../Accordion/accordion'

interface Riwayat {
    response: {
        id: number,
        bank: string,
        amount: string,
        transaction_type: string,
        created_at: string
    }[]
}

const page = ({ }) => {
    const [riwayats, setRiwayat] = useState<Riwayat>({ response: [] });
    const [token, setToken] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        const typeUser = localStorage.getItem('type');

        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        async function fetchDataPembeli() {
            try {
                const response = await fetch(`/api/getUserPembeli`, {
                    headers: {
                        // mode : 'no-cors',
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    }
                });
                if (!response.ok) {
                    console.log("error");
                    throw new Error('Network response was not ok');
                }
                const res = await response.json();
                const data = res.data
                console.log(data);
                setPhoneNumber(data.pembeli.phone)

            } catch (error) {
                console.error('Error fetching data a:', error);
            }
        }

        async function fetchDataRiwayat() {
            try {
                const responses = await fetch(`https://api.pasarid-dev.bitcorp.id/api/payment/get-history-transactions?phone=${phoneNumber}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!responses.ok) {
                    throw new Error('Network responses was not ok');
                }
                const data: Riwayat[] = await responses.json();
                console.log(data);
                data.response.forEach((riwayat) => {
                    const dateParts = riwayat.created_at.split('T')[0].split('-');
                    const year = dateParts[0];
                    const month = dateParts[1];
                    const day = dateParts[2];
                    const formattedDate = new Date(year, month - 1, day).toLocaleDateString('id-ID', { day: '2-digit', month: 'long', year: 'numeric' });
                    riwayat.created_at = formattedDate;
                });
                setRiwayat(data);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchDataRiwayat()
        fetchDataPembeli()
    }, [token, phoneNumber])

    return (
        <div>
            <Header />
            <div className="flex pt-[75px] pb-[75px] px-20">
                <div className='w-[256px] mr-10 '>
                    <Accordion />
                </div>
                <div className='flex justify-center shadow-lg'>
                    <div className='bg-white rounded-lg w-[60vw]'>
                        <div className='pl-12 pt-4 pb-4 pr-12'>
                            <div className='font-bold text-base pb-2'>Riwayat Transaksi Saldo</div>
                            {riwayats.response.map((riwayat) => (
                                <div key={riwayat.id}>
                                    <div className='font-semibold'>
                                        <div className='text-abu-abu font-semibold text-base pb-3 pt-3'>{riwayat.created_at}</div>
                                        <div className='bg-abu-abu-terang rounded-lg p-3'>
                                            <div className='font-medium'>{riwayat.bank}</div>
                                            <div className="flex text-abu-terang justify-between">
                                                <div>{riwayat.transaction_type == 'kredit' ? 'Dana Keluar' : 'Dana Masuk'}</div>
                                                <div>{"Rp "+ formatNumber(riwayat.amount)}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default page