"use client"
import React, { useEffect, useState } from 'react'
import cheerio, { AnyNode } from 'cheerio';
import { useRouter } from 'next/navigation';
import ArrowBack from '../../public/arrowback_white.svg'
import Image from 'next/image';


export default function syaratKetentuan() {
    const [title, setTitle] = useState('');
    // const[subtitle,setSubtitle] = useState('');
    const [description, setDescription] = useState('');

    const extractTextFromHtml = (html: string | AnyNode | AnyNode[] | Buffer) => {
        const $ = cheerio.load(html);
        return $('body').text();
      };

    useEffect(() => {
        fetch("https://api.pasarid-dev.bitcorp.id/api/content?type=kebijakan-privasi")
            .then((response) => response.json())
            .then((data) => {

                setTitle(data.title);
                // setSubtitle(data.subtitle);
                setDescription(data.description);
            })
            .catch(error => console.error('Error fetching key:', error));
    }, []);
    const router = useRouter()

    return (
        <>
        <div className="flex justify-center w-full bg-[#94A561] py-[6.5px]" >
                <div className='self-center mr-[106px]'>
                    <button onClick={router.back}>
                        <Image src={ArrowBack} alt='' className='fill-current text-white' />
                    </button>
                </div>
                <div>
                    <h3 className='font-medium text-white text-[20px] mr-[100px]'>Kebijakan Privasi</h3>
                </div>
            </div>
        <section>
            <p className='text-[38px] pt-5 pb-2 px-5 font-extrabold'>{title}</p>
            <div dangerouslySetInnerHTML={{ __html: description }} className='px-5'/>
        </section>
        </>
    )
}
