'use client'
import React, { useEffect, useState } from 'react'

const VerifikasiKode = () => {
    const [display, setDisplay] = useState('none')
    const [codeVerification, setCodeVerification] = useState('')

    const handleCodeChange=(event: { target: { value: any; }; })=>{
        console.log(event.target.value);
        setCodeVerification(event.target.value)
    }

    useEffect(() => {
        console.log(codeVerification.length);
        
    }, [codeVerification])
    
    return (

        <div className=''>
            <div 
            className='text-[#788E39] font-semibold text-sm border-[2px] border-[#788E39] rounded-2xl p-1 w-[150px] flex justify-center cursor-pointer' style={{ display: '' }}
            onClick={() => setDisplay('flex')}>Pesanan Diambil</div>
            <div className='bg-opacity-50 bg-black fixed inset-0 z-30 flex justify-center items-center' style={{ display: display }}>
                <div className='bg-white h-[29vh] w-[25vw] p-3 text-abu-abu font-bold rounded-lg'>
                    <div className='flex justify-end'>
                    <svg onClick={(e) => setDisplay('none')} className='self-center cursor-pointer' width="12" height="12" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z" fill="#858585" />
                    </svg>
                    </div>
                    <div className="flex mt-2">
                    <div className='ml-1'>Kode Pickup</div>
                    </div>
                    <div className="mb-1 mt-4 px-3 py-2 border rounded-md">
                    <input
                      className="w-full hover:border-indigo-300 focus:outline-none focus:ring-0 text-sm font-sans no-spinners"
                      type="text"
                      id="codeVerification"
                      value={codeVerification}
                      onChange={handleCodeChange}
                      maxLength={6}
                      placeholder="Masukkan Kode Pengambilan"
                    />
                  </div>
                <div className='text-sm mt-1 ml-[5px]'>{codeVerification.length}/6</div>
                <div className='mt-2 mb-2 bg-merah-lihat-semua font-bold text-white rounded-3xl p-2 w-[110px] flex justify-center cursor-pointer text-sm'>Verifikasi</div>
                </div>
            </div>
        </div>
    )
}

export default VerifikasiKode