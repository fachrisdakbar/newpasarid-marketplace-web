'use client'
import Header from '@/app/components/Header'
import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import Accordion from '../../../Accordion/accordion'
import Account from '../../../../public/account.svg'
import Delivery from '../../../../public/delivery.svg'
import PickUp from '../../../../public/pickup.svg'
import Footer from '@/app/components/Footer'
import { useParams, useRouter } from 'next/navigation'

interface Order {
    id: number,
    note: string,
    store_order_id: string,
    promo_value: number,
    status: string,
    created_at: string,
    pengiriman_id: number,
    pembeli: {
        user: {
            name: string,
        }
    }
    pengiriman: {
        alamat_nama_penerima: string,
        waktu_pengiriman: string,
        promo_value: number
        biaya: number,
        jenis_pengiriman: {
            title: string
        }
        booking_id: string
    },
    pembayaran: {
        jenis_pembayaran: {
            title: string
        }
    }
    order_items: [
        {
            id : string,
            created_at: string,
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {

    const [token, setToken] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState<Order[]>([]);
    const [type, setType] = useState('');
    const [totals, setTotal] = useState<number>();
    const { id } = useParams();
    const router = useRouter();
    const [orderStatus, setOrderStatus] = useState(() => {
        const storedStatus = localStorage.getItem('orderStatus');
        return storedStatus ? JSON.parse(storedStatus) : {};
    });

    async function fetchData() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.images&with[]=orderItems(where=penjual_id,${idPenjual})(where=status,[diproses])&with[]=pembeli.user&with[]=pengiriman.jenisPengiriman&with[]=pembayaran.jenisPembayaran&whereHas[]=orderItems(where=penjual_id,${idPenjual})(where=status,[diproses])&where=id,${id}&orderBy=created_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Order[] = await response.json();
                setOrders(data);
                console.log(data);

                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsOrder(true)
                    setLoading(false)
                }

                let total = 0;
                data[0]?.order_items.forEach((item: any) => {
                    const subtotal = item.jumlah * item.produk.harga;
                    console.log(subtotal);
                    total += subtotal;
                });
                console.log(total);


                // Add pengiriman.biaya to the total
                // total += data[0]?.pengiriman?.biaya || 0;

                console.log('Total:', total);
                setTotal(total);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPenjual]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPenjual])


    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)
        return formattedDate;
    }

    const editData = (id: any) => {
        const indexs = orders.findIndex((o) => o.id === id);
        let index;
        for (let i = 0; i < orders.length; i++) {
            if (orders[i].id == id) {
                index = i;
            }
        }
        let idOrderItems;
        let count = 0;
        let idKurir;
        for (let i = 0; i < orders[index!].order_items.length; i++) {
            console.log(orders[index!].order_items[i].id);
            if (orders[index!]?.pengiriman_id) {
                console.log(orders[index!]?.store_order_id);
                let storeid = orders[index!]?.store_order_id
                console.log('ni orderid',orders[index!]?.store_order_id);
                
                editDelivery(storeid)
                setOrderStatus((prevStatus: any) => ({
                    ...prevStatus,
                    [id]: 'selesai'
                }));
                // router.push(`/PesananDariPembeli/PesananMenyiapkan`)
            }
            else if(orders[index!]?.pengiriman?.jenis_pengiriman.title == 'Kurir Pasar.id'){
                idKurir = orders[index!].order_items[i].id
                editKurir(idKurir);
                // setLoadingAction(true)
                count = count + 1
                console.log(count);
                if (count == orders[index!].order_items.length) {
                    router.push(`/PesananDariPembeli/PesananSedangDikirim`)
                }
            }
            else {
                idOrderItems = orders[index!].order_items[i].id
                editStatus(idOrderItems)
                count = count + 1
                console.log(count);
                if (count == orders[index!].order_items.length) {
                    setTimeout(() => {
                        router.push(`/PesananDariPembeli/PesananSiapDiambil`) 
                      }, 1000);
                }
            }

        }
    }

    async function editStatus(idOrderItems: any) {
        try {
            const response = await fetch(`/api/PesananDariPembeliMenyiapkanPickup`, {
                method: 'POST',
                body: JSON.stringify({
                    id: idOrderItems,
                    status: 'disiapkan',
                    token: token,
                    _method: 'PUT',
                }),
            });
            const responseData = await response.json();
            if (response.ok && responseData) {
                console.log('edit status success!');

            } else {
                console.error('Error:', responseData.message);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }

    }
    async function editKurir(idKurir: any) {
        try {
            const response = await fetch(`/api/PesananDariPembeliMenyiapkanKurir`, {
                method: 'POST',
                body: JSON.stringify({
                    id: idKurir,
                    status: 'dikumpulkan',
                    token: token,
                    _method: 'PUT',
                }),
            });
            const responseData = await response.json();
            if (response.ok && responseData) {
                console.log('edit status success!');

            } else {
                console.error('Error:', responseData.message);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }

    }
    async function editDelivery(storeid: any) {
        try {
            const response = await fetch(`/api/PesananDariPembeliMenyiapkanDelivery`, {
                method: 'POST',
                body: JSON.stringify({
                    token: token,
                    store_order_id: storeid
                }),
            });
            const responseData = await response.json();
            if (response.ok && responseData) {
                console.log('edit status success!');

            } else {
                console.error('Error:', responseData.message);
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }

    }

    useEffect(() => {
        localStorage.setItem('orderStatus', JSON.stringify(orderStatus));
      }, [orderStatus]);

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-24">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananDariPembeliMenyiapkan={true}
                    />
                </div>
                <div className='w-[65vw] shadow-lg font-roboto'>
                    <div className='flex self-center text-center items-center'>
                        <div className='pl-6 pt-8 font-roboto font-normal flex text-black'>
                            <a href={'/PesananDariPembeli/PesananMenyiapkan'} className='text-[14px] mr-1 no-underline'>Menyiapkan / </a>
                            <div className='text-[14px] mr-1 no-underline'>Detail</div>
                        </div>
                    </div>
                    {loading == true ?
                        <div className='flex flex-col justify-center items-center'>
                            <div className='loadingContainer '>
                                <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                <div className="loadingAnimation"></div>
                            </div>
                        </div>
                        :
                        <>
                            <div className='w-full pl-6 pr-6 pt-3 pb-8 font-roboto'>
                                <div className="flex items-center mt-2">
                                    {
                                        orders[0]?.['pengiriman_id'] ?
                                            <div className='bg-hijau w-[60px] p-2 rounded-[100px] flex justify-center items-center mr-4'>
                                                <Image src={Delivery} alt='' width={100} height={100}
                                                    className='w-[30px] h-[30px]'
                                                />
                                            </div>
                                            :
                                            <div className='bg-blue-200 w-[60px] p-2 rounded-[100px] flex justify-center items-center mr-4'>
                                                <Image src={PickUp} alt='' width={100} height={100}
                                                    className='w-[30px] h-[30px]'
                                                />
                                            </div>
                                    }
                                    <div className='text-sm'>
                                        <div>Status Pesanan</div>
                                        <div className='font-semibold'>Pesanan Diproses</div>
                                    </div>
                                </div>
                                <div className='flex mt-3 text-sm'>
                                    <div className='text-abu-terang mr-1'>Tanggal Pesanan : </div>
                                    <div className='text-sm'>{(() => {
                                        const formattedDate = formatDate(orders[0]?.['created_at']);
                                        const justDate = formattedDate.split(' ')
                                        return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                    })()}
                                    </div>
                                </div>
                                <div className="text-sm mt-3 flex">
                                    <div className='text-abu-terang mr-1'>Metode Pembayaran : </div>
                                    <div className=''>{orders[0]?.['pembayaran']?.['jenis_pembayaran']?.['title']}</div>
                                </div>
                                <div className="text-sm mt-3 flex">
                                    <div className='text-abu-terang mr-1'>Metode Pengiriman : </div>
                                    {
                                        orders[0]?.['pengiriman_id'] ?
                                            <div className=''>{orders[0]?.['pengiriman']?.['jenis_pengiriman']?.['title']}</div>
                                            :
                                            <div className=''>Pickup</div>
                                    }
                                </div>
                                <div className="text-sm mt-3">
                                    <div className='font-semibold'>Detail Pesanan ({orders[0]?.['order_items']['length']} Barang)</div>
                                </div>
                                <div className="text-sm flex mt-3">
                                    <div className='text-abu-terang mr-1'>No.Order : </div>
                                    <div className='font-semibold'>{orders[0]?.['store_order_id']}</div>
                                </div>
                                <div className='flex items-center mt-3'>
                                    <div className='mr-2 w-[25px] h-[25px]'>
                                        <Image src={Account} alt='' width={100} height={100} />
                                    </div>
                                    {
                                        orders[0]?.['pengiriman_id'] ?
                                            <div>{orders[0]?.['pengiriman']?.['alamat_nama_penerima']}</div>
                                            :
                                            <div>{orders[0]?.['pembeli']?.['user']['name']}</div>
                                    }
                                </div>
                                {orders[0]?.['order_items']?.map((item: any, index: any) => (
                                    <div key={index}>
                                        <div className='flex mt-3'>
                                            <div className='w-[120px] h-[120px] mr-2 flex items-center'>
                                                <Image src={item?.produk.images[0] ? item?.produk?.images[0].image : item?.produk?.image} alt='' height={100} width={100}
                                                    className='rounded-lg h-[100px] w-[120px]' />
                                            </div>
                                            <div className='flex flex-col justify-center'>
                                                <div className='mb-3 text-sm'>{item?.produk?.nama}</div>
                                                <div className='text-abu-abu text-sm mb-3'>{"Rp " + formatNumber(item?.produk.harga) + ",00"}</div>
                                                <div className='flex'>
                                                    <div className='mr-1 text-sm text-abu-abu'>Jumlah</div>
                                                    <div className='text-merah-lihat-semua text-sm font-semibold'>{item?.jumlah}</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className='text-sm text-abu-abu'>Catatan Pembeli : {orders[0]?.['note'] ? orders[0]?.['note'] : '-'}</div>
                                    </div>
                                ))}
                                <div className="flex text-sm mt-3">
                                    <div className='mr-2'>Total</div>
                                    <div className='font-semibold'>{"Rp " + formatNumber(totals) + ",00"}</div>
                                </div>
                                <div className='mt-4'>
                                    <div className='bg-[#788E39] w-[250px] rounded-3xl p-2 text-white text-sm flex justify-center items-center cursor-pointer' onClick={() => editData(orders[0].id)} >Selesai Menyiapkan</div>
                                </div>
                            </div>
                        </>
                    }
                </div>
            </div>
            <Footer />
        </>

    )
}

export default page