'use client'
import Header from '@/app/components/Header'
import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import Accordion from '../../../Accordion/accordion'
import Account from '../../../../public/account.svg'
import Delivery from '../../../../public/delivery.svg'
import Footer from '@/app/components/Footer'
import { useParams } from 'next/navigation'

const page = () => {

    const [token, setToken] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState();
    const [type, setType] = useState('');
    const [totals, setTotal] = useState<number>();
    const { id } = useParams()

    async function fetchData() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.images&with[]=orderItems(where=penjual_id,${idPenjual})(where=status,[dikirim,dikumpulkan,menunggu dikirim])&with[]=pembeli.user&with[]=pengiriman.jenisPengiriman&with[]=pembayaran.jenisPembayaran&whereHas[]=orderItems(where=penjual_id,${idPenjual})(where=status,[dikirim,dikumpulkan,menunggu dikirim])&where=id,${id}&orderBy=created_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setOrders(data[0]);
                console.log(data);

                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsOrder(true)
                    setLoading(false)
                }

                let total = 0;
                data[0]?.order_items.forEach((item: any) => {
                    const subtotal = item.jumlah * item.produk.harga;
                    console.log(subtotal);
                    total += subtotal;
                });
                console.log(total);


                // Add pengiriman.biaya to the total
                // total += data[0]?.pengiriman?.biaya || 0;

                console.log('Total:', total);
                setTotal(total);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPenjual]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPenjual])


    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)


        return formattedDate;
    }

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-24">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananDariPembeliSedangDikirim={true}
                    />
                </div>
                <div className='w-[65vw] shadow-lg font-roboto'>
                    <div className='flex self-center text-center items-center'>
                        <div className='pl-6 pt-8 font-roboto font-normal flex text-black'>
                            <a href={'/PesananDariPembeli/PesananSedangDikirim'} className='text-[14px] mr-1 no-underline'>Sedang Dikirim / </a>
                            <div className='text-[14px] mr-1 no-underline'>Detail</div>
                        </div>
                    </div>
                    {loading == true ?
                        <div className='flex flex-col justify-center items-center'>
                            <div className='loadingContainer '>
                                <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                <div className="loadingAnimation"></div>
                            </div>
                        </div>
                        :
                        <>
                            <div className='w-full pl-6 pr-6 pt-3 pb-8 font-roboto'>
                                <div className="flex items-center mt-2">
                                    <div className='bg-hijau w-[60px] p-2 rounded-[100px] flex justify-center items-center mr-4'>
                                        <Image src={Delivery} alt='' width={100} height={100}
                                            className='w-[30px] h-[30px]'
                                        />
                                    </div>
                                    <div className='text-sm'>
                                        <div>Status Pesanan</div>
                                        <div className='font-semibold'>Sedang Dikirim</div>
                                    </div>
                                </div>
                                <div className='flex mt-3 text-sm'>
                                    <div className='text-abu-terang mr-1'>Tanggal Pesanan : </div>
                                    <div className='text-sm'>{(() => {
                                        const formattedDate = formatDate(orders?.['created_at']);
                                        const justDate = formattedDate.split(' ')
                                        return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                    })()}
                                    </div>
                                </div>
                                <div className="text-sm mt-3 flex">
                                    <div className='text-abu-terang mr-1'>Metode Pembayaran : </div>
                                    <div className=''>{orders?.['pembayaran']?.['jenis_pembayaran']?.['title']}</div>
                                </div>
                                <div className="text-sm mt-3 flex">
                                    <div className='text-abu-terang mr-1'>Metode Pengiriman : </div>
                                    <div className=''>{orders?.['pengiriman']?.['jenis_pengiriman']?.['title']}</div>
                                </div>
                                <div className="text-sm mt-3">
                                    <div className='font-semibold'>Detail Pesanan ({orders?.['order_items']['length']} Barang)</div>
                                </div>
                                <div className="text-sm flex mt-3">
                                    <div className='text-abu-terang mr-1'>No.Order : </div>
                                    <div className='font-semibold'>{orders?.['store_order_id']}</div>
                                </div>
                                <div className='flex items-center mt-3'>
                                    <div className='mr-2 w-[25px] h-[25px]'>
                                        <Image src={Account} alt='' width={100} height={100} />
                                    </div>
                                    <div className='font-semibold text-sm'>{orders?.['pengiriman']?.['alamat_nama_penerima']}</div>
                                </div>
                                {orders?.['order_items']?.map((item: any, index: any) => (
                                    <div key={index}>
                                        <div className='flex mt-3'>
                                            <div className='w-[120px] h-[120px] mr-2 flex items-center'>
                                                <Image src={item?.produk.images[0] ? item?.produk?.images[0].image : item?.produk?.image} alt='' height={100} width={100}
                                                    className='rounded-lg h-[100px] w-[120px]' />
                                            </div>
                                            <div className='flex flex-col justify-center'>
                                                <div className='mb-3 text-sm'>{item?.produk?.nama}</div>
                                                <div className='text-abu-abu text-sm mb-3'>{"Rp " + formatNumber(item?.produk.harga) + ",00"}</div>
                                                <div className='flex'>
                                                    <div className='mr-1 text-sm text-abu-abu'>Jumlah</div>
                                                    <div className='text-merah-lihat-semua text-sm font-semibold'>{item?.jumlah}</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className='text-sm text-abu-abu'>Catatan Pembeli : {orders?.['note'] ? orders?.['note'] : '-'}</div>
                                    </div>
                                ))}
                                <div className="flex text-sm mt-3">
                                    <div className='mr-2'>Total</div>
                                    <div className='font-semibold'>{"Rp " + formatNumber(totals) + ",00"}</div>
                                </div>
                            </div>
                        </>
                    }
                </div>
            </div>
            <Footer />
        </>

    )
}

export default page