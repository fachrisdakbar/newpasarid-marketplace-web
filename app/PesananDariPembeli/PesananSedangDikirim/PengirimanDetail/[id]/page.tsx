'use client'
import Footer from '@/app/components/Footer';
import Header from '@/app/components/Header';
import { useParams } from 'next/navigation';
import React, { useEffect, useState } from 'react'
import Accordion from '../../../../Accordion/accordion'
import Link from 'next/link';


interface Order {
    id: number,
    note: string,
    store_order_id: string,
    promo_value: number,
    status: string,
    created_at: string,
    pengiriman_id: number,
    pembeli: {
        user: {
            name: string,
        }
    }
    pengiriman: {
        alamat_nama_penerima: string,
        waktu_pengiriman: string,
        promo_value: number
        biaya: number,
        jenis_pengiriman: {
            title: string
        }
        booking_id: string
    },
    pembayaran: {
        jenis_pembayaran: {
            title: string
        }
    }
    order_items: [
        {
            id : string,
            created_at: string,
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {
    const [token, setToken] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState<Order[]>([]);
    const [type, setType] = useState('');
    const [totals, setTotal] = useState<number>();
    const { id } = useParams()

    async function fetchData() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.images&with[]=orderItems(where=penjual_id,${idPenjual})(where=status,[dikirim,dikumpulkan,menunggu dikirim])&with[]=pembeli.user&with[]=pengiriman.jenisPengiriman&with[]=pembayaran.jenisPembayaran&whereHas[]=orderItems(where=penjual_id,${idPenjual})(where=status,[dikirim,dikumpulkan,menunggu dikirim])&where=id,${id}&orderBy=created_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Order[] = await response.json();
                setOrders(data);
                console.log(data);
                console.log(orders);
                

                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsOrder(true)
                    setLoading(false)
                }

                let total = 0;
                data[0]?.order_items.forEach((item: any) => {
                    const subtotal = item.jumlah * item.produk.harga;
                    console.log(subtotal);
                    total += subtotal;
                });
                console.log(total);


                // Add pengiriman.biaya to the total
                // total += data[0]?.pengiriman?.biaya || 0;

                console.log('Total:', total);
                setTotal(total);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPenjual]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPenjual])
    return (

        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-24">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananDariPembeliSedangDikirim={true}
                    />
                </div>
                <div className='w-[65vw] shadow-lg font-roboto'>
                    <div className='flex self-center text-center items-center'>
                        <div className='pl-6 pt-8 font-roboto font-normal flex text-black'>
                            <a href={'/PesananDariPembeli/PesananSedangDikirim'} className='text-[14px] mr-1 no-underline'>Sedang Dikirim / </a>
                            <div className='text-[14px] mr-1 no-underline'>Detail Pengiriman</div>
                        </div>
                    </div>
                <div className='pl-6 mt-3 text-sm pb-[40px]'>
                    <div>
                        <div className='text-abu-terang'>Booking ID</div>
                        <div className='font-bold'>{orders[0]?.pengiriman?.booking_id}</div>
                    </div>
                    <div className='mt-2'>
                        <div className='text-abu-terang'>Status Pesanan</div>
                        <div className='font-bold'>Menunggu update dari driver</div>
                    </div>
                    <div className='mt-2'>
                        <div className='text-abu-terang'>Nama Penerima</div>
                        <div className='font-bold'>{orders[0]?.pengiriman?.alamat_nama_penerima}</div>
                    </div>
                    {/* <div className='mt-2 font-bold'>Driver Anda</div> */}
                </div>
                </div>
            </div>
            <Footer />
        </>
    )
}

export default page