'use client'
import Header from '@/app/components/Header'
import Image from 'next/image'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import Accordion from '../../Accordion/accordion'
import Clipboard from '../../../public/Clipboard.svg'
import StripesComponent from '@/app/OrderManagement/StripComponent/StripesComponent'
import Account from '../../../public/account.svg'
import Delivery from '../../../public/delivery.svg'
import PickUp from '../../../public/pickup.svg'
import Footer from '@/app/components/Footer'

interface Order {
    id: number,
    note: string,
    store_order_id: string,
    promo_value: number,
    status: string,
    created_at: string,
    pengiriman_id: number,
    pembeli: {
        user: {
            name: string,
        }
    }
    pengiriman: {
        booking_id : string,
        alamat_nama_penerima: string,
        waktu_pengiriman: string,
        promo_value: number
        biaya: number,
        jenis_pengiriman: {
            title: string
        }
    },
    pembayaran: {
        jenis_pembayaran: {
            title: string
        }
    }
    order_items: [
        {
            created_at: string,
            produk_id: number,
            jumlah: number,
            pasar: {
                nama: string
            }
            produk: {
                nama: string,
                harga: number,
                images: {
                    image: string
                }
                penjual: {
                    nama_toko: string
                }
            }
        }
    ]
}

const page = () => {

    const [token, setToken] = useState('');
    const [idPenjual, setIdPenjual] = useState('');
    const [isOrder, setIsOrder] = useState(true);
    const [loading, setLoading] = useState(true);
    const [orders, setOrders] = useState<Order[]>([]);
    const [type, setType] = useState('');

    async function fetchData() {
        try {
            const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/user?type=penjual`, {
                headers: {
                    // mode : 'no-cors',
                    Authorization: `Bearer ${token}`,
                    // 'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                console.log("error");
                throw new Error('Network response was not ok');
            }
            const res = await response.json();
            const data = res
            console.log(data);
            setIdPenjual(data.penjual.id)
        } catch (error) {
            console.error('Error fetching data a:', error);
        }
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Order?with[]=orderItems.produk.images&with[]=orderItems(where=penjual_id,${idPenjual})(where=status,[dikirim,dikumpulkan,menunggu dikirim])&with[]=pembeli.user&with[]=pengiriman.jenisPengiriman&with[]=pembayaran.jenisPembayaran&whereHas[]=orderItems(where=penjual_id,${idPenjual})(where=status,[dikirim,dikumpulkan,menunggu dikirim])&orderBy=created_at,DESC&get=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Order[] = await response.json();
                setOrders(data);
                console.log(data);


                // for (let i = 0; i < data.length; i++) {
                //     console.log(data[i].order_items.length);
                // }
                if (!data[0]) {
                    setIsOrder(false)
                    setLoading(false)
                }
                else if (data[0]) {
                    setIsOrder(true)
                    setLoading(false)
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token, idPenjual]);

    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);

        } else {
            console.error('Token data not found in local storage');
        }
        fetchData()
        // fetchOrder()
    }, [token, idPenjual])


    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    const formatDate = (date: any) => {
        const options: Intl.DateTimeFormatOptions = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            timeZoneName: 'short',

        }
        const localizedDate = new Date(date);
        localizedDate.setHours(localizedDate.getHours() - 7);
        const formattedDate = localizedDate.toLocaleString('id-ID', options);

        const time = formattedDate.match(/\d+:\d+/)


        return formattedDate;
    }

    return (
        <>
            <Header />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
            <div className="flex pt-[75px] pb-[75px] px-24">
                <div className='w-[256px] mr-10 '>
                    <Accordion
                        isPesananDariPembeliSedangDikirim={true}
                    />
                </div>
                <div>
                    <div className='font-bold mb-2 text-abu-abu'>Sedang Dikirim</div>
                    <div className='w-[65vw] shadow-lg font-roboto'>
                        <div className='flex flex-col '>
                            {isOrder == false &&
                                <div className='flex justify-center items-center flex-col font-roboto pt-[75px] pb-[75px]'>
                                    <div className='text-lg font-semibold'>Pesanan Dikirim</div>
                                    <div>Pesanan dikirim akan tampil disini</div>
                                    <div>
                                        <Image alt='' src={Clipboard} width={100} height={100}
                                            className='w-[300px] h-[300px]'
                                        />
                                    </div>
                                </div>
                            }
                        </div>
                        <div className=''>
                            {loading == true ?
                                <div className='flex flex-col justify-center items-center'>
                                    <div className='loadingContainer '>
                                        <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                        <div className="loadingAnimation"></div>
                                    </div>
                                </div>
                                :
                                <>
                                    <div className='w-full pl-6 pr-6 pt-3 pb-3 font-roboto'>
                                        {orders.map((order) => {
                                            let subtotal = 0
                                            order.order_items.forEach(item => {
                                                const itemSubtotal = item.produk.harga * item.jumlah;
                                                subtotal += itemSubtotal;
                                            });
                                            const total = subtotal
                                            return (
                                                <div className='shadow-md p-3 mb-3'>
                                                    <div className="flex justify-between">
                                                        <div className='flex'>
                                                            <div className='text-sm mr-[91px]'>No. Order</div>
                                                            <div className='text-sm font-semibold'>{order?.store_order_id}</div>
                                                        </div>
                                                        <div className='text-sm flex items-center'>
                                                            <div className='text-sm text-abu-abu'>{(() => {
                                                                const formattedDate = formatDate(order.created_at);
                                                                const justDate = formattedDate.split(' ')
                                                                return formattedDate.replace('pukul', '').replace(/\./g, ':');

                                                            })()}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="flex justify-between items-center">
                                                        <div className="flex">
                                                            <div className='text-sm mr-[102px]'>Pembeli</div>
                                                            {
                                                                order?.pengiriman_id ?
                                                                    <div className='text-sm font-semibold'>{order.pengiriman?.alamat_nama_penerima}</div>
                                                                    :
                                                                    <div className='text-sm font-semibold'>{order.pembeli?.user?.name}</div>
                                                            }
                                                        </div>
                                                        <div className="flex items-center">
                                                            {
                                                                order?.pengiriman_id ?
                                                                    <div className='flex items-center'>
                                                                        <div className='text-sm mr-2'>{order?.pengiriman?.jenis_pengiriman?.title}</div>
                                                                        <div>
                                                                            <Image src={Delivery} alt='' height={100} width={100}
                                                                                className='w-[30px] h-[50px]'
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    :
                                                                    <div className='flex items-center'>
                                                                        <div className='text-sm mr-2'>Pickup</div>
                                                                        <div>
                                                                            <Image src={PickUp} alt='' height={100} width={100}
                                                                                className='w-[30px] h-[50px]'
                                                                            />
                                                                        </div>
                                                                    </div>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="flex items-center">
                                                        <div className='text-sm mr-[22px]'>Metode Pembayaran</div>
                                                        <div className='text-sm font-semibold'>
                                                            {
                                                                order?.pembayaran?.jenis_pembayaran?.title == 'Saldo' ?
                                                                    'Saldo'
                                                                    :
                                                                    'BRIVA'
                                                            }
                                                        </div>
                                                    </div>
                                                    {order.order_items.map((item: any, index: any) => (
                                                        <div className='flex mt-2'>
                                                            <div className='w-[120px] h-[120px] mr-7 flex items-center'>
                                                                <Image src={item?.produk.images[0] ? item?.produk?.images[0].image : item?.produk?.image} alt='' height={100} width={100}
                                                                    className='rounded-lg h-[100px] w-[120px]' />
                                                            </div>
                                                            <div className='flex flex-col justify-center'>
                                                                <div className='mb-3 text-sm'>{item?.produk?.nama}</div>
                                                                <div className='text-abu-terang text-sm mb-3'>{item?.jumlah}</div>
                                                                <div className='font-semibold text-sm'>{"Rp " + formatNumber(item?.produk.harga)}</div>
                                                            </div>
                                                        </div>
                                                    ))}
                                                    <div className="flex mt-2">
                                                        <div className='text-sm  text-abu-terang mr-2'>Catatan dari pembeli : </div>
                                                        <div className='text-abu-terang text-sm'>{order?.note ? order?.note : '-'}</div>
                                                    </div>
                                                    <div className='mt-3 mb-3'><StripesComponent /></div>
                                                    <div className="flex items-center justify-between">
                                                        <div className="flex mt-2">
                                                            <div className='text-sm mr-2'>Total</div>
                                                            <div className='font-semibold text-sm'>{"Rp " + formatNumber(total)}</div>
                                                        </div>
                                                        <div className="flex justify-evenly">
                                                            <a href={`/PesananDariPembeli/PesananSedangDikirim/${order.id}`} className='text-sm text-abu-abu font-semibold flex justify-center mt-2 cursor-pointer mr-10'>Lihat Detail</a>
                                                            <a href={`/PesananDariPembeli/PesananSedangDikirim/PengirimanDetail/${order.id}`} className='text-sm text-abu-abu font-semibold flex justify-center mt-2 cursor-pointer'>Lihat Detail Pengiriman</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </>
                            }
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </>

    )
}

export default page