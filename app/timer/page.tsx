import React, { useState, useEffect, useRef } from 'react';

const Timer = ({ onTimeExpired, isSeconds }: any) => {
  const [seconds, setSeconds] = useState(120);
  const [timerStopped, setTimerStopped] = useState(false);
  const [isDisplay, setIsDisplay] = useState('block');

  // function untuk timer berjalan (2 menit)
  useEffect(() => {
    let interval: any;

    if (isSeconds) {
      interval = setInterval(() => {
        setSeconds(prevSeconds => {
          if (prevSeconds > 0) {
            return prevSeconds - 1;
          } else {
            clearInterval(interval);
            onTimeExpired();
            setTimerStopped(true);
            setIsDisplay('none');
            return prevSeconds;
          }
        });
      }, 1000);
    }
    else {
      setIsDisplay('none')
    }
    return () => clearInterval(interval);
  }, [isSeconds, onTimeExpired]);

  // untuk menghilangkan tampilan timer pada saat waktu sudah habis
  useEffect(() => {
    console.log(isSeconds);
    if (isSeconds) {
      setTimerStopped(false)
      setSeconds(120)
      setIsDisplay('block')
    }
    else {
      setIsDisplay('none')
    }
  }, [isSeconds]);


  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = seconds % 60;

  return (
    <div className='font-semibold text-xs ml-[185px] mt-[-20px] mb-3 text-merah-lihat-semua'>
      {timerStopped
        ?
        <div>
          Kode OTP kadaluarsa
        </div>

        : <div style={{ display: isDisplay }}>Sisa waktu berlakunya kode OTP : {String(minutes).padStart(2, '0')}:{String(remainingSeconds).padStart(2, '0')}</div>}
    </div>
  );
};

export default Timer;
