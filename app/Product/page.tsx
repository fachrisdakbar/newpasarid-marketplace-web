"use client"
import Image from 'next/image';
import { useEffect, useState } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Link from 'next/link';
import Toko from '../../public/storefront.svg';
import Rating from '../../public/star.svg';

interface Product {
    id: number;
    nama: string;
    harga: number;
    image: string;
    sold: number;
    berat: number;
    rating: number;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
    satuan: {
        name: string;
    }
    penjual: {
        pasar: {
            nama: string;
        }
    }
}

function Page() {
    const [products, setProducts] = useState<Product[]>([]);
    const [loading, setLoading] = useState(true);
    const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 60;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = products.slice(firstIdx, lastIdx);

    useEffect(() => {
        async function fetchData() {
            // untuk nge get produk
            try {
                const response = await fetch('https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?with[]=penjual.pasar&with[]=images&with[]=satuan&orderby=created_at,DESC&get=*', {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data: Product[] = await response.json();
                if (data.length != 0) {
                    setLoading(false);
                }
                setProducts(data);
                console.log(data);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, []);

    // untuk format harga
    const formatNumber = (value: any) => {
        return value.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }
    const formatRating = (rating: number | undefined) => {
        if (rating === undefined || rating === null) {
            return '-';
        }
        // Check if rating is already an integer
        if (Number.isInteger(rating)) {
            return rating.toFixed(1);
        }
        // Format rating to one decimal place
        return rating.toFixed(1);
    }

    return (
        <>
            <Header />
            <div className='home-content-wrapper shadow-lg pb-12'>
                <div className='flex self-center text-center items-center'>
                    <div className='pl-6 pt-12 font-roboto font-normal flex text-black'>
                        <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
                        <a href={'/Product'} className='text-[14px] mr-1 no-underline'>Product Terbaru</a>
                    </div>
                </div>
                <div className='mt-6 flex justify-between grid grid-cols-5 gap-5 pl-6 pr-6'>
                    {loading == true ?
                        <div className='flex flex-col justify-center items-center w-[75vw]'>
                            <div className='loadingContainer'>
                                <div className='mb-[10px] mt-[30px] text-base font-medium'>Loading...</div>
                                <div className="loadingAnimation"></div>
                            </div>
                        </div>
                        :
                        <>
                            {datas.map((product) => (
                                <div key={product?.id}>
                                    <div className='items-center shadow-md ' style={{ borderBottomLeftRadius: '10px', borderBottomRightRadius: '10px' }}>
                                        <a className='no-underline' href={`/Product/${product?.id}`}>
                                            <Image width={140} height={140} src={product?.image ? product?.image : ''} alt=''
                                                className='w-[40vw] h-[25vh] cursor-pointer' style={{ borderTopRightRadius: '10px', borderTopLeftRadius: '10px' }} />
                                            <div className='pl-3 pt-1 font-sans text-warna-text-title-produk font-semibold text-[14.7px] w-[135px]  mt-[3.67px] truncate cursor-pointer'>{product?.nama + " - " + product?.berat + " g"}</div>
                                            <div className='pl-3 pt-2 font-sans text-[13px] text-black flex flex-row items-center'><span className='font-semibold text-[17.15px] mr-1'>{"Rp " + formatNumber(product?.harga)}</span>{product?.satuan === null ? '' : "  /" + product?.satuan?.name}</div>
                                            <div className='flex'>
                                                <div className='pl-3 pt-2 flex items-center'>
                                                    <Image src={Toko} alt='' width={140} height={140}
                                                        className='w-[20px] h-[20px] mr-2'
                                                    />
                                                    <div className='text-xs text-abu-abu'>{product?.penjual ? product?.penjual?.pasar?.nama : 'Pasar -'}</div>
                                                </div>
                                            </div>
                                            <div className='pl-3 pt-2 pb-3 flex items-center'>
                                                <Image src={Rating} alt='' width={140} height={140}
                                                    className='w-[20px] h-[20px] mr-1'
                                                />
                                                {/* <div className='text-abu-abu text-xs mr-1'>{product?.rating ? product?.rating : '-'}</div> */}
                                                <div className='text-abu-abu text-xs mr-1'>{formatRating(product?.rating)}</div>

                                                <div className="text-[12.25px] font-normal  text-warna-text-sold-produk mr-4">{product?.sold == null ? "(0 terjual)" : "(" + product?.sold + " terjual)"}</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            ))}
                        </>
                    }
                </div>

            </div>
            <Footer />
        </>
    );
}

export default Page;
