'use client'
import Image from 'next/image'
import Link from 'next/link'
import { useParams } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import Carousel from 'react-material-ui-carousel'
import { useRouter } from 'next/navigation'


interface Banner {
    id: number;
    nama: string;
    harga: number;
    image: string;
    sold: number;
    berat: number;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

interface ProdukImage {
    id: number;
    produk_id: number;
    image: string;
    created_at: string;
    created_by: number;
    updated_at: string;
    updated_by: number;
    deleted_at: null | string;
}

const Modal = ({
}) => {
    const { id } = useParams()
    const [products, setProducts] = useState();
    const [imgBanners, setImgBanners] = useState<ProdukImage[]>([]);
    const [showModal, setShowModal] = useState('none');
    const [video, setVideo] = useState("");
    const token = '8938|cjHzia31P7B9P2l6YWDB00PZOyuusCpvyqI38fCP';
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 3;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const [carouselKey, setCarouselKey] = useState(0);
    const router = useRouter()

    const loadVideo = () => {
        setVideo(products?.['video_url']!)
    }

    // untuk carousel image
    const carouselItems =
        imgBanners.map((item) => (
            <div key={item.id} className='flex'>
                <div className="flex-shrink-0">
                    <div className='items-center'>
                        <Image width={140} height={140} src={item.image ? item.image : ""} alt='' className='w-[420px] h-[500px] rounded-[7.74px] ' />
                    </div>
                </div>
            </div>
        ));

    // untuk carousel video
    const additionalItem = (
        <div className="flex" key="additional-item">
            <div className='flex-shrink-0'>
                <div className='items-center'>
                    <video controls={true} src={video} className='w-[500px] h-[500px] rounded-[7.74px] pl-[-60px] absolute mb-[7.74px]' />
                </div>
            </div>
        </div>
    );

    // untuk carousel image
    const imgProductId = (
        <div className="flex" key="additional-item">
            <div className='flex-shrink-0'>
                <div className='items-center'>
                    <Image width={140} height={140} src={products?.['image']! ? products?.['image']! : ""} alt=''
                        className='w-[410px] h-[410px] cursor-pointer'
                    />
                </div>
            </div>
        </div>
    );

    // menggabungkan semua menjadi satu carousel
    const allItems = [imgProductId, ...carouselItems, additionalItem];

    // mereset tampilan carousel menjadi tampilan awal saat carousel di tutup
    const handleReset = () => {
        setCarouselKey(prevKey => prevKey + 1);
    };

    useEffect(() => {
        async function fetchData() {

            try {
                // nge get produk dengan join table penjual, pasar
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?where=id,${id}&with[]=penjual.pasar&with[]=images&first=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
                // nge get produk dengan join table penjual, pasar
                const responseBanner = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?where=id,${id}&with=penjual.pasar&first=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                const datas: ProdukImage[] = await response.json();
                if (!response.ok) {
                    console.log("gada data")
                    router.push('/register')
                }
                // merah tapi berjalan
                setImgBanners(datas.images);

                const data = await responseBanner.json();

                setVideo(data.video_url)
                setProducts(data);

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
        loadVideo()
    }, [video, setVideo]);

    // untuk menutup modal
    const changeModal = () => {
        setShowModal('none')
        handleReset()
    }

    // untuk membuka modal
    const viewModal = () => {
        setShowModal('block')
    }
    return (
        <>
            <div className='flex items-center justify-center'>
                <div onClick={() => viewModal()}>
                    <div className='block'>
                        <Image width={140} height={140} src={products?.['image']! ? products?.['image']! : ""} alt=''
                            className='w-[410px] h-[410px] pl-6 cursor-pointer'
                        />
                    </div>
                </div>
                <div>
                    <div>
                        {imgBanners.map((item => (
                            <div key={item.id} className='flex'>
                                <div className="flex-shrink-0">
                                    <div className='items-center'>
                                        <Image width={140} height={140} src={item.image ? item.image : ""} alt='' className='w-[96.7px] h-[96.7px] rounded-[7.74px] mb-[7.74px] ml-[7.74px]' />
                                    </div>
                                </div>
                            </div>
                        )))}
                    </div>
                    <div>
                        <video controls={true} src={video} className='w-[96.7px] h-[96.7px] rounded-[7.74px] mb-[7.74px] ml-[7.74px]'>
                            <source src={video} type="video/mp4" />
                        </video>
                    </div>
                </div>

                <section className='bg-black bg-opacity-50 inset-0 fixed' style={{ display: showModal }}>
                    <div className='flex self-center justify-center'>
                        <div className='h-[100vh] flex items-center'>
                            <div className='bg-white w-[450px] p-3 rounded-lg'>
                                <svg onClick={() => changeModal()} style={{ position: 'absolute', zIndex: 1000 }}
                                    className='cursor-pointer mb-4' width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.2453 9L17.5302 2.71516C17.8285 2.41741 17.9962 2.01336 17.9966 1.59191C17.997 1.17045 17.8299 0.76611 17.5322 0.467833C17.2344 0.169555 16.8304 0.00177586 16.4089 0.00140366C15.9875 0.00103146 15.5831 0.168097 15.2848 0.465848L9 6.75069L2.71516 0.465848C2.41688 0.167571 2.01233 0 1.5905 0C1.16868 0 0.764125 0.167571 0.465848 0.465848C0.167571 0.764125 0 1.16868 0 1.5905C0 2.01233 0.167571 2.41688 0.465848 2.71516L6.75069 9L0.465848 15.2848C0.167571 15.5831 0 15.9877 0 16.4095C0 16.8313 0.167571 17.2359 0.465848 17.5342C0.764125 17.8324 1.16868 18 1.5905 18C2.01233 18 2.41688 17.8324 2.71516 17.5342L9 11.2493L15.2848 17.5342C15.5831 17.8324 15.9877 18 16.4095 18C16.8313 18 17.2359 17.8324 17.5342 17.5342C17.8324 17.2359 18 16.8313 18 16.4095C18 15.9877 17.8324 15.5831 17.5342 15.2848L11.2453 9Z"
                                        fill="black" />
                                </svg>
                                <div className=''>
                                    <Carousel
                                        indicators={false}
                                        autoPlay={false}
                                        navButtonsAlwaysVisible={true}
                                        animation='slide'
                                        key={carouselKey}
                                    >
                                        {allItems}
                                    </Carousel>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </>
    )
}
export default Modal