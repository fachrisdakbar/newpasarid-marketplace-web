'use client'
import Footer from '@/app/components/Footer'
import Header from '@/app/components/Header'
import Image from 'next/image'
import Link from 'next/link'
import { useParams, useRouter } from 'next/navigation'
import React, { useEffect, useState } from 'react'
import Toko from '../../../public/toko.svg'
import Line from '../../../public/Line.svg'
import Star from '../../../public/star.svg'
import Decrement from '../../../public/decrement.svg'
import Increment from '../../../public/increment.svg'
import Modal from './Modal'
import Login from '../../login/page'

interface fetchProducts {
    id: number;
    nama: string;
    harga: number;
    image: string;
    sold: number;
    berat: number;
    created_by: number;
    updated_by: number;
    created_at: string;
    updated_at: string;
    deleted_at: null | string;
}

const page = () => {
    const router = useRouter()
    const { id } = useParams()
    const [isMobile, setIsMobile] = useState(false);
    const [products, setProducts] = useState();
    const [fetchProduct, setFetchProduct] = useState<fetchProducts[]>([]);
    const [namaToko, setNamaToko] = useState("undefined");
    const [showModal, setShowModal] = useState('none');
    const [rating, setRating] = useState(0);
    const [visible, setVisible] = useState(false);
    const [thisProductDetail, setThisProductDetail] = useState(true);
    const [video, setVideo] = useState(products?.['video_url']);
    const [quantity, setQuantity] = useState(0);
    const [jumlahProduk, setJumlahProduk] = useState<number>(0);
    const [token, setToken] = useState('')
    const [currPage, setCurrPage] = useState(1);
    const dataPerPage = 5;
    const lastIdx = dataPerPage * currPage;
    const firstIdx = lastIdx - dataPerPage;
    const datas = fetchProduct.slice(firstIdx, lastIdx);
    const hasToken = !!token;
    const [tokenManually, setTokenManually] = useState(false);

    const detectUserAgent = () => {
        // detect apakah user menggunakan laptop atau mobile
        const userAgent = window.navigator.userAgent;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(userAgent)) {
            setIsMobile(true)
            router.push(`https://newpasar.id/.well-known/produk.html?key=${id}`)
        } else {
            setIsMobile(false)
            router.push(`https://play.google.com/store/apps/details?id=com.firebase.pasar.dev`)
        }
    }
    useEffect(() => {
        const tokenData = localStorage.getItem('token');
        if (tokenData) {
            const parsedData = JSON.parse(tokenData);
            const extractedValue = `${parsedData.token}`;
            setToken(extractedValue);
            console.log(extractedValue);
            setTokenManually(true);
        } else {
            setTokenManually(false);
            setToken('11939|wiZLoQr2Th67GpSL3kRJb8rm2zlrDQGt9tSymvCr');
            console.error('Token data not found in local storage');
        }
    }, [token]);

    useEffect(() => {
        async function fetchData() {
            try {
                // untuk nge get produk dengan join table penjual, pasar
                const response = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?where=id,${id}&with[]=penjual.pasar&with[]=images&first=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                // untuk nge get produk dengan join table penjual, produk
                const response2 = await fetch(`https://api.pasarid-dev.bitcorp.id/api/elorest/Models/Produk?where=id,${id}&with=penjual.produk&first=*`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                else if (!response2.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                const dataFetchProduct = await response2.json();
                console.log(data);
                console.log(dataFetchProduct);
                
                

                setVideo(data.video_url)
                setProducts(data);
                setFetchProduct(dataFetchProduct.penjual.produk)
                setJumlahProduk(data?.jumlah)

                const test = 2.5;
                const isValid = test <= 3 && test >= 1;
                setRating((Math.floor(data.rating * 10) / 10))

                if (data?.penjual?.nama_toko != undefined) {
                    setNamaToko(data?.penjual?.nama_toko)
                }
                else {
                    setNamaToko("undefined");
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchData();
    }, [token]);

    // untuk format harga
    const formatNumber = (value: any) => {
        return value?.toLocaleString('id-ID', {
            useGrouping: true,
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
            minimumIntegerDigits: 1,
            style: 'decimal',
        });
    }

    // untuk menampilkan icon bintang
    const star = () => {
        if (rating >= 1 && rating < 2) {
            return <div className='flex justify-center mr-[9px]'><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px]' /></div>
        }
        else if (rating >= 2 && rating < 3) {
            return <>
                <div className='flex self-center items-center justify-center'>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px]' /></div>
                </div>
            </>
        }
        else if (rating >= 3 && rating < 4) {
            return <>
                <div className='flex self-center items-center justify-center'>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px]' /></div>
                </div>
            </>
        }
        else if (rating >= 4 && rating < 5) {
            return <>
                <div className='flex self-center items-center justify-center'>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px]' /></div>
                </div>
            </>
        }
        else if (rating >= 5 && rating < 6) {
            return <>
                <div className='flex self-center items-center justify-center'>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px] mr-[9px]' /></div>
                    <div><Image width={140} height={140} src={Star} alt='' className='w-[15px] h-[15px]' /></div>
                </div>
            </>
        }
    }

    // untuk menampilkan deskripsi rating sesuai dengan bintang
    const descRating = () => {
        if (products?.['rating'] == 'null' || products?.['rating'] == 0) {
            return ''
        }
        else if (rating >= 1 && rating < 2) {
            return 'Sangat Kurang'
        }
        else if (rating >= 2 && rating < 3) {
            return 'Kurang'
        }
        else if (rating >= 3 && rating < 4) {
            return 'Cukup'
        }
        else if (rating >= 4 && rating < 5) {
            return 'Baik'
        }
        else if (rating >= 5 && rating < 6) {
            return 'Sangat Baik'
        }
    }

    // untuk menambahkan produk
    const increment = () => {
        if (jumlahProduk == 0) {
            setQuantity(quantity)
        }
        else if (jumlahProduk! > 0) {
            setQuantity(quantity + 1)
            setJumlahProduk(jumlahProduk - 1)
        }
    }

    // untuk mengurangi produk
    const decrement = () => {
        if (quantity > 0) {
            setQuantity(quantity - 1)
            setJumlahProduk(jumlahProduk + 1)
        }
        else if (quantity == 0) {
            setQuantity(quantity)
        }
    }

    return (
        <>
            <div className="">
                <Header />
                <div className='content-detail-wrapper shadow-lg'>
                    <div className='pl-6 pt-12 pb-6 font-roboto font-normal flex text-black'>
                        <a href={'/'} className='text-[14px] mr-1 no-underline'>Home / </a>
                        <a href={'/Product'} className='text-[14px] mr-1 no-underline'>Product / </a>
                        <p className='text-[14px]'>{products?.['nama']}</p>
                    </div>
                    <div className=' flex'>
                        <div>
                            <Modal />
                        </div>
                        <div className='pl-9 pr-6'>
                            <div className='font-semibold font-roboto text-[32px] mb-[6px]'>{products?.['nama']}</div>
                            <div className='flex'>
                                <div className='text-abu-abu font-semibold text-[12px]'>Kualitas Produk</div>
                                <div className='w-[111px] h-[15px] ml-[25px] mr-[25px] text-center'>
                                    {star()}
                                </div>
                                <div className='text-text-kuning font-semibold text-[12px]'>{descRating()}</div>
                            </div>
                            <div className='font-semibold text-[16px] text-warna-text-title-produk mt-[10px] mb-[8px]'>{"Toko " + namaToko}</div>
                            <div className='flex'>
                                <div>
                                    <Image src={Toko} alt='' width={140} height={140}
                                        className='w-[20px] h-[20px]'
                                    />
                                </div>
                                <div className='font-normal text-[14px] text-warna-text-title-produk ml-1 mr-[6px]'>{products?.['penjual'] ? products?.['penjual']['pasar']['nama'] : "Undefined"}</div>
                                <div className=''>
                                    <Image src={Line} alt='' width={140} height={140}
                                        className='w-[2px] h-[20px]'
                                    />
                                </div>
                                <div className='ml-[6px] mr-[6px] self-center text-center items-center justify-center'>
                                    <Image src={Star} alt='' width={140} height={140}
                                        className='w-[12.19px] h-[11.67]'
                                    />
                                </div>
                                <div className='text-warna-text-title-produk text-[10px] font-normal self-center'>{rating + "/5"}</div>
                            </div>
                            <div className='mt-[10px] flex justify-start'>
                                <div className='mr-[9px] font-sans font-bold  text-warna-text-harga-produk text-[20px]'>{"Rp " + formatNumber(products?.['harga'])}</div>
                                <div className='text-[12.25px] font-normal text-warna-text-sold-produk self-end'>{products?.['sold'] == null ? "0 terjual" : products?.['sold'] + " terjual"}</div>
                            </div>
                            <div className='mt-[14px] text-[16px] font-semibold text-warna-text-title-produk'>Deskripsi Produk</div>
                            <div className='text-[14px] font-normal mt-[8px] w-[517px]'>{products?.['deskripsi']}</div>
                            <div className='mt-[14px] text-[16px] font-semibold text-warna-text-title-produk mb-2'>Kuantitas Produk</div>
                            <div className="flex">
                                <div className='flex w-[120px] h-[32px] bg-abu-abu-terang rounded-[16px] justify-between'>
                                    <div onClick={decrement} className='ml-2 mt-1 mr-4 mb-1 self-center cursor-pointer w-[22px] h-[22px]'>
                                        <Image width={140} height={140} src={Decrement} alt='' />
                                    </div>
                                    <div className='self-center text-warna-text-harga-produk text-[12px] font-normal'>{quantity}</div>
                                    <div onClick={increment} className='mr-2 mt-1 ml-4 mb-1 self-center cursor-pointer w-[20px] h-[20px]'>
                                        <Image width={140} height={140} src={Increment} alt='' />
                                    </div>
                                </div>
                                <div className='font-normal text-[12.25px] ml-2 text-warna-text-sold-produk self-center'>{jumlahProduk + " Tersisa"}</div>
                            </div>
                            {hasToken && tokenManually ? (
                                <button className='bg-[#788E39] w-[178px] h-[44px] rounded-[8px] text-white font-roboto text-[16px] font-medium flex justify-center items-center cursor-pointer mt-[38px] mb-[48px]'
                                    onClick={detectUserAgent}>
                                    Beli</button>
                            ) : (
                                <Login
                                    thisLogin={false}
                                    thisProductDetail={true}
                                />
                            )}
                        </div>
                    </div>
                </div>
                <div className='content-detail-wrapper shadow-lg mt-3'>
                    <div className='flex justify-between pt-6'>
                        <div className='ml-7 font-normal text-[24px] text-abu-abu'>Produk Dari Toko</div>
                        <div className='mr-7 no-underline font-semibold text-[20px] text-merah-lihat-semua cursor-pointer'>Lihat Semua</div>
                    </div>
                    <div className='flex justify-between grid grid-cols-5 gap-4 pl-7 pr-7'>
                        {datas.map((fetchProduct) => (
                            <div key={fetchProduct.id}>
                                <div className='items-center mb-6 mt-6 '>
                                    <a className='no-underline' href={`/Product/${fetchProduct.id}`}>
                                        <Image width={140} height={140} src={fetchProduct.image! ? fetchProduct.image! : ""} alt=''
                                            className='w-[191.08px] h-[191.08px] cursor-pointer'
                                        />
                                        <div className='font-sans text-warna-text-title-produk font-semibold text-[14.7px] w-[135px] h-[25px] mt-[3.67px] truncate cursor-pointer'>{fetchProduct.nama + " - " + fetchProduct.berat + " g"}</div>
                                        <div className='flex justify-between w-[190px] h-[25px] items-center'>
                                            <div className='font-sans font-semibold text-warna-text-harga-produk text-[17.15px]'>{"Rp " + formatNumber(fetchProduct.harga)}</div>
                                            <div className='text-[12.25px] font-normal text-warna-text-sold-produk mr-4'>{fetchProduct.sold == null ? "0 terjual" : fetchProduct.sold + " terjual"}</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
                <Footer />
            </div>
        </>
    )
}

export default page